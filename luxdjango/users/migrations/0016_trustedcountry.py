# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0049_auto_20170111_2236'),
        ('users', '0015_subscription_token'),
    ]

    operations = [
        migrations.CreateModel(
            name='TrustedCountry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country', models.ForeignKey(to='vehicles.Country')),
            ],
        ),
    ]
