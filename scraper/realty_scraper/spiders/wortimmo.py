# -*- coding: utf-8 -*-
import scrapy, re
import datetime

from ..items import RealtyItem
from realty.models import Realty


class WortimmoSpider(scrapy.Spider):
    name = 'wortimmo'
    allowed_domains = ["wortimmo.lu"]
    start_urls = [
        "http://www.wortimmo.lu/fr/recherche?p=64zb5sPmqprB18ni4I%252BeV5KSlpfqueaqnrTa09PO4c2kz5Kkl%252Bq55qqed5qG3tLiylebktja6bqaYlu23MfZ0tuGb4PR2NjdueZYtg%253D%253D",
        "http://www.wortimmo.lu/en/search?p=64zb5sPmqprB18ni4I%252BeV5KSlpfqueaqnrTa09PO4c2kz5Kkl%252BDD25etvt3SkpmP0prW1oyvlsLdq593mobR29DNms%252BSpJfVwtufnsOQ4Q%253D%253D"
    ]

    def parse(self, response):
        adv_list = response.xpath('//div[@class ="content_annonce_bien"]')
        for adv in adv_list:
            price = adv.xpath('.//div[@class="price"]/a/text()').extract_first()
            link = adv.xpath('.//div[@class="reference"]/a/@href').extract_first()
            url = response.urljoin(link)
            realty = Realty.objects.filter(url=url).only('price').first()
            if not realty:
                yield scrapy.Request(url, callback=self.parse_detail)
            else:
                self.logger.info('/// already exists ///')
                price = re.search(r'\d+[ \.]?\d{0,3}[ \.]?\d{0,3}', price)
                if price:
                    price = price.group().replace(' ', '')
                    price = int(price) if price else None
                    item = RealtyItem()
                    item['model_obj'] = realty
                    if price != realty.price:
                        item['new_price'] = price
                    item['update'] = True
                    yield item
        next_page = response.xpath(u'//a[@title="Page suivante du résultat"]/@href').extract_first()
        if next_page:
            self.logger.info('///////////////////// go to next page /////////////////////')
            yield scrapy.Request(response.urljoin(next_page), callback=self.parse)

    def parse_detail(self, response):
        new_realty_years = 5
        self.logger.info('/// parse detail ///')
        item = RealtyItem()
        item['url'] = response.url
        item['country'] = 'Luxembourg'

        city = response.xpath('//div[@class="city"]/text()').extract_first()
        if city:
            item['city'] = city.strip()

        title = response.xpath('//meta[@name="description"]/@content').extract_first()
        item['deal'] = 1 if u'\xe0 vendre' in title else 2

        property_type = response.xpath('//div[@class="fil_ariane_element"]/a/span/text()').extract()[1]

        if 'Garage' in property_type:
            property_type = 'Garage'
        elif 'Jardin' in property_type:
            property_type = 'Terrain'
        elif 'Location de vacances' in property_type:
            property_type = 'Chambre'
        item['property_type'] = property_type.strip()

        description = response.xpath('//div[@class="sub_content_detail description_detail"]/text()').extract()
        if description:
            description = [x.strip() for x in description if x.strip()]
            if description:
                item['description'] = '<br>'.join(description)

        price = response.xpath('//div[@id="prix_detail"]/text()').extract_first()
        price = re.sub(r'\D', '', price) if price else None
        if price:
            item['price'] = price

        bloc_caracteristiques_objects = response.xpath('//div[@class="bloc_caracteristiques"]/ul/li/text()').extract()
        dict_features = {'land_area': u'Terrain',
                         'garden': u'Jardin'}

        for key, value in dict_features.iteritems():
            matching = filter(lambda x: value in x, bloc_caracteristiques_objects)
            if matching:
                ss = re.search(r'[\d.]+', matching[0])
                result = ss.group().strip() if ss else 1
                item[key] = result

        dict_features = {'bathroom': u'Salle de bain',
                         'balcony': u'Balcon',
                         'terrace': u'Terrasse',
                         'floor': u'Etage',
                         'total_floors': u"Nombre d'étage(s)",
                         'garage': u'Garage',
                         'area': u'Surface',
                         'rooms': u'chambre(s)'}
        for key, value in dict_features.iteritems():
            matching = filter(lambda x: value in x, bloc_caracteristiques_objects)
            if matching:
                ss = re.search(r'[\d.]+', matching[0])
                result = ss.group().strip().split('.')[0] if ss else 1
                item[key] = result

        matching = filter(lambda x: u'Année de constr.' in x, bloc_caracteristiques_objects)
        if matching:
            ss = re.search(r'[\d]+', matching[0])
            result = ss.group() if ss else None
            item['year_built'] = result
            if result and (int(item['year_built']) + new_realty_years) >= datetime.datetime.now().year:
                item['new_building'] = True

        energy = response.xpath('//div[@class="image_peb"]/img/@alt').extract()
        if energy:
            energy = [x.strip() for x in energy]
            if energy[0]:
                item['energy_class'] = energy[0]
            if energy[1]:
                item['termal_protection_class'] = energy[1]
        script = response.xpath('//script[contains(text(), "var locations")]/text()').extract_first()
        coordinates = re.findall(r'\d+\.\d+', script)
        if coordinates:
            item['latitude'] = coordinates[0]
            item['longitude'] = coordinates[1]

        phones = response.xpath('//a[@class="hidden-sm hidden-md hidden-lg btn-detail blue"]/@href').extract_first()
        phones = re.sub(r'\D', '', phones)
        if phones:
            item['phones'] = [phones]

        seller = response.xpath('//div[@class="agency-name"]/text()').extract_first()
        if seller:
            item['seller'] = seller

        image_urls = response.xpath('//div[@class="slides_details_element"]/table/tr/td/a/img/@src').extract()
        if image_urls:
            item['image_urls'] = image_urls

        yield item
