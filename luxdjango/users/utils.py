# -*- coding: utf-8 -*-
import json
import site

from django.utils import timezone
from django.utils.translation import ugettext as _
from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.conf import settings
from django.contrib.sites.models import Site

from classifieds.models import SubCategory, SubSubCategory
from jobs.models import WorkHours, Contract
from luxdjango.users.utils2 import get_template_text
from geoip2 import database as geoip2_db
from geoip2.errors import AddressNotFoundError
from realty.models import PropertyTypeType, PropertyType, City
from vehicles.models import Brand, Country
from vehicles.models import Model
from jobs.models import Category as JobCategory
from classifieds.models import Category as ClassifiedsCategory
from .models import User
from vehicles.views import ElasticSearchAjax as vehicle_search
from realty.views import ElasticSearchAjax as realty_search
from jobs.views import ElasticSearchAjax as job_search
from classifieds.views import ElasticSearchAjax as classified_search
from django.template import Template as DjangoTemplate
from django.template import Context

from .models import Subscription, TrustedCountry
from vehicels.models import Country


categories = ((vehicle_search, 1, 'Cars'),
              (realty_search, 2, 'Realties'),
              (job_search, 3, 'Jobs'),
              (classified_search, 4, 'Classifieds'),
              (vehicle_search, 5, 'Motorbikes'))


def get_subscription(filters, category):
    href = ''
    name = ''
    for key, value in filters.iteritems():
        href += (u'{}={}&'.format(key, value))
        if value:
            if 'brand_index' == key:
                value = Brand.objects.get(pk=int(value)).brand_name
            if 'model_index' == key:
                value = Model.objects.get(pk=value).model_name
            if 'work_hours_index' == key:
                value = WorkHours.objects.get(pk=value).work_hours
            if 'q' == key:
                key = 'Text'
            if 'category_index' == key:
                if category == 3:
                    value = JobCategory.objects.get(pk=value).name
                else:
                    value = ClassifiedsCategory.objects.get(
                        pk=value).name
            if 'contract_index' == key:
                value = Contract.objects.get(pk=value).contract
            if 'sub_category_index' == key:
                value = SubCategory.objects.get(pk=value).name
            if 'sub_sub_category_index' == key:
                value = SubSubCategory.objects.get(pk=value).name
            if 'property_type_type' == key:
                value = PropertyTypeType.objects.filter(id=value).first().name
            elif 'property_type' == key:
                value = PropertyType.objects.filter(id=value).first().name
            if 'city' == key:
                value = City.objects.get(id=value)
            if 'countries' == key:
                country_ids = [int(x) for x in str(value).split(',')]
                country_names = Country.objects.filter(
                    id__in=country_ids
                ).values_list('name', flat=True)
                value = ', '.join(country_names)
            if 'deal' == key:
                value = _('Sale') if value == 1 else _('Rent')
            if key.endswith('_index'):
                key = key.replace('_index', '')
            name += (u'{}: {}; '.format(key, value))
    if category == 1:
        category = 'vehicles/cars'
    elif category == 2:
        category = 'realty'
    elif category == 5:
        category = 'vehicles/motorbikes'
    else:
        category = [i[2].lower() for i in categories if i[1] == category][0]
    result = u'<a href="{}/{}/search/?{}">{}</a>'.format(Site.objects.get_current().domain, category, href, name)
    return result


def send_notification():
    from email import Charset
    Charset.add_charset('utf-8', Charset.SHORTEST, None, 'utf-8')
    result = {}
    site = Site.objects.get_current()
    users = User.objects.filter(is_active=True).values_list('id', 'email')
    for user_id, user_email in users:
        for ElasticSearchAjax, category, category_name in categories:
            result[category_name] = {}
            subscriptions = Subscription.objects.filter(user_id=user_id, category=category)
            el_search = ElasticSearchAjax()
            el_search.request = lambda: None
            for subscr in subscriptions:
                el_search.request.GET = json.loads(subscr.filters)
                if category == 5:
                    el_search.request.META = {}
                    el_search.request.META['HTTP_REFERER'] = 'motorbikes'
                elif category == 1:
                    el_search.request.META = {}
                    el_search.request.META['HTTP_REFERER'] = 'sfdsf'
                elif category == 4:
                    el_search.request.META = {}
                    el_search.request.META['HTTP_REFERER'] = '4'
                q = el_search.queryset()
                if subscr.last_notification:
                    q = q.filter(date_index__gt=subscr.last_notification)
                total = q.count()
                if total:
                    object_list = q.order_by('-date_index')[:3]
                    result[category_name][get_subscription(json.loads(subscr.filters), category)] = {
                        'object_list': object_list,
                        'total': total,
                        'category_id': category,
                        'token': subscr.token.token
                    }
                    subscr.last_notification = timezone.now()
                    subscr.save()
        if [j for i, j in result.iteritems() if j]:
            email_content = DjangoTemplate(get_template_text('updates')).render(
                Context({'result': result, 'site': site, 'domain': settings.DOMAIN}))
            email = EmailMessage(_('new posts for you subscriptions'), email_content, settings.EMAIL_HOST_USER,
                                 to=[user_email])
            email.content_subtype = 'html'
            email.send()


def needModeration(ip):
    reader = geoip2_db.Reader(settings.GEOIP_PATH)
    try:
        ip_data = reader.city(ip)
        country = Country.objects.filter(name_em=ip_data.country.name)
        if TrustedCountry.objects.filter(country_id=country.id).exists():
            return False
        else:
            return True
    except AddressNotFoundError:
        return False

