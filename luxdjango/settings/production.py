# -*- coding: utf-8 -*-
'''
Production Configurations

- Use djangosecure
- Use Amazon's S3 for storing static files and uploaded media
- Use mailgun to send emails
- Use Redis on Heroku


'''

from __future__ import absolute_import, unicode_literals

# from boto.s3.connection import OrdinaryCallingFormat
from django.utils import six

from .common import *  # noqa

# STORAGE CONFIGURATION
# ------------------------------------------------------------------------------
# Uploaded Media Files
# ------------------------
# See: http://django-storages.readthedocs.org/en/latest/index.html
# INSTALLED_APPS += (
#     'storages',
#     'collectfast',
# )
# DEFAULT_FILE_STORAGE = 'luxdjango.settings.s3utils.MediaRootS3BotoStorage'
# STATICFILES_STORAGE = 'luxdjango.settings.s3utils.StaticRootS3BotoStorage'

# AWS_PRELOAD_METADATA = True

# DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
#
# AWS_ACCESS_KEY_ID = 'AKIAI5LC5FE5WPXETE3A'
# AWS_SECRET_ACCESS_KEY = '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'
# AWS_STORAGE_BUCKET_NAME = 'skruut'
# AWS_AUTO_CREATE_BUCKET = False
# AWS_QUERYSTRING_AUTH = False
# AWS_S3_CALLING_FORMAT = OrdinaryCallingFormat()
#
# # AWS cache settings, don't change unless you know what you're doing:
# AWS_EXPIRY = 60 * 60 * 24 * 7

# TODO See: https://github.com/jschneier/django-storages/issues/47
# Revert the following and use str after the above-mentioned bug is fixed in
# either django-storage-redux or boto
# AWS_HEADERS = {
#     'Cache-Control': six.b('max-age=%d, s-maxage=%d, must-revalidate' % (AWS_EXPIRY, AWS_EXPIRY))
# }

# AWS_S3_CUSTOM_DOMAIN = '%s.amazonaws.com/autodjango' % 's3-us-west-2'

# ADMIN_MEDIA_PREFIX = 'http://127.0.0.1:8000/static/admin'

# db_from_env = dj_database_url.config(conn_max_age=500)
# DATABASES['default'].update(db_from_env)

DEBUG = True

SITE_ID = 2

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DATABASE_NAME,
        'USER': DATABASE_NAME,
        'PASSWORD': DATABASE_PASSWORD,
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

SESSION_COOKIE_SECURE = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')
CSRF_COOKIE_SECURE = True

# HAYSTACK_CONNECTIONS = {
#     'default': {
#         'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
#         'URL': 'https://8rg7c2f0:1uh4eiypndmk6fhh@azalea-4460077.us-east-1.bonsai.io',
#         'INDEX_NAME': 'haystack',
#     },
# }


# for row in Car.objects.all():
#     if Car.objects.filter(id=row.id).count() > 1:
#         row.delete()

# unique_fields = ['url', …,]

# duplicates = (Car.objects.values(*unique_fields)
#                              .order_by()
#                              .annotate(max_id=models.Max('id'),
#                                        count_id=models.Count('id'))
#                              .filter(count_id__gt=1))
# for duplicate in duplicates:
#     (Car.objects.filter(**{x: duplicate[x] for x in unique_fields})
#                     .exclude(id=duplicate['max_id'])
#                     .delete())
