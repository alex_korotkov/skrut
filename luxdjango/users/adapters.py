# -*- coding: utf-8 -*-
from allauth.account.utils import perform_login
from allauth.utils import resolve_url
from django.conf import settings
from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter

from luxdjango.users.models import User


class AccountAdapter(DefaultAccountAdapter):
    def is_open_for_signup(self, request):
        return getattr(settings, 'ACCOUNT_ALLOW_REGISTRATION', True)

    # def get_login_redirect_url(self, request):
    #     url = settings.LOGIN_REDIRECT_URL
    #     r = request
    #     print r
    #     import pdb; pdb.set_trace()
    #     return resolve_url(url)


class SocialAccountAdapter(DefaultSocialAccountAdapter):
    def is_open_for_signup(self, request, sociallogin):
        return getattr(settings, 'ACCOUNT_ALLOW_REGISTRATION', True)

    def pre_social_login(self, request, sociallogin):
        user = sociallogin.user
        if user.id:
            return
        try:
            exist_user = User.objects.get(email=user.email)  # if user exists, connect the account to the existing account and login
            sociallogin.state['process'] = 'connect'
            perform_login(request, exist_user, 'none')
            sociallogin.connect(request, exist_user)
        except User.DoesNotExist:
            pass

        return



