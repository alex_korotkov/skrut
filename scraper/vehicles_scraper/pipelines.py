# -*- coding: utf-8 -*-
# import hashlib
import logging
from django.db.models import Q
# from django.utils import timezone
from scrapy.exceptions import DropItem
from scrapy.http import Request
from scrapy.pipelines.images import ImagesPipeline

from realty.models import City
from scraper.utils import img_path
from vehicles.models import Car, CarImage, Brand, Model, Seller, Country,\
                            Color, BaseColor, Moto, MotoImage

BASE_COLORS = BaseColor.objects.all().values_list('id', 'color_en', 'color_fr')
COUNTRIES = [
    {'id': x[0], 'names': x[1:]}
    for x in Country.objects.all().values_list('id', 'name_en', 'name_fr')
]
CAR_FIELDS = [x for x in Car().__dict__.keys() if x not in ['_state', 'id']]
MOTO_FIELDS = [x for x in Moto().__dict__.keys() if x not in ['_state', 'id']]

logger = logging.getLogger('vehicles_pipline')


class ObjModelPipeline(object):
    def process_item(self, item, spider):
        if item.get('new_price') or item.get('update'):
            return self.process_item_update(item, spider)
        else:
            return self.process_new_item(item, spider)

    def process_new_item(self, item, spider):
        logger.info('///', 'process new %s item' % item['model_class'])
        brand = Brand.objects.filter(brand_name__iexact=item['brand']).first()
        if not brand:
            brand = Brand.objects.create(brand_name=item['brand'])
        FIELDS = CAR_FIELDS if item['model_class'] == 'Car' else MOTO_FIELDS
        ModelClass = Car if item['model_class'] == 'Car' else Moto
        values_dict = {'brand': brand}
        if item['seller'].get('name'):
            seller = Seller.objects.filter(name__iexact=item['seller']['name']).first()
            if not seller:
                seller = Seller.objects.create(**item['seller'])
            values_dict.update({'seller': seller})

        if item.get('model'):
            vehicle_model = Model.objects.filter(
                model_name__iexact=item['model'], brand=brand
            ).first()
            if not vehicle_model:
                vehicle_model = Model.objects.create(
                    model_name=item['model'], brand=brand
                )
            values_dict['model'] = vehicle_model

        for key in ['color', 'color_inside']:
            if item.get(key):
                item[key] = item[key].lower()
                color = Color.objects.filter(
                    Q(color_fr=item[key]) | Q(color_en=item[key])
                ).first()
                if not color:
                    color_data = {'color': item[key]}
                    for c in BASE_COLORS:
                        if c[2] in item[key] or c[1] in item[key]:
                            color_data['base_color_id'] = c[0]
                            break
                    color = Color.objects.create(**color_data)
                values_dict['%s_id' % key] = color.id

        if item.get('country'):
            for c in COUNTRIES:
                if item['country'] in c['names']:
                    values_dict['country_id'] = c['id']
                    break
            city_name = item.get('city')
            if city_name:
                city = City.objects.filter(
                    name__iexact=city_name, country_id=values_dict['country_id'],
                ).first()
                if not city:
                    city = City.objects.create(
                        name=city_name, country_id=values_dict['country_id'],
                    )
                values_dict['city'] = city

        for field in FIELDS:
            value = item.get(field)
            if value:
                values_dict[field] = value
        obj = ModelClass.objects.create(**values_dict)
        item['model_obj'] = obj
        return item

    def process_item_update(self, item, spider):
        if item.get('new_price'):
            logger.info('/// process price update')
            item['model_obj'].update_price(item['new_price'])
        item['model_obj'].update_update_date()
        raise DropItem('Price updated')


class StoreImgPipeline(ImagesPipeline):
    def process_item(self, item, spider):
        self.sub_folder_name = item['model_class'].lower() + '-sc'
        return super(StoreImgPipeline, self).process_item(item, spider)

    def file_path(self, request, response=None, info=None):
        logger.info('//// file path ////')
        return img_path(self.sub_folder_name, request.url)


class SaveImgModelPipeline(object):
    def process_item(self, item, spider):
        logger.info('/// process img model save ///')
        class_name = item['model_obj'].__class__.__name__
        ImageModelClass = CarImage if class_name == 'Car' else MotoImage
        key = '%s_id' % class_name.lower()

        for img in item.get('images', []):
            values_dict = {key:item['model_obj'].id, 'image':img['path']}
            ImageModelClass.objects.create(**values_dict)
        return item
