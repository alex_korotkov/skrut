'use strict';

var gulp = require('gulp'),
    del = require('del'),
	sass = require('gulp-sass');


gulp.task('clean', function() {
    return del(
        [
            // static dir
            '../luxdjango/static/assets/base/css/' ,
            '../luxdjango/static/assets/base/img/' ,
            '../luxdjango/static/assets/base/js/' ,
        ],
        {force: true}
    );
});

gulp.task('sass', function () {
	gulp.src('./sass/**/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('../luxdjango/static/assets/base/css/'));
});

gulp.task('sass:watch', function () {
	gulp.watch('./sass/**/*.scss', ['sass']);
});

gulp.task('images:copy', function() {
    return gulp.src('./assets/base/img/**')
        .pipe(gulp.dest('../luxdjango/static/assets/base/img/'))
});

gulp.task('js:copy', function() {
    return gulp.src('./assets/base/js/**')
        .pipe(gulp.dest('../luxdjango/static/assets/base/js/'))
});

gulp.task('js:copy:modules', function() {
    return gulp.src('./assets/base/js/modules/*')
        .pipe(gulp.dest('../luxdjango/static/assets/base/js/modules/'))
});

gulp.task('plugins:copy', function() {
    return gulp.src('./assets/plugins/**')
        .pipe(gulp.dest('../luxdjango/static/assets/plugins/'))
});

gulp.task('js:watch', function() {
    gulp.watch('./assets/base/js/modules/**.js', ['js:copy:modules']);
});

gulp.task('default', ['clean',], function() {
    gulp.start('sass', 'images:copy', 'js:copy', 'plugins:copy');
});


var prettify = require('gulp-prettify');
 
gulp.task('prettify', function() {
  gulp.src('../../master/release/theme/*.html')
    .pipe(prettify({
    	indent_size: 4, 
    	indent_inner_html: true,
    	unformatted: ['pre', 'code']
   	}))
    .pipe(gulp.dest('../../master/release/theme/'))
});