# -*- coding: utf-8 -*-
import datetime, json, re, scrapy

from realty.models import City
from ..items import CarItem
from vehicles.models import Car

cities = City.objects.filter(country_id=35).values_list('name', flat=True)


class AutofinderSpider(scrapy.Spider):
    name = 'autofinder'
    allowed_domains = ['autofinder.lu']
    start_urls = ['http://www.autofinder.lu/q/r11240/seite_1/Auto.html?em_language=french&em_suche[seite]=1']

    def parse(self, response):
        self.logger.info('////start parsing////')
        adv_list = response.xpath('//div[@class="em_ergebnis_container"]')
        for adv in adv_list:
            link = adv.xpath('.//h2[@class="em_left"]/a/@href').extract_first()
            url = response.urljoin(link)
            car = Car.objects.filter(url=url).only('price').first()
            if not car:
                self.logger.info('yield request')
                yield scrapy.Request(url, callback=self.parse_detail)
            else:
                self.logger.info('already exists')
                price = adv.xpath('.//span[@class="em_right"]/text()').extract_first()
                price = int(re.sub(r'\D', '', price))
                item = CarItem()
                item['model_obj'] = car
                if price != car.price:
                    item['new_price'] = price
                item['update'] = True
                yield item

        next_page = response.xpath(
            '//a[@class="em_btn_arrow_right  seoLink"]/@href'
        ).extract_first()
        if next_page:
            yield scrapy.Request(url=response.urljoin(next_page), callback=self.parse)

    def parse_detail(self, response):
        item = CarItem()
        item['url'] = response.url
        item['model_class'] = 'Car'
        item['country'] = 'Luxembourg'
        dict_details = {}
        details = response.xpath('//div[@id="em_object_details"]/div')
        for detail in details:
            key = detail.xpath('.//div[@class="em_merkmal_label em_left em_bold"]/text()').extract_first()
            value = detail.xpath('.//div[@class="em_merkmal_value em_left"]/text()').extract_first()
            dict_details[key] = value
        dict_features = {
            'brand': u'Marque',
            'energy': u'Carburant',
            'color': u"Couleur",
            'model': u'Modèle',
            'gear_box': u'Boîte'
        }
        dict_details_list = dict_details.keys()
        dict_details_list.remove(None) if None in dict_details_list else dict_details_list
        for key, value in dict_features.iteritems():
            matching = filter(lambda x: value in x, dict_details_list)
            if matching:
                item[key] = re.sub(r'\W', '', dict_details[matching[0]])
        dict_features = {
            'model_year': u'Année',
            'power': u'Puissance',
            'cylinder': u'ccm',
            'price': u'Prix',
            'km': u'KM',
        }
        for key, value in dict_features.iteritems():
            matching = filter(lambda x: value in x, dict_details_list)
            if matching:
                ss = re.sub(r'\D', '', dict_details[matching[0]])
                result = ss.strip() if ss else 1
                item[key] = result

        options = response.xpath(
            '//div[@id="em_object_details"]/div[@class="em_merkmal_value em_left"]/text()'
        ).extract_first()
        if options:
            item['options'] = json.dumps([x.strip() for x in options.split(',')])
        item['seller'] = {}
        seller = response.xpath(
            '//div[@class="em_modul_box_content"]/b/text()'
        ).extract_first()
        if seller:
            item['seller']['name'] = seller

        model_type = response.xpath(
            '//div[@class="em_cnt_box_body"]/div[@class="em_cnt_master_head"]/h3/text()'
        ).extract_first()
        if model_type:
            item['model_type'] = model_type
        seller_info = response.xpath(
            '//div[@class="em_modul_box_content"]/text()'
        ).extract()
        if seller_info:
            matching = filter(lambda x: u'Tél' in x, seller_info)
            if matching:
                ss = re.sub(r'\D', '', matching[0])
                result = ss.strip() if ss else None
                item['seller']['phones'] = result
                item['phones'] = [result]
                item['seller']['seller_type'] = 2  # professional
        seller_info = ' '.join(seller_info).lower()
        city = next((x for x in cities if x.lower() in seller_info), None)
        if city:
            item['city'] = city
        image_urls = response.xpath('//img[@class="emthumbimage em_tooltip"]/@alt').extract()
        if image_urls:
            item['image_urls'] = image_urls

        yield item
