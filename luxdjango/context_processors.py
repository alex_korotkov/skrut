from collections import OrderedDict
from django.core.urlresolvers import reverse
from django.conf import settings
from django.db.models import Q
from django.utils.translation import ugettext as _
from math import ceil

from classifieds.models import SubCategory, Category
from jobs.models import Category as JobCategory
from realty.models import Realty


def header_categories(request):
    if request.is_ajax():
        return {}

    def list_to_cols(lst, cols=3):
        l = len(lst)
        if l > 3:
            n = int(ceil(l / float(cols)))
            return [lst[x:x+n] for x in range(0, l, n)]
        else:
            return [lst]

    vehicles_url = reverse('vehicles:all_vehicles')
    vehicles = {
        'big_cat': _(u'Vehicles'),
        'cats':  [
            {'name':_(u'All'), 'url': vehicles_url},
            {'name':_(u'Cars'), 'url': vehicles_url + '?sub_category=1'},
            {'name':_(u'Car parts and accessories'), 'url': vehicles_url + '?sub_category_index=3'},
            {'name':_(u'Motorbikes'), 'url': vehicles_url + '?sub_category=2'},
            {'name':_(u'Moto parts and accessories'), 'url': vehicles_url + '?sub_category_index=18'},
        ],
    }
    subcats = SubCategory.objects.filter(id__in=(26, 39, 67))\
                                 .order_by('name')\
                                 .values_list('id', 'name')
    vehicles['cats'] += list([
        {'name': x[1], 'url': u'%s?sub_category_index=%s' % (vehicles_url, x[0])}
        for x in subcats
    ]) + [
        {'name': u'Boats', 'url': vehicles_url + u'?sub_category_index=39&sub_sub_category_index=67'}
    ]
    vehicles['active'] = True if '/vehicles/' in request.path else False

    realty_url = reverse('realty:search')
    realty = {
        'big_cat': _(u'Realty'),
        'cats': [{'name': _(u'All'), 'url': realty_url}] + [
            {'name': x[1], 'url': u'%s?deal=%s' % (realty_url, x[0])}
            for x in Realty.DEAL_CHOICES
        ]
    }
    realty['cats'].append({'name': _('view on map'), 'url': reverse('realty:map')})
    realty['active'] = True if '/realty/' in request.path else False

    jobs_url = reverse('jobs:search')
    jobs = {
        'big_cat': _(u'Jobs'),
        'cats': [{'name': _(u'All'), 'url': jobs_url}] + [
            {'name': x[1], 'url': u'%s?category_index=%s' % (jobs_url, x[0])}
            for x in JobCategory.objects.all().values_list('id', 'name').order_by('name')
        ]
    }
    jobs['active'] = True if u'/jobs/' in request.path else False

    good_deals_url = reverse('classifieds:good_deals')
    good_deals = {
        'big_cat': _(u'Good deals'),
        'cats': [{'name': _(u'All'), 'url': good_deals_url}] + [
            {'name': x[1], 'url': u'%s?sub_category_index=%s' % (good_deals_url, x[0])}
            for x in SubCategory.objects.exclude(
                Q(category_id__in=(2, 3)) | Q(id__in=(3, 18, 26, 39, 67))
            ).order_by('name').values_list('id', 'name')
        ]
    }
    good_deals['active'] = True if '/good-deals/' in request.path else False

    sports_url = reverse('classifieds:sports_and_leisure')
    sports = {
        'big_cat': _(u'Sports and leisure'),
        'cats': [{'name': _(u'All'), 'url': sports_url}] + [
            {'name': x[1], 'url': u'%s?sub_category_index=%s' % (sports_url, x[0])}
            for x in SubCategory.objects.filter(category_id=2)\
                                        .exclude(id__in=(3, 18, 26, 39, 67))\
                                        .order_by('name')\
                                        .values_list('id', 'name')
        ]
    }
    sports['active'] = True if '/sports-and-leisure/' in request.path else False

    pets_url = reverse('classifieds:pets')
    pets = {
        'big_cat': _(u'Pets'),
        'cats': [{'name': _(u'All'), 'url': pets_url}] + [
            {'name': x[1], 'url': u'%s?sub_category_index=%s' % (pets_url, x[0])}
            for x in SubCategory.objects.filter(category_id=3)\
                                        .order_by('name')\
                                        .values_list('id', 'name')
        ]
    }
    pets['active'] = True if '/pets/' in request.path else False
    if (pets['active'] == sports['active'] == good_deals['active'] == jobs['active'] == realty['active'] ==
            vehicles['active']):
        vehicles['active'] = True

    result = [vehicles, realty, jobs, good_deals, sports, pets]
    for x in result:
        x['cats'] = list_to_cols(x['cats'])
    return {'header_categories': result}


def google_api_key(request):
    if hasattr(settings, 'GOOGLE_MAPS_API_KEY'):
        return {'GOOGLE_MAPS_API_KEY': settings.GOOGLE_MAPS_API_KEY}

def facebook_app_id(request):
    if hasattr(settings, 'FACEBOOK_APP_ID'):
        return {'FACEBOOK_APP_ID': settings.FACEBOOK_APP_ID}
