from django.core.management import BaseCommand

from luxdjango.users.utils3 import send_re_validation


class Command(BaseCommand):
    def handle(self, *args, **options):
        send_re_validation()
