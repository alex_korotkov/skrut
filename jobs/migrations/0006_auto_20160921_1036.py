# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0005_auto_20160919_1203'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employer',
            name='site',
            field=models.CharField(max_length=100, blank=True),
        ),
    ]
