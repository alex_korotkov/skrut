# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0034_auto_20160816_1405'),
        ('realty', '0016_auto_20160817_2131'),
    ]

    operations = [
        migrations.CreateModel(
            name='CandidateLevel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Contract',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('contract', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Employer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('site', models.CharField(max_length=50, blank=True)),
                ('email', models.EmailField(max_length=254, blank=True)),
                ('address', models.CharField(max_length=100, blank=True)),
                ('phones', models.CharField(max_length=100, blank=True)),
                ('country', models.ForeignKey(to='vehicles.Country', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=300)),
                ('address', models.CharField(max_length=100, blank=True)),
                ('description', models.TextField()),
                ('candidate_level', models.ManyToManyField(to='jobs.CandidateLevel', blank=True)),
                ('category', models.ManyToManyField(to='jobs.Category')),
                ('city', models.ForeignKey(blank=True, to='realty.City', null=True)),
                ('contract', models.ManyToManyField(to='jobs.Contract', blank=True)),
                ('country', models.ForeignKey(to='vehicles.Country')),
                ('employer', models.ForeignKey(blank=True, to='jobs.Employer', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='JobType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('job_type', models.CharField(max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='job',
            name='job_type',
            field=models.ManyToManyField(to='jobs.JobType', blank=True),
        ),
    ]
