# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('AdminConfig', '0005_auto_20161228_1532'),
    ]

    operations = [
        migrations.AlterField(
            model_name='facebookpageaccesstoken',
            name='facebook_access_token',
            field=models.CharField(max_length=200),
        ),
    ]
