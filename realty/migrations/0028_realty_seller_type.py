# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0027_auto_20161213_1157'),
    ]

    operations = [
        migrations.AddField(
            model_name='realty',
            name='seller_type',
            field=models.PositiveIntegerField(default=1, verbose_name='Seller type', choices=[(1, 'Private seller'), (2, 'Professional  seller')]),
        ),
    ]
