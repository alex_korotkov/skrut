# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0026_auto_20161213_1121'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realtyimage',
            name='realty',
            field=models.ForeignKey(related_name='images', to='realty.Realty'),
        ),
    ]
