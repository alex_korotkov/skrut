from django.contrib import admin

from realty.models import Realty, PropertyType, PropertyTypeType, City, Seller


class RealtyAdmin(admin.ModelAdmin):
    list_display = (
        'property_type', 'property_type_type', 'deal', 'rooms', 'price', 'country', 'city',
        'year_built', 'floor', 'phones', 'phones', 'is_active', 'url'
    )
    admin_order_field = 'property_type'

class SellerAdmin(admin.ModelAdmin):
    list_display = ('name', 'email')

admin.site.register(PropertyType)
admin.site.register(PropertyTypeType)
admin.site.register(City)
# admin.site.register(Seller, SellerAdmin)
admin.site.register(Realty, RealtyAdmin)
admin.site.register(Seller, SellerAdmin)
