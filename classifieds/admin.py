from django.contrib import admin

from .models import Classified


class ClassifiedAdmin(admin.ModelAdmin):
    list_display = (
        'title', 'category', 'sub_category', 'country',
        'city', 'price', 'email', 'phones', 'is_active', 'url'
    )

admin.site.register(Classified, ClassifiedAdmin)