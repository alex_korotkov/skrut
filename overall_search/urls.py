# -*- coding: utf-8 -*-
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^ajax/$', views.ElasticSearchAjax.as_view(), name="search_ajax"),
]
