from __future__ import unicode_literals

from django.apps import AppConfig


class VehiclesConfig(AppConfig):
    name = 'vehicles'

    def ready(self):
        # import signal handlers
        import luxdjango.signals2
