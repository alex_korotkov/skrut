# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.postgres.fields
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('classifieds', '0004_auto_20161006_1408'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='classified',
            name='options',
        ),
        migrations.RemoveField(
            model_name='classified',
            name='seller',
        ),
        migrations.AddField(
            model_name='classified',
            name='email',
            field=models.EmailField(max_length=254, blank=True),
        ),
        migrations.AddField(
            model_name='classified',
            name='phones',
            field=django.contrib.postgres.fields.ArrayField(size=None, null=True, base_field=models.CharField(max_length=24, validators=[django.core.validators.RegexValidator(b'^[\\s \\+ 0-9]{6,17}$', b'Enter a valid phone number.')]), blank=True),
        ),
        migrations.DeleteModel(
            name='Seller',
        ),
    ]
