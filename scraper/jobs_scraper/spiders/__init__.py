from .frjobs import FrJobsSpider
from .monster import MonsterJobsSpider
from .moovijob import MoovijobJobsSpider
from .jobfinder import JobfinderJobsSpider
