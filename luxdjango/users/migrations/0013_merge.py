# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0012_user_hide_phones'),
        ('users', '0012_auto_20161201_1504'),
    ]

    operations = [
    ]
