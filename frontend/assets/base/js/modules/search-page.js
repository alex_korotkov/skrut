function SearchPage(config) {
    var content = $('body');
    var semaphore = true;
    var disableScroll = true;
    parameters = {'q': config.q_text};
    var resultBlock = $('#search-list-result');
    var locateMeBtn = $('#locate-me');
    locateMeBtn.initial_text = locateMeBtn.text();
    var setMarker;

    if (getCookie('subscribe') === 'true') {
        var filters = getSubscriptionFilters();
        if (filters) subscribeToNotification(filters);
    }

    // put search text from post
    content.find(".fire-reload").val(config.q_text);

    // blocking enter key press
    $(".fire-reload").keypress(function(event) {
        if (event.keyCode === 10 || event.keyCode === 13)
            event.preventDefault();
    });

    // if page reload
    parameters = $.isEmptyObject($.getQueryParameters()) ? parameters : $.getQueryParameters();
    sendRequest();
    //window.si = setInitalLocation;

    // Base request
    function sendRequest(k, v) {
        if (k) parameters[k] = v;
        if (k != 'scroll' && v != true) {
            delete parameters.page;
            var style = getCookie('style');
            if (style) {
                parameters['style'] = style;
            }
            delete parameters.scroll;
        }

        if (config.rangeFiltersList && config.rangeFiltersList.hasOwnProperty(k)) {
            var minmax = v.split(',');
            if (
                config.rangeFiltersList[k].range.min[0] == minmax[0]
                && config.rangeFiltersList[k].range.max[0] == minmax[1].replace(/\./g, '')
            ) {
                delete parameters[k]
            }
        }
        resultBlock.append('<div id="results-loading">Loading...</div>');
        // console.log(parameters);

        ajaxGet(djangoContext.urls.ajaxSearch, parameters, function (content) {
            resultBlock.find('#results-loading').remove();
            scrollBehavior(content);
            if (window.location.pathname == "/vehicles/" && !parameters.scroll) {
                for (var item in config.rangeFiltersList) {
                    rangeFilter(item);
                }
            }
            delete parameters.scroll;
            history.pushState('filters', "current_state", $.encodeQueryParameters(parameters));
            semaphore = true;
        });
    }

    if (window.location.pathname != "/vehicles/") {
        for (var item in config.rangeFiltersList) {
            rangeFilter(item);
        }
    }

    //main text filter
    content.find(".fire-reload").on("keyup", function(event) {
        sendRequest('q', this.value);
    });

    //Check box filters
    content.find(".fire-reload-filters").on("click", 'li div input', function(event) {
        $('#search-list-result').empty();
        if (parameters[this.name] == this.value) {
            if ($(this).closest('li.head-cat').length) {
                parameters = {};
            } else {
                delete parameters[this.name];
            }
            sendRequest()
        } else {
            sendRequest(this.name, this.value);
        }
    });

    //colors filter
    $('#search-filters').on('click', "#colors-filters .color_faced_box", function(event) {
        if (parameters.color_index == $(this).attr('data_info')) {
            delete parameters.color_index;
            sendRequest()
        } else {
            sendRequest('color_index', $(this).attr('data_info'));
        }
    });

    if (parameters.sort) $('div#sort select').val(parameters.sort);
    if (parameters.show) $('div#show select').val(parameters.show);
    if (parameters.style) $('div#style select').val(parameters.style);
    content.find("#sort").on('change', '#sort', function() {
        $('#search-list-result').empty();
        if (this.value == 0) {
            delete parameters['sort'];
            sendRequest();
        } else {
            sendRequest('sort', this.value);
        }
    });
    content.find("#show").on('change', '#show', function() {
        $('#search-list-result').empty();
        if (this.value == 0) {
            delete parameters['show'];
            sendRequest();
        } else {
            sendRequest('show', this.value);
        }
    });
       content.find("#style").on('change', '#style', function() {
        $('#search-list-result').empty();
        setCookie('style', this.value);
        add_options();
        if (this.value == 0) {
            delete parameters['style'];
            sendRequest();
        } else {
            sendRequest('style', this.value);
        }
    });

    //Double Range filters
    function rangeFilter(key) {
        // Initializing the slider
        domElement = document.getElementById(key + '-range_slider');
        if (domElement == null) return;

        var temp = config.rangeFiltersList[key];
        var html5Slider = {};
        var range = temp['range'];
        var step = temp['step'] || 1000;
        var moneyFormat = temp['moneyFormat'] || wNumb({decimals: 0, thousand: '.'});

        var slider = noUiSlider.create(domElement, {
            start: parameters[key] ? parameters[key].split(",") : [range['min'][0], range['max'][0]],
            step: step,
            connect: true,
            range: range,
            format: moneyFormat

        });

        var leftSlider = document.getElementById(key + '-input-number-l');
        var rightSlider = document.getElementById(key + '-input-number-r');

        (function (key, leftSlider, rightSlider, domElement) {
            domElement.noUiSlider.on('update', function(values, handle) {
                var value = values[handle];
                if (handle) {
                    rightSlider.value = value;
                } else {
                    leftSlider.value = value;
                }
            });

            domElement.noUiSlider.on('set', function(values, handle) {
                console.log(key)
                sendRequest(key, values[0] + ',' + values[1]);
            });

            leftSlider.addEventListener('change', function(e) {
                e.preventDefault();
                console.log('left');
                domElement.noUiSlider.set([this.value, null]);
                // sendRequest(key, domElement.noUiSlider.get()[0] + ',' + this.value);
            });

            rightSlider.addEventListener('change', function(e) {
                e.preventDefault();
                console.log('fight');
                domElement.noUiSlider.set([null, this.value]);
                // sendRequest(key, this.value + ',' + domElement.noUiSlider.get()[1]);
            });
        })(key, leftSlider, rightSlider, domElement);
    }

    //Radius range filters
    var snapSlider = document.getElementById('radius-rangefilter');
    var model = /\w+?(?=\/search\/)/.exec(window.location.href);
    var max = (model=='realty') ? 105 : 1000;
    noUiSlider.create(snapSlider, {
        start: parameters.distance ? parameters.distance : [max],
        range: {'min': [5], 'max': [max]},
        step: 5
    });

    var snapSliderValueElement = document.getElementById('radius-step-value');

    snapSlider.noUiSlider.on('update', function(values, handle) {
        if (values[0] != max) {
            var data = values[handle] + ' km';
        } else {
            var data = gettext('WORLDWIDE');
        }
        snapSliderValueElement.innerHTML = data;
    });

    snapSlider.noUiSlider.on('change', function(values, handle) {
        var location = getCookie('location');
        if (location) {
            $('#search-list-result').empty();
            if (values[0] == max) {
                delete parameters.distance;
                sendRequest();
            } else {
                sendRequest('distance', values[0]);
            }
        } else {
            alert(gettext('Specify the location where you want to search'));
            snapSlider.noUiSlider.set(max);
        }
    });

    //filters  countries
    var country_filter = $(".chosen-select-country");
    country_filter.select2().val(parameters.countries ? parameters.countries.split(',') : []).trigger("change");
    $('.chosen-wrapper').css("display", "block");
    $('span.select2-container').css('width', '100%');

    country_filter.on('change', function(evt) {
        $('#search-list-result').empty();
        delete parameters.city;
        if (country_filter.val() == null) {
            delete parameters.countries;
            sendRequest();
        } else {
            sendRequest('countries', (country_filter.val()).join());
        }
    });

    locateMeBtn.on('click', function() {
        locateMeBtn.text(gettext('locating...'));
        getLocationWithAPI(setLocateMe);
    });

    //scroll pagination
    function scrollBehavior(content) {
        if ('append-fragments' in content) {
            if ((
                    content['append-fragments']['#search-list-result']
                        .match(/<div class="c-content-product-2 c-bg-white">/g)
                    || content['append-fragments']['#search-list-result']
                        .match(/<div class="c-content-product-2 c-bg-white c-border">/g) || []
                ).length < 10) {
                disableScroll = false;
                if (parameters.page) {
                    if (Number(parameters.page) > 2) { parameters.page--; }
                    else { delete parameters.page; }
                }
            } else {
                disableScroll = true;
            }
        } else if ('inner-fragments' in content) {
            if (( content['inner-fragments']['#search-list-result']
                        .match(/<div class="c-content-product-2 c-bg-white">/g)
                    ||
                    content['inner-fragments']['#search-list-result']
                        .match(/<div class="c-content-product-2 c-bg-white c-border">/g)
                    || []
                ).length < 10) {
                disableScroll = false;
            } else {
                disableScroll = true;
            }
        }
    }

    var win = $(window);
    win.scroll(function() {
        clearTimeout($.data(this, "scrollCheck"));
        $.data(this, "scrollCheck", setTimeout(function () {
            var height = resultBlock.height() + resultBlock.offset().top - win.height() - 150;
            if ((win.scrollTop() >= height) && semaphore == true ) {
                parameters.page ? parameters.page++ : parameters.page = 2;
                semaphore = false;
                if (disableScroll) {
                    sendRequest('scroll', true);
                }
            }
        }, 250));
    });

    $('#search-filters').on('change', '#property_type', function() {
        delete parameters.page;
        $('#search-list-result').empty();
        delete parameters['property_type_type'];
        if (this.value == '0') {
            delete parameters['property_type'];
            sendRequest();
        } else {
            sendRequest('property_type', this.value);
        }
    });

    $('#search-filters').on('change', '#city', function() {
        delete parameters.page;
        $('#search-list-result').empty();
        if (this.value == 0) {
            delete parameters['city'];
            sendRequest();
        } else {
            sendRequest('city', this.value);
        }
    });

    $('#search-filters-toggler').show().on('click', function() {
        $('#search-filters').modal('show');
    });


    setLocationFromCookie();
    var locationInput = $('#location-input');

    $('#change-location').on('click', function () {
        if (window.google == undefined) {
            loadGoogleMapsAPI(setLocationAutocomplete);
        } else {
            setLocationAutocomplete();
        }
        locationInput.val('');
        locationInput.show();
    });

    function setLocationAutocomplete() {
        locationAutocomplete= new google.maps.places.Autocomplete(
            locationInput[0], {types: ['geocode']}
        );
        locationAutocomplete.addListener('place_changed', function () {
            var place = locationAutocomplete.getPlace();
            setLocationCookie(place, setLocateMe);
            locationInput.hide();
        });
    }

    function setLocationFromCookie() {
        var location = getCookie('location');
        if (location) {
            $('#location-span').text(JSON.parse(decodeURI(location)).formatted_address);
        }
    }

    function setLocateMe(location) {
        locateMeBtn.text(locateMeBtn.initial_text);
        if (location) $('#location-span').text(location.formatted_address);
    }
    add_options();
    function add_options() {
        var style = getCookie('style');
        var select = $('#show').find("#show");
        select.empty();
        delete parameters.show;
        if (style && style == 'grid') {
            var options = [32, 64, 128];
            $.each(options, function (i, item) {
                select.append($('<option>', {
                    value: item,
                    text: item
                }));
            });
        }
        else {
            var options = [10, 25, 50, 100, 500];
            $.each(options, function (i, item) {
                select.append($('<option>', {
                    value: item,
                    text: item
                }));
            });
        }
    }

}
