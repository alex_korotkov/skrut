from django.db.models.signals import pre_delete
from classifieds.models import Classified
from jobs.models import Job
from luxdjango.utils import facebook_post_delete
from realty.models import Realty
from vehicles.models import Car, Moto


def model_post_delete(sender, instance, **kwargs):
    facebook_post_delete(instance)


pre_delete.connect(model_post_delete, sender=Classified)
pre_delete.connect(model_post_delete, sender=Car)
pre_delete.connect(model_post_delete, sender=Moto)
pre_delete.connect(model_post_delete, sender=Realty)
pre_delete.connect(model_post_delete, sender=Job)
