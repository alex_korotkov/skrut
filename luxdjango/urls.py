# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views import defaults as default_views
from django.views.generic import TemplateView
from django.views.i18n import javascript_catalog

from geoip.views import GetLocationByIPView
from vehicles.views import HomeView, ImageAsyncUpload

js_info_dict = {
    'packages': ('luxdjango.static.assets.base.js.modules',)
}

urlpatterns = [
    url(r'^$', HomeView.as_view(), name="home"),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^jsi18n/$', javascript_catalog, js_info_dict, name='javascript-catalog'),
    url(r'^rosetta/', include('rosetta.urls')),

    url(r'^about/$', TemplateView.as_view(template_name='pages/about.html'), name="about"),

    # Django Admin, use {% raw %}{% url 'admin:index' %}{% endraw %}
    url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin/', include(admin.site.urls)),

    # User management
    url(r'^users/', include("luxdjango.users.urls", namespace="users")),
    url(r'^accounts/', include('allauth.urls')),

    url(r'^vehicles/', include('vehicles.urls', namespace="vehicles")),
    url(r'^realty/', include('realty.urls', namespace="realty")),
    url(r'^jobs/', include('jobs.urls', namespace="jobs")),

    url(r'^get-location-by-ip/', GetLocationByIPView.as_view()),

    url(r'^classifieds/', include('classifieds.urls', namespace="classifieds")),
    url(r'^image-upload/$', ImageAsyncUpload.as_view(), name="image_async_upload"),

    url(r'^search/', include('overall_search.urls', namespace="overallsearch")),


] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)\
  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# from django.contrib.staticfiles.urls import staticfiles_urlpatterns
if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    # urlpatterns += staticfiles_urlpatterns()
    # urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += [
        url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception("Bad Request!")}),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception("Permission Denied")}),
        url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception("Page not Found")}),
        url(r'^500/$', default_views.server_error),
    ]

