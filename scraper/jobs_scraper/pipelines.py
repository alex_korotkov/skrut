# -*- coding: utf-8 -*-
import logging
from jobs.models import Employer, Job
from vehicles.models import Country
from realty.models import City
from jobs.models import Category, Job, Contract, WorkHours

cat_dict = {x.id: x.name_variants for x in Category.objects.all() if x.name_variants}

COUNTRIES = [
    {'id': x[0], 'names': [y.lower() for y in x[1:] if y]}
    for x in Country.objects.all().values_list('id', 'name_en', 'name_fr')
    ]
categories = list(Category.objects.filter(name_variants__isnull=False).only('id', 'name_variants'))
DEFAULT_CATEGOTY_ID = Category.objects.get(name_en='Other')

logger = logging.getLogger('jobs_pipline')


class ObjModelPipeline(object):
    def process_item(self, item, spider):
        if item.get('update'):
            return self.process_item_update(item, spider)
        else:
            return self.process_new_item(item, spider)

    def process_new_item(self, item, spider):
        logger.info('/// process new item ///')
        values_dict = {'url': item['url']}
        mm_fields = {'category': [], 'work_hours': [], 'contract': []}

        if item.get('employer') and item['employer'].get('name'):
            if item['employer'].get('country'):
                item['employer']['country'] = item['country'].lower()
                for c in COUNTRIES:
                    for cn in c['names']:
                        if cn in item['employer']['country']:
                            item['employer']['country_id'] = c['id']
            if 'country' in item['employer']:
                del item['employer']['country']
            employer = Employer.objects.filter(name__iexact=item['employer']['name']).first()
            if not employer:
                employer = Employer.objects.create(**item['employer'])
            values_dict['employer_id'] = employer.id

        for cat_id in cat_dict:
            for cat in item['category']:
                if cat in cat_dict[cat_id]:
                    mm_fields['category'].append(cat_id)

        if item.get('country_id'):
            values_dict['country_id'] = item['country_id']
        elif item.get('country'):
            item['country'] = item['country'].lower()
            for c in COUNTRIES:
                for cn in c['names']:
                    if cn in item['country']:
                        values_dict['country_id'] = c['id']

        if item.get('city_id'):
            values_dict['city_id'] = item['city_id']
        elif item.get('city'):
            if values_dict.get('country_id'):
                city = City.objects.filter(
                    name__iexact=item['city'], country_id=values_dict['country_id'],
                ).first()
                if not city:
                    city = City.objects.create(
                        name=item['city'], country_id=values_dict['country_id'],
                    )
                values_dict['city_id'] = city.id
            else:
                city = City.objects.filter(name__iexact=item['city']).first()
                if city:
                    values_dict['city_id'] = city.id
                    values_dict['country_id'] = city.country_id

        if not values_dict.get('country_id'):
            if item['employer'].get('country_id'):
                values_dict['country_id'] = item['employer']['country_id']
                if item['employer'].get('city_id'):
                    values_dict['city_id'] = item['employer']['city_id']
            else:
                values_dict['country_id'] = item['default_country_id']

        if item.get('address'):
            values_dict['address'] = item['address']

        values_dict['title'] = item['title']
        values_dict['description'] = item['description']

        if item.get('candidate_level'):
            pass

        if item.get('work_hours'):
            for wh in item['work_hours']:
                work_hours = WorkHours.objects.filter(work_hours_fr__iexact=wh).first()
                if not work_hours:
                    work_hours = WorkHours.objects.create(work_hours_fr=wh)
                mm_fields['work_hours'].append(work_hours.id)

        if item.get('contract'):
            for c in item['contract']:
                contract = Contract.objects.filter(contract_fr__iexact=c).first()
                if not contract:
                    contract = Contract.objects.create(contract_fr=c)
                mm_fields['contract'].append(contract.id)

        if item.get('category'):
            for icat in item['category']:
                icat = icat.lower()
                for dbcat in categories:
                    if icat in dbcat.name_variants:
                        mm_fields['category'].append(dbcat.id)
                        break
        if not mm_fields['category']:
            mm_fields['category'].append(DEFAULT_CATEGOTY_ID)

        job = Job.objects.create(**values_dict)

        for f in mm_fields:
            if mm_fields[f]:
                relation_manager = getattr(job, f)
                relation_manager.add(*mm_fields[f])

    def process_item_update(self, item, spider):
        item['model_obj'].update_update_date()
