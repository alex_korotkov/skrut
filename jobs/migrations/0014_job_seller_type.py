# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0013_employer_phones'),
    ]

    operations = [
        migrations.AddField(
            model_name='job',
            name='seller_type',
            field=models.PositiveIntegerField(default=1, verbose_name='Seller type', choices=[(1, 'Private seller'), (2, 'Professional  seller')]),
        ),
    ]
