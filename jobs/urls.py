# -*- coding: utf-8 -*-
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^search/', views.SearchView.as_view(), name="search"),
    url(r'^search-ajax/', views.ElasticSearchAjax.as_view(), name='search-ajax'),
    url(r'^details/(?P<pk>\d+)/$', views.DetailJobView.as_view(), name='details'),
    url(r'^add/', views.AddJobView.as_view(), name="add-job"),
    url(r'^get_city/', views.get_city, name='get_city'),
    url(r'^get_employer/', views.get_employer, name='get_employer'),
    url(r'^edit/(?P<pk>\d+)/$', views.EditJobView.as_view(), name='edit'),
]
