from haystack import indexes

from .models import Realty


class RealtyIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    property_type = indexes.IntegerField(model_attr='property_type_id', faceted=True)
    property_type_type = indexes.IntegerField(model_attr='property_type_type_id', default=None)
    deal = indexes.IntegerField(model_attr='deal')
    country = indexes.IntegerField(model_attr='country_id', faceted=True)
    price_index = indexes.IntegerField(model_attr='price', default=None)
    year_index = indexes.IntegerField(model_attr='year_built', default=None)
    date_index = indexes.DateTimeField(model_attr='creation_date')
    energy_class = indexes.CharField(model_attr='energy_class', default=None)
    city = indexes.IntegerField(model_attr='city_id', default=None)
    location_index = indexes.LocationField(model_attr='get_location', faceted=True, default=None)
    big_category = indexes.CharField(model_attr='big_category', faceted=True)
    rooms = indexes.IntegerField(model_attr='rooms', default=None)
    area = indexes.IntegerField(model_attr='area', default=None)
    land_area = indexes.IntegerField(model_attr='land_area', default=None)

    def get_model(self):
        return Realty

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(is_active=True)
