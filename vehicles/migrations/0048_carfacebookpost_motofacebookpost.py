# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('AdminConfig', '0004_facebookpageaccesstoken'),
        ('vehicles', '0047_auto_20161223_1302'),
    ]

    operations = [
        migrations.CreateModel(
            name='CarFacebookPost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('post_id', models.CharField(max_length=50, verbose_name='post id')),
                ('access_token', models.ForeignKey(to='AdminConfig.FacebookPageAccessToken')),
                ('car', models.ForeignKey(related_name='facebookpost_set', to='vehicles.Car')),
            ],
        ),
        migrations.CreateModel(
            name='MotoFacebookPost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('post_id', models.CharField(max_length=50, verbose_name='post id')),
                ('access_token', models.ForeignKey(to='AdminConfig.FacebookPageAccessToken')),
                ('moto', models.ForeignKey(related_name='facebookpost_set', to='vehicles.Moto')),
            ],
        ),
    ]
