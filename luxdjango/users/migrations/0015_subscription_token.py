# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0014_auto_20161221_1256'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscription',
            name='token',
            field=models.ForeignKey(blank=True, to='users.Token', null=True),
        ),
    ]
