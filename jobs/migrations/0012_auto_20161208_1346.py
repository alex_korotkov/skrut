# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0011_job_update_date'),
    ]

    operations = [
        migrations.RenameField(
            model_name='employer',
            old_name='phones',
            new_name='phones2',
        ),
    ]
