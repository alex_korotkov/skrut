# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0044_auto_20161125_1322'),
    ]

    operations = [
        migrations.AddField(
            model_name='country',
            name='calling_code',
            field=models.CharField(default='a', max_length=10),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='basecolor',
            name='color',
            field=models.CharField(max_length=50, blank=True),
        ),
        migrations.AlterField(
            model_name='basecolor',
            name='color_en',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='basecolor',
            name='color_fr',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
