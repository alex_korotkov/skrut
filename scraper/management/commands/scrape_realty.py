import os, scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from django.core.management.base import BaseCommand, CommandError

from scraper.realty_scraper import spiders


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        os.environ['SCRAPY_PROJECT'] = 'realty'
        process = CrawlerProcess(get_project_settings())
        process.crawl(spiders.ImmotopSpider)
        process.crawl(spiders.AthomeSpider)
        process.crawl(spiders.WortimmoSpider)
        process.crawl(spiders.ImmostarSpider)
        process.start()
