# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0011_job_update_date'),
        ('vehicles', '0044_auto_20161125_1322'),
        ('realty', '0024_realty_update_date'),
        ('classifieds', '0009_classified_update_date'),
        ('users', '0010_auto_20161005_1332'),
    ]

    operations = [
        migrations.AddField(
            model_name='token',
            name='car',
            field=models.ForeignKey(blank=True, to='vehicles.Car', null=True),
        ),
        migrations.AddField(
            model_name='token',
            name='classified',
            field=models.ForeignKey(blank=True, to='classifieds.Classified', null=True),
        ),
        migrations.AddField(
            model_name='token',
            name='job',
            field=models.ForeignKey(blank=True, to='jobs.Job', null=True),
        ),
        migrations.AddField(
            model_name='token',
            name='moto',
            field=models.ForeignKey(blank=True, to='vehicles.Moto', null=True),
        ),
        migrations.AddField(
            model_name='token',
            name='realty',
            field=models.ForeignKey(blank=True, to='realty.Realty', null=True),
        ),
    ]
