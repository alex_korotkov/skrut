# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('AdminConfig', '0003_auto_20161213_1638'),
    ]

    operations = [
        migrations.CreateModel(
            name='FacebookPageAccessToken',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('facebook_access_token', models.CharField(max_length=100)),
                ('post_on_facebook', models.NullBooleanField(default=True)),
            ],
        ),
    ]
