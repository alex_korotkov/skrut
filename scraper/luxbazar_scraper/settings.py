from scraper.settings_common import *

BOT_NAME = 'scraper_luxbazar'

SPIDER_MODULES = ['scraper.luxbazar_scraper.spiders']
NEWSPIDER_MODULE = 'scraper.luxbazar_scraper.spiders'

ITEM_PIPELINES = {
    'scraper.luxbazar_scraper.pipelines.LuxbazarScraperPipeline': 300,
    'scraper.luxbazar_scraper.pipelines.StoreImgPipeline': 301,
    'scraper.luxbazar_scraper.pipelines.SaveImgModelPipeline': 302
}
