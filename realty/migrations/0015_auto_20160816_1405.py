# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0014_auto_20160815_1016'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realty',
            name='garden',
            field=models.DecimalField(null=True, max_digits=7, decimal_places=2, blank=True),
        ),
        migrations.AlterField(
            model_name='realty',
            name='year_built',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='Year built', validators=[django.core.validators.MaxValueValidator(2016), django.core.validators.MinValueValidator(1600)]),
        ),
    ]
