import os, scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from django.core.management.base import BaseCommand, CommandError

from scraper.vehicles_scraper import spiders


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        os.environ['SCRAPY_PROJECT'] = 'vehicles'
        process = CrawlerProcess(get_project_settings())
        process.crawl(spiders.LuxautoSpider)
        process.crawl(spiders.BazarSpider)
        process.crawl(spiders.AutofinderSpider)
        process.start()
