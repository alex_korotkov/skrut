from storages.backends.s3boto import S3BotoStorage

StaticRootS3BotoStorage = lambda: S3BotoStorage(location='static')
MediaRootS3BotoStorage  = lambda: S3BotoStorage(location='media')
# Then in your settings.py:

# DEFAULT_FILE_STORAGE = 'myproject.s3utils.MediaRootS3BotoStorage'
# STATICFILES_STORAGE = 'myproject.s3utils.StaticRootS3BotoStorage'
