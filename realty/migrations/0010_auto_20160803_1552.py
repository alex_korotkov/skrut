# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0009_auto_20160803_1322'),
    ]

    operations = [
        migrations.AddField(
            model_name='realty',
            name='creation_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='realty',
            name='year_build',
            field=models.PositiveIntegerField(blank=True, null=True, validators=[django.core.validators.MaxValueValidator(2016), django.core.validators.MinValueValidator(1900)]),
        ),
    ]
