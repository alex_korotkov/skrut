from haystack.routers import BaseRouter


class JobsRouter(BaseRouter):
    def for_write(self, **hints):
        return 'jobs'

    def for_read(self, **hints):
        return 'jobs'
