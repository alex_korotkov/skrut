# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('classifieds', '0007_classified_price_history'),
    ]

    operations = [
        migrations.AddField(
            model_name='classified',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
