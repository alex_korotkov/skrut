# -*- coding: utf-8 -*-
import datetime, re
from django.db import models
from django.contrib.gis.geos import Point
from django.contrib.postgres.fields import ArrayField
from django.core.urlresolvers import reverse
from django.core.validators import MaxValueValidator, MinValueValidator, RegexValidator
from django.utils import timezone
from django.utils.translation import ugettext as _

from AdminConfig.models import FacebookPageAccessToken
from luxdjango.utils import img_upload_to, get_geo_point, post_to_facebook
from vehicles.models import Country, SELLER_CHOICES


class PropertyType(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class PropertyTypeType(models.Model):
    name = models.CharField(max_length=50)
    property_type = models.ForeignKey(PropertyType, on_delete=models.CASCADE)

    def __unicode__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=50)
    country = models.ForeignKey(Country)
    latitude = models.FloatField(null=True, blank=True)
    longitude = models.FloatField(null=True, blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Seller(models.Model):
    name = models.CharField(max_length=60, null=True)
    email = models.EmailField(null=True, blank=True)

    def __unicode__(self):
        return self.name


class Realty(models.Model):
    DEAL_CHOICES = (
        (1, _('sale')),
        (2, _('rent'))
    )
    ENERGY_CHOICES = (
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('D', 'D'),
        ('E', 'E'),
        ('F', 'F'),
        ('G', 'G'),
        ('H', 'H'),
        ('I', 'I'))

    property_type = models.ForeignKey(PropertyType, on_delete=models.PROTECT)  ##
    property_type_type = models.ForeignKey(
        PropertyTypeType, null=True, blank=True, on_delete=models.PROTECT
    )
    deal = models.PositiveIntegerField(choices=DEAL_CHOICES, blank=False, default=1)
    rooms = models.PositiveIntegerField(null=True, blank=True)
    area = models.PositiveIntegerField(null=True, blank=True)  # m2
    land_area = models.DecimalField(null=True, decimal_places=2, max_digits=7, blank=True)  # ares
    price = models.PositiveIntegerField(null=True, blank=True)  # euro
    price_history = ArrayField(models.IntegerField(), blank=True, null=True)
    energy_class = models.CharField(max_length=1, choices=ENERGY_CHOICES, blank=True)
    termal_protection_class = models.CharField(max_length=1, choices=ENERGY_CHOICES, blank=True)
    country = models.ForeignKey(Country, on_delete=models.PROTECT)  ##
    city = models.ForeignKey(City, null=True, blank=True, on_delete=models.PROTECT)  ##
    address = models.CharField(max_length=100, null=True, blank=True)
    latitude = models.FloatField(null=True, blank=True)  #
    longitude = models.FloatField(null=True, blank=True)  #
    year_built = models.PositiveIntegerField(_("Year built"), null=True, blank=True)
    new_building = models.NullBooleanField()
    url = models.CharField(max_length=300, null=True, blank=True)
    part_of = models.ForeignKey('self', null=True, blank=True, on_delete=models.SET_NULL)  #
    has_child = models.NullBooleanField()  #
    floor = models.PositiveIntegerField(null=True, blank=True)
    total_floors = models.PositiveIntegerField(null=True, blank=True)
    description = models.TextField(blank=True)  ##
    bathroom = models.PositiveIntegerField(_("Bathrooms"), null=True, blank=True)
    garden = models.DecimalField(null=True, decimal_places=2, max_digits=7, blank=True)
    terrace = models.PositiveIntegerField(null=True, blank=True)
    balcony = models.PositiveIntegerField(null=True, blank=True)
    garage = models.PositiveIntegerField(null=True, blank=True)
    seller = models.ForeignKey(Seller, null=True, blank=True, on_delete=models.SET_NULL)
    phones = ArrayField(
        models.CharField(max_length=24, validators=[
            RegexValidator(r'^[\s \+ 0-9]{6,17}$', 'Enter a valid phone number.')
        ]),
        null=True, blank=True
    )
    update_date = models.DateTimeField(default=timezone.now)
    creation_date = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey('users.User', null=True, blank=True)
    is_active = models.BooleanField(default=True)
    seller_type = models.PositiveIntegerField(_('Seller type'), choices=SELLER_CHOICES, default=1)

    @property
    def domain(self):
        if self.url:
            domain = re.search(r'\/\/([^\/]+)', self.url)
            if domain:
                return domain.groups()[0]
        return ''

    @property
    def big_category(self):
        return u'realty'

    def get_location(self):
        if self.longitude and self.latitude:
            return get_geo_point(longitude=self.longitude,
                                 latitude=self.latitude)
        return None

    def get_absolute_url(self):
        return reverse('realty:details', kwargs={'pk': self.id})

    def update_price(self, new_price):
        if self.price != new_price:
            if self.price_history:
                index = len(self.price_history) - 1
                if self.price and self.price != self.price_history[index]:
                    self.price_history.append(self.price)
                elif new_price == self.price_history[index]:
                    del self.price_history[index]
            elif self.price:
                self.price_history = [self.price]
            self.price = new_price
            self.save()

    def update_update_date(self):
        self.update_date = timezone.now()
        self.save()

    def post_on_facebook(self):
        property_type = self.property_type_type.name if self.property_type_type else self.property_type.name
        post_to_facebook(
            '{} for {} in {}, {}'.format(property_type, self.get_deal_display(), self.country, self.city),
            self.get_absolute_url(),
            self.description,
            self,
            self.images.all()[0].image.url if self.images.all() else None,
        )


class RealtyImage(models.Model):
    realty = models.ForeignKey(Realty, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(upload_to=img_upload_to)


class RealtyFacebookPost(models.Model):
    realty = models.ForeignKey(Realty, on_delete=models.CASCADE, related_name='facebookpost_set')
    post_id = models.CharField(_('post id'), max_length=50)
    access_token = models.ForeignKey(FacebookPageAccessToken, on_delete=models.CASCADE)
