# -*- coding: utf-8 -*-
import simplejson as json
import datetime, hashlib, os, shutil
from AdminConfig.models import HideContacts, FacebookPageAccessToken
from django.conf import settings
from django.contrib.gis.geos import Point
from django.core.files.base import File
from django.utils.translation import ugettext as _
from django.contrib.sites.models import Site
import facebook
from django.conf import settings


def get_user_data(model, pk):
    result = {}
    item = model.objects.get(pk=pk)
    for i in HideContacts.objects.all():
        if i.domain == item.domain and i.hide_contacts:
            result[_("url")] = '<a href="' + item.url + '" target="_blank">' + item.domain + '</a>'
            return json.dumps(result)
    if hasattr(item, 'phones') and item.phones:
        result['phones'] = item.phones
    if hasattr(item, 'email') and item.email:
        result['email'] = item.email
    if item.author:
        if not result.get('email'):
            email = item.author.email
            if email:
                result['email'] = email
        if not result.get('phones'):
            phones = item.author.phones
            if phones:
                result['phones'] = phones
    else:
        if hasattr(item, 'seller') and item.seller:
            if not result.get('email'):
                email = item.seller.email if hasattr(item.seller, 'email') else None
                if email:
                    result['email'] = email
            if not result.get('phones'):
                phones = item.seller.phones if hasattr(item.seller, 'phones') else None
                if phones:
                    result['phones'] = phones
        elif hasattr(item, 'employer') and item.employer:
            if not result.get('email'):
                email = item.employer.email
                if email:
                    result['email'] = email
            if not result.get('phones'):
                phones = item.employer.phones
                if phones:
                    result['phones'] = phones
        if not result:
            result[_("url")] = '<a href="' + item.url + '" target="_blank">' + item.domain + '</a>'
    for key in result.keys():
        if key == 'phones':
            if not item.author or not item.author.hide_phones:
                result[_("Phones")] = result[key]
            result.pop(key)
        elif key == 'email':
            result[_("Email")] = result[key]
            result.pop(key)
    data = json.dumps(result)
    return data


def get_hash_name(filename):
    ext = filename.split('.')[-1]
    repr(filename)
    hash_name = hashlib.md5(filename.encode('utf-8')).hexdigest()
    return '%s.%s' % (hash_name, ext)


def img_upload_to(instance, filename, folder_name=None, unique_name=False):
    date = datetime.datetime.now()
    if instance:
        folder_name = instance.__class__.__name__.lower().replace('image', '') + u'-us'
    if not unique_name:
        # add unique key as users can upload files with same names
        filename = u'%s%s' % (date, filename)
    hash_name = get_hash_name(filename)
    return '%s/%s/%s/%s/%s/%s/%s' % (
        folder_name,
        date.year, date.month, date.day,
        hash_name[:2], hash_name[2:4], hash_name
    )


def get_geo_point(longitude, latitude):
    if (
        longitude.__class__.__name__ in ('str', 'unicode')
        or latitude.__class__.__name__ in ('str', 'unicode')
    ):
        longitude = float(longitude)
        latitude = float(latitude)
    return Point(longitude, latitude)


def handnle_form_images(obj, img_related_key, csrf, images_tmp, images_files, images_saved=None):
    RelationManager = getattr(obj, img_related_key)
    ImageModel = RelationManager.model
    fk_field = obj._meta.fields_map[img_related_key.replace('_set', '')].field.name
    folder = os.path.join(settings.MEDIA_ROOT, 'tmp', csrf)

    # remove removed images
    old_images = RelationManager.all().only('id')
    if images_saved:
        images_saved = [int(x) for x in images_saved]
        for img in old_images:
            if img.id not in images_saved:
                img.delete()
    elif old_images:
        old_images.delete()

    # save directly uploaded images (for old browsers)
    if images_files:
        images = [
            ImageModel(**{'image': File(x), fk_field: obj})
            for x in images_files if x.content_type.startswith('image')
            ]
        RelationManager.bulk_create(images)

    # save preuploaded images
    elif os.path.exists(folder):
        files = []
        for filename in os.listdir(folder):
            if filename in images_tmp:
                files.append(File(open(os.path.join(folder, filename))))
        images = [ImageModel(**{fk_field: obj, 'image': x}) for x in files]
        RelationManager.bulk_create(images)
        map(lambda f: f.close(), files)
        shutil.rmtree(folder)


def get_tmp_image_urls(csrf):
    tmp_folder = os.path.join(settings.MEDIA_ROOT, 'tmp', csrf)
    if os.path.exists(tmp_folder):
        tmp_url = '%stmp/%s/' % (settings.MEDIA_URL, csrf)
        return [tmp_url + x for x in os.listdir(tmp_folder)]
    else:
        return []


def post_to_facebook(name, link, description, item, picture=None):
    pages = FacebookPageAccessToken.objects.filter(post_on_facebook=True)
    if not pages:
        return

    if not picture:
        picture = '/static/assets/base/img/nopic.png'
    domain = Site.objects.get_current().domain
    path = 'https://' + domain
    attachment = {
        'name': name,
        'link': path + link,
        'caption': domain,
        'description': description,
        'picture': path + picture,
    }
    for i in pages:
        api = facebook.GraphAPI(i.facebook_access_token)
        try:
            fb_response = api.put_wall_post(message=' ', attachment=attachment)
            item.facebookpost_set.create(post_id=fb_response['id'], access_token=i)
        except:
            pass


def facebook_post_delete(item):
    ids = item.facebookpost_set.all()
    for i in ids:
        api = facebook.GraphAPI(i.access_token.facebook_access_token)
        api.delete_object(id=i.post_id)
