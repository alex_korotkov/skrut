$(document).ready(function () {
    $('#del-subscr').on('click', function () {
        var subscriptions = $('input[name="subscr"]:checked');
        var ids_sub = [];
        subscriptions.each(function () {
            ids_sub.push($(this).data('id'));
            $('#' + $(this).data('id')).hide()
        });
        if (ids_sub.length) {
            $.ajax({
                method: 'post',
                url: '/users/delete-subscriptions/',
                dataType: 'json',
                data: {subscriptions: JSON.stringify(ids_sub)}
            }).done(function () {
                if ($('input[name="subscr"]').length == 0) {
                    $('#subscr-list').hide();
                    $('#no-subscr').show();
                }
            }).fail(function (resp) {
                alert('Error!');
                console.log(resp);
            });
        }
    });
    $('#del-posts').on('click', function () {
        var posts = $('input[name="posts"]:checked');
        if (!$('input[name="posts"]')) {
            $('.post-name').hide();
        }
        var ids = [];
        posts.each(function () {
            ids.push($(this).data('id'));
            $('#' + $(this).data('id')).hide()
        });

        if (ids.length) {
            $.ajax({
                method: 'post',
                url: '/users/delete-posts/',
                dataType: 'json',
                data: {posts: JSON.stringify(ids)}
            }).done(function () {
                if ($('input[name="posts"]').length == 0) {
                    $('#post-list').hide();
                    $('#no-posts').show();
                }
            }).fail(function (resp) {
                alert('Error!');
                console.log(resp);
            });
        }
    });
    $('.update-post').on('click', function () {
        $(this).hide()
        $.ajax({
            method: 'post',
            url: '/users/update-post/',
            dataType: 'json',
            data: {post: JSON.stringify($(this).parents('.c-checkbox').attr('id'))}
        }).fail(function (resp) {
            alert('Error!');
            console.log(resp);
        });
    });
});