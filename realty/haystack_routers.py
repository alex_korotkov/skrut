from haystack.routers import BaseRouter


class RealtyRouter(BaseRouter):
    def for_write(self, **hints):
        return 'realty'

    def for_read(self, **hints):
        return 'realty'