# -*- coding: utf-8 -*-
import scrapy, re
from ..items import JobItem

from scrapy.shell import inspect_response

from vehicles.models import Country
from realty.models import City
from jobs.models import Job

COUNTRIES = [
    {'id': x[0], 'names': [y.lower() for y in x[1:] if y]}
    for x in Country.objects.all().values_list('id', 'name_en', 'name_fr')
]


class FrJobsSpider(scrapy.Spider):
    name = 'frjobs'
    allowed_domains = ['fr.jobs.lu']
    start_urls = [
        'http://fr.jobs.lu/Jobs.aspx?Categories=Type+d%27emploi&Regions=Lieu+de+travail'
    ]

    def parse(self, response):
        self.logger.info('///// parse list /////');
        link_list = response.xpath('//div[@class="search_results"]/article//a/@href').extract()
        url_list = [response.urljoin(x).replace('http', 'https') for x in link_list]
        for url in url_list:
            if Job.objects.filter(url=url).exists():
                self.logger.info('/// already exists ///')
                item = JobItem()
                item['model_obj'] = Job.objects.filter(url=url).first()
                item['update'] = True
                yield item
            else:
                yield scrapy.Request(url, callback=self.parse_detail)

        next_page = response.xpath('//a[@class="pagination_next"]/@href').extract_first()
        if next_page:
            self.logger.info('//////////// got to next page //////////////')
            yield scrapy.Request(response.urljoin(next_page), callback=self.parse)



    def parse_detail(self, response):
        self.logger.info('/// parse detail ///')
        item = JobItem()
        item['url'] = response.url
        item['default_country_id'] = 35  # Luxembourg

        item['employer'] = {}
        employer_block = response.xpath('//td[@class="details"]')
        employer_dict = [
            {'field': 'name', 'selector': './/span[@itemprop="name"]/text()'},
            {'field': 'address', 'selector': './/span[@itemprop="address"]/text()'},
            {'field': 'site', 'selector': './/a[@itemprop="url"]/@href'},
            {'field': 'email', 'selector': './/a[@itemprop="email"]/text()'},
        ]
        for x in employer_dict:
            val = employer_block.xpath(x['selector']).extract_first()
            val = val.strip() if val else None
            if val and val != 'http://':
                item['employer'][x['field']] = val
        phones = employer_block.xpath('.//span[@itemprop="telephone"]/text()').extract_first()
        phones = phones.strip().split(',') if phones else None
        if phones:
            item['employer']['phones'] = phones

        if item['employer'].get('address'):
            for c in COUNTRIES:
                for cn in c['names']:
                    if cn in item['employer']['address'].lower():
                        item['employer']['country_id'] = c['id']

        detail_block = response.xpath('//div[@class="job_view"]')
        item['title'] = detail_block.xpath('.//div[@itemprop="title"]/text()').extract_first()
        description = detail_block.xpath('.//div[@itemprop="description"]//text()').extract()
        item['description'] = ' '.join(description)
        city = detail_block.xpath('.//td[@itemprop="jobLocation"]/text()').extract_first()
        if city and u'définir' not in city:
            if re.search(r'[\s\d,/-]', city):
                city_filter = {'country_id': item['country_id']} if item.get('country_id') else {}
                city = city.lower().replace('luxemburg', 'luxembourg')
                lcity = city.lower().split(',')
                check_list = [x.strip() for x in lcity]
                for c in lcity:
                    check_list.append(re.sub(r'-{2,}', '-', re.sub(r'[\s/]', '-', c)))
                cities = City.objects.filter(**city_filter).only('id', 'name')
                city_matches = []
                for c in cities:
                    stop = False
                    for raw_city in check_list:
                        if c.name.lower() in raw_city or raw_city in c.name.lower():
                            city_matches.append(c)
                            if c.name != 'Luxembourg':
                                stop = True
                            break
                    if stop:
                        break
                if city_matches:
                    item['city_id'] = city_matches[-1].id
                    if not item.get('country_id'):
                        item['country_id'] = city_matches[-1].country_id
                        if not item['employer'].get('country_id'):
                            item['employer']['country_id'] = city_matches[-1].country_id
            else:
                if city.lower() == 'luxemburg':
                    item['city'] = 'Luxembourg'
                else:
                    item['city'] = city

        item['category'] = detail_block.xpath('.//td[@itemprop="occupationalCategory"]/a/text()').extract()
        contract = detail_block.xpath('.//span[@itemprop="employmentType"]/text()').extract_first()
        item['contract'] = [x.strip() for x in contract.split(',') if x.strip()]
        work_hours = detail_block.xpath('.//span[@itemprop="workHours"]/text()').extract_first()
        item['work_hours'] = [x.strip() for x in work_hours.split(',') if x.strip()]

        yield item

