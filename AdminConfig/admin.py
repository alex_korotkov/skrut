from django.contrib import admin
from AdminConfig.models import Template
from AdminConfig.models import HideContacts
from AdminConfig.models import FacebookPageAccessToken
from django import forms
from ckeditor.widgets import CKEditorWidget


class HideContactsAdmin(admin.ModelAdmin):
    list_display = (
        'domain', 'hide_contacts'
    )


class FacebookPageAccessTokenAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'facebook_access_token', 'post_on_facebook'
    )


class TemplateAdminForm(forms.ModelForm):
    template = forms.CharField(required=False, widget=CKEditorWidget())

    class Meta:
        model = Template
        fields = ['template', 'name']


class TemplateAdmin(admin.ModelAdmin):
    form = TemplateAdminForm


admin.site.register(Template, TemplateAdmin)

admin.site.register(HideContacts, HideContactsAdmin)

admin.site.register(FacebookPageAccessToken, FacebookPageAccessTokenAdmin)
