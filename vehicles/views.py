# -*- coding: utf-8 -*-

import copy, urllib, os

import simplejson as json
from allauth.account.models import EmailAddress
from collections import OrderedDict

from django.contrib.sites.models import Site
from django_ajax.mixin import AJAXMixin
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.utils.translation import ugettext as _
from django.views.generic import TemplateView, DetailView, FormView, View
from haystack.inputs import Raw
from haystack.query import SearchQuerySet
from haystack.utils.geo import Point, D
from luxdjango.users.form import UserForm
from luxdjango.users.models import User, Token
from luxdjango.users.utils2 import send_notification_after_adding_post
from luxdjango.utils import handnle_form_images, get_tmp_image_urls, get_user_data, post_to_facebook
from realty.form import max_images
from geoip.utils import get_location_by_ip
from realty.models import Realty, City
from vehicles.form import CarForm, MotoForm
from vehicles.models import Country, Car, CarImage, Brand, Model, Color, SELLER_CHOICES, Moto, MotoImage
from django.core.urlresolvers import resolve
from django.conf import settings
from classifieds.models import SubCategory, SubSubCategory

CALLING_CODE = OrderedDict((name, calling_code) for name, calling_code in
                           Country.objects.all().values_list('name', 'calling_code').extra(order_by=['name']))


class VehicleView(TemplateView):
    template_name = 'vehicles/all_vehicles.html'

    def get_context_data(self, **kwargs):
        context = super(VehicleView, self).get_context_data(**kwargs)
        context['post_q'] = self.request.POST.get('q')
        context['location'] = get_location_by_ip(self.request)
        context['list_of_country'] = Country.objects.all()
        return context


class AllVehiclesElasticSearchAjax(AJAXMixin, TemplateView):
    FACET = OrderedDict([
        ('sub_category', ((1, u'Cars'), (2, u'Motorbikes'))),
        ('sub_category_index', (SubCategory, 'name')),
    ])

    CLASSIFIED_FACET = OrderedDict([
        ('sub_category_index', (SubCategory, 'name')),
        ('sub_sub_category_index', (SubSubCategory, 'name')),
    ])

    CARMOTO_FACET = OrderedDict([
        ('sub_category', ((1, u'Cars'), (2, u'Motorbikes'))),
        ('sub_category_index', (SubCategory, 'name')),
        ('sub_sub_category_index', (SubSubCategory, 'name')),
        ('brand_index', (Brand, 'brand_name')),
        ('model_index', (Model, 'model_name')),
        ('gear_box_index', None),
        ('color_index', None),
        ('energy_index', None),
        ('seller_index', SELLER_CHOICES),
        ('country_index', None),
    ])

    RANGE_FILTER = [
        'price_index',
    ]

    CARMOTO_RANGE_FILTER = [
        'year_index',
        'mileage_index',
        'price_index',
        'power_index',
    ]

    def queryset(self):
        q = SearchQuerySet().filter(big_category=u'vehicles')
        r = self.request.GET

        if r.get('q'):
            q = q.filter(text=r.get('q'))

        for p in r:
            if p in self.FACET:
                if (p == "energy_index" or p == "gear_box_index") and r.get(p) == "not_provided":
                    params = {p: Raw('*')}
                    q = q.exclude(**params)
                else:
                    params = {p: r.get(p)}
                    q = q.filter(**params)

        for p in r:
            if p in self.RANGE_FILTER:
                y = r.get(p).split(',')
                params = {p + '__range': [int(y[0].replace('.', '')), int(y[1].replace('.', ''))]}
                q = q.filter(**params)

        if r.get('city'):
            q = q.filter(city_index=r['city'])
        if r.get('countries'):
            country_ids = str(r['countries']).split(',')
            q = q.filter(country_index__in=country_ids)
            self.cities = City.objects.filter(country_id__in=country_ids)
        else:
            self.cities = City.objects.all()

        distance = r.get('distance')
        if distance and distance != '1000.00':
            distance = D(km=int(r.get('distance').replace('.00', '')))

            if 'location' in self.request.COOKIES:
                location = json.loads(urllib.unquote(self.request.COOKIES['location']))
                latitude = location['coords']['lat']
                longitude = location['coords']['lng']
            # else:
            #     location = get_location_by_ip(self.request)
            #     latitude = location.location.latitude
            #     longitude = location.location.longitude
            if location:
                user_point = Point(float(longitude), float(latitude))
                q = q.dwithin('location_index', user_point, distance)

        sort = r.get('sort')
        if sort:
            q = q.order_by(sort)
        else:
            q = q.order_by('-date_index')
        return q

    def faceted(self, search_result):
        fin_counts = OrderedDict()
        for f in self.FACET:
            search_result = search_result.facet(f)
        counts = search_result.facet_counts().get('fields', {})
        for f in self.FACET:
            if f in counts:
                if self.FACET[f]:
                    if f in ('seller_index', 'sub_category'):
                        result = [
                            {'index': index, 'count': count, 'name': self.FACET[f][int(index) - 1][1]}
                            for index, count in counts[f]
                            ]
                    else:
                        name_dict = {int(index): None for index, count in counts[f]}
                        names = self.FACET[f][0].objects.filter(
                            pk__in=name_dict.keys()
                        ).only('id', self.FACET[f][1])
                        for i in names:
                            name_dict[i.id] = getattr(i, self.FACET[f][1])
                        result = [
                            {'index': index, 'count': count, 'name': name_dict[int(index)]}
                            for index, count in counts[f]
                            ]
                    fin_counts.update({f: sorted(result, key=lambda k: k['name'])})
                else:
                    fin_counts.update(
                        {f: [{'index': index, 'count': count, 'name': index} for index, count in counts[f]]}
                    )
        return fin_counts

    def check_boxes_filters(self, facets):
        check_boxes_filters = copy.deepcopy(facets)
        if 'color_index' in check_boxes_filters:
            del check_boxes_filters["color_index"]
        if 'country_index' in check_boxes_filters:
            del check_boxes_filters["country_index"]
        return check_boxes_filters

    def get(self, request, *args, **kwargs):
        if 'sub_category' in request.GET:
            self.FACET = self.CARMOTO_FACET
            self.RANGE_FILTER = self.CARMOTO_RANGE_FILTER
        elif 'sub_category_index' in request.GET:
            self.FACET = self.CLASSIFIED_FACET

        search_result = self.queryset()
        facets = self.faceted(search_result)

        if request.GET.get('show'):
            post_count = int(request.GET['show'])
        else:
            post_count = 10
        template = ''
        if request.GET.get('style') and request.GET.get('style') == 'grid':
            template = 'search/grid_item_preview.html'
            post_count = 32
        else:
            template = 'search/car_item_preview.html'

        search_list_result = lambda: render(
            request,
            template,
            {'query': True, 'object_list': search_result, 'post_count': post_count}
        )

        sidebar_context = {
            'conditions': request.GET,
            # 'cities': self.cities,
            'selected_city': int(request.GET.get('city', 0)),
            'range_filters': self.RANGE_FILTER,
        }
        if 'color_index' in facets:
            sidebar_context.update({'colors': facets['color_index']})
        sidebar_context.update({'facets': self.check_boxes_filters(facets)})
        left_sidebar_filters = lambda: render(request, 'vehicles/left_sidebar_filters.html', sidebar_context)

        if not request.GET.get('scroll'):
            AJAX_TEMPLATE = {'inner-fragments': {
                '#search-list-result': search_list_result(),
                '#left-sidebar-filters': left_sidebar_filters(),
            }}
        else:
            AJAX_TEMPLATE = {'append-fragments': {'#search-list-result': search_list_result()}}

        return AJAX_TEMPLATE


class MainCarView(TemplateView):
    template_name = 'search/vehicle_search.html'

    def get_context_data(self, **kwargs):
        from allauth.socialaccount.models import SocialToken
        context = super(MainCarView, self).get_context_data(**kwargs)
        context['post_q'] = self.request.POST.get('q')
        context['location'] = get_location_by_ip(self.request)
        context['list_of_country'] = Country.objects.all()
        return context


class DetailCarView(DetailView):
    model = Car

    def get(self, context, pk):
        if self.request.is_ajax():
            data = get_user_data(Car, pk)
            return HttpResponse(data)
        else:
            return super(DetailCarView, self).get(context, pk)


class DetailMotoView(DetailView):
    model = Moto
    template_name = 'vehicles/car_detail.html'

    def get(self, context, pk):
        if self.request.is_ajax():
            data = get_user_data(Moto, pk)
            return HttpResponse(data)
        else:
            return super(DetailMotoView, self).get(context, pk)


AJAX_TEMPLATE = {
    'fragments': {
        '#id1': 'replace element with this content1'
    },
    'inner-fragments': {
        '#replace-inner-content': 'replace inner content'
    },
    'append-fragments': {
        '.class1': 'append this content'
    },
    'prepend-fragments': {
        '.class2': 'prepend this content'
    }
}


def autocomplete(request):
    sqs = SearchQuerySet().autocomplete(brand_auto=request.GET.get('q', ''))[:5]
    suggestions = [result.km for result in sqs]
    # Make sure you return a JSON object, not a bare list.
    # Otherwise, you could be vulnerable to an XSS attack.
    the_data = json.dumps({
        'results': suggestions
    })
    return HttpResponse(the_data, content_type='application/json')


class ElasticSearchAjax(AJAXMixin, TemplateView):
    """depricated"""

    FACET = OrderedDict([
        ('brand_index', (Brand, 'brand_name')),
        ('model_index', (Model, 'model_name')),
        ('gear_box_index', None),
        ('color_index', None),
        ('energy_index', None),
        ('seller_index', SELLER_CHOICES),
        ('country_index', None),
    ])

    RANGE_FILTER = [
        'year_index',
        'mileage_index',
        'price_index',
        'power_index',
    ]

    def queryset(self):
        if 'motorbikes' in self.request.META['HTTP_REFERER']:
            q = SearchQuerySet(using='motorbikes')
        else:
            q = SearchQuerySet(using='cars')
        r = self.request.GET

        if r.get('q'):
            q = q.filter(text=r.get('q'))

        for p in r:
            if p in self.FACET:
                if (p == "energy_index" or p == "gear_box_index") and r.get(p) == "not_provided":
                    params = {p: Raw('*')}
                    q = q.exclude(**params)
                else:
                    params = {p: r.get(p)}
                    q = q.filter(**params)

        for p in r:
            if p in self.RANGE_FILTER:
                y = r.get(p).split(',')
                params = {p + '__range': [int(y[0].replace('.', '')), int(y[1].replace('.', ''))]}
                q = q.filter(**params)

        if r.get('city'):
            q = q.filter(city_index=r['city'])
        if r.get('countries'):
            country_ids = str(r['countries']).split(',')
            q = q.filter(country_index__in=country_ids)
            self.cities = City.objects.filter(country_id__in=country_ids)
        else:
            self.cities = City.objects.all()

        distance = r.get('distance')
        if distance and distance != '1000.00':
            distance = D(km=int(r.get('distance').replace('.00', '')))

            if 'location' in self.request.COOKIES:
                location = json.loads(urllib.unquote(self.request.COOKIES['location']))
                latitude = location['coords']['lat']
                longitude = location['coords']['lng']
            else:
                location = get_location_by_ip(self.request)
                latitude = location.location.latitude
                longitude = location.location.longitude
            if location:
                user_point = Point(float(longitude), float(latitude))
                q = q.dwithin('location_index', user_point, distance)

        sort = r.get('sort')
        if sort:
            q = q.order_by(sort)
        else:
            q = q.order_by('-date_index')
        return q

    def faceted(self, search_result):
        fin_counts = OrderedDict()
        for f in self.FACET:
            search_result = search_result.facet(f)
        counts = search_result.facet_counts().get('fields', {})
        for f in self.FACET:
            if f in counts:
                if self.FACET[f]:
                    if f == 'seller_index':
                        result = [
                            {'index': index, 'count': count, 'name': SELLER_CHOICES[int(index) - 1][1]}
                            for index, count in counts[f]
                            ]
                    else:
                        name_dict = {int(index): None for index, count in counts[f]}
                        names = self.FACET[f][0].objects.filter(
                            pk__in=name_dict.keys()
                        ).only('id', self.FACET[f][1])
                        for i in names:
                            name_dict[i.id] = getattr(i, self.FACET[f][1])
                        result = [
                            {'index': index, 'count': count, 'name': name_dict[int(index)]}
                            for index, count in counts[f]
                            ]
                    fin_counts.update({f: sorted(result, key=lambda k: k['name'])})
                else:
                    fin_counts.update(
                        {f: [{'index': index, 'count': count, 'name': index} for index, count in counts[f]]}
                    )
        return fin_counts

    def check_boxes_filters(self, facets):
        check_boxes_filters = copy.deepcopy(facets)
        del check_boxes_filters["color_index"]
        del check_boxes_filters["country_index"]
        return check_boxes_filters

    def get(self, request, *args, **kwargs):
        search_result = self.queryset()
        facets = self.faceted(search_result)

        if request.GET.get('show'):
            post_count = int(request.GET['show'])
        else:
            post_count = 10
        search_list_result = lambda: render(
            request,
            'search/car_item_preview.html',
            {'query': True, 'object_list': search_result, 'post_count': post_count}
        )

        left_sidebar_filters = lambda: render(
            request,
            'search/left_sidebar_filters.html',
            {
                'facets': self.check_boxes_filters(facets),
                'conditions': request.GET,
                'cities': self.cities,
                'selected_city': int(request.GET.get('city', 0)),
            }
        )

        color_filters = lambda: render(
            request,
            'search/colors_filter.html',
            {'colors': facets['color_index']}
        )

        if not request.GET.get('scroll'):
            AJAX_TEMPLATE = {'inner-fragments': {
                '#search-list-result': search_list_result(),
                '#left-sidebar-filters': left_sidebar_filters(),
                '#colors-filters': color_filters(),
            }}
        else:
            AJAX_TEMPLATE = {'append-fragments': {'#search-list-result': search_list_result()}}

        return AJAX_TEMPLATE


class DeleteImage(View):
    def post(self, request):
        images = json.loads(request.POST['images'])
        CarImage.objects.filter(pk=images).delete()
        return HttpResponse(status=204)


class EditVehicleView(TemplateView):
    template_name = 'vehicles/car_form.html'
    home_url = '/'

    def get_form_class(self, path_info):
        if resolve(path_info).url_name != 'edit-car':
            return MotoForm, _('motorbike'), Moto, False
        else:
            return CarForm, _('car'), Car, True

    def get(self, request, pk, *args, **kwargs):
        vehicle_form, header, model, is_car = self.get_form_class(request.path_info)
        vehicle = model.objects.get(pk=pk)
        vehicle.options = ','.join((json.loads(vehicle.options)))
        if request.user != vehicle.author:
            return HttpResponseRedirect(self.home_url)
        vehicle_form = vehicle_form(instance=vehicle)
        vehicle_form.fields['brand'].initial = vehicle.brand
        vehicle_form.fields['model'].initial = vehicle.model
        vehicle_form.fields['color'].initial = vehicle.color
        vehicle_form.fields['city'].initial = vehicle.city
        images = vehicle.images.all()
        if is_car:
            vehicle_form.fields['color_inside'].initial = vehicle.color_inside

        return render(request, self.template_name, {
            "car_form": vehicle_form,
            "header": header,
            "images": images,
            'user_form': None,
            'calling_code': json.dumps(CALLING_CODE),
            "images_tmp": get_tmp_image_urls(request.META['CSRF_COOKIE'])
        })

    def post(self, request, pk):
        vehicle_form, header, model, is_car = self.get_form_class(request.path_info)
        vehicle = model.objects.get(pk=pk)
        vehicle_form = vehicle_form(data=request.POST, instance=vehicle)
        images = vehicle.images.all()
        if vehicle_form.is_valid():
            return self.form_valid(vehicle_form, is_car)
        else:
            return render(request, self.template_name, {
                "car_form": vehicle_form,
                "images": images,
                "header": header,
                "images_tmp": get_tmp_image_urls(request.META['CSRF_COOKIE']),
                'user_form': None,
                'calling_code': json.dumps(CALLING_CODE),
            })

    def form_valid(self, vehicle_form, is_car):
        brand_name = vehicle_form.cleaned_data.get('brand')
        model_name = vehicle_form.cleaned_data.get('model')
        color_name = vehicle_form.cleaned_data.get('color')
        city_name = vehicle_form.cleaned_data.get('city')
        phones = self.request.POST.getlist('phones')
        color_inside_name = vehicle_form.cleaned_data.get('color_inside')
        brand = Brand.objects.get_or_create(brand_name=brand_name)
        model = Model.objects.get_or_create(brand=brand[0], model_name=model_name)
        city = City.objects.get_or_create(name=city_name, country=vehicle_form.cleaned_data['country'])
        vehicle = vehicle_form.save(commit=False)
        if color_name:
            color = Color.objects.get_or_create(color=color_name)
            vehicle.color = color[0]
        if color_inside_name and is_car:
            color_inside = Color.objects.get_or_create(color=color_inside_name)
            vehicle.color_inside = color_inside[0]
        vehicle.brand = brand[0]
        vehicle.model = model[0]
        vehicle.city = city[0]
        vehicle.phones = phones
        vehicle.options = json.dumps([vehicle.options])
        if 'hide_phones' in self.request.POST:
            if not self.request.user.hide_phones:
                self.request.user.hide_phones = True
                self.request.user.save()
        elif self.request.user.hide_phones:
            self.request.user.hide_phones = False
            self.request.user.save()
        if not vehicle.author:
            vehicle.author = self.request.user
        vehicle.save()
        handnle_form_images(obj=vehicle,
                            img_related_key='images',
                            csrf=self.request.META['CSRF_COOKIE'],
                            images_tmp=self.request.POST.getlist('image_tmp'),
                            images_files=self.request.FILES.getlist('image'),
                            images_saved=self.request.POST.getlist('image_saved'))
        path = 'vehicles:car-details' if is_car else 'vehicles:moto-details'
        return HttpResponseRedirect(reverse(path, kwargs={'pk': vehicle.id}))


class AddCarView(FormView):
    template_name = 'vehicles/car_form.html'
    success_url = '/'

    def get_form_class(self, path_info):
        if resolve(path_info).url_name != 'add-car':
            return MotoForm, _('motorbike')
        else:
            return CarForm, _('car')

    def get(self, request):
        vehicle_form, header = self.get_form_class(request.path_info)
        user_form = UserForm()
        return render(request, self.template_name, {
            'car_form': vehicle_form,
            'user_form': user_form,
            'header': header,
            'images_tmp': get_tmp_image_urls(request.META['CSRF_COOKIE']),
            'calling_code': json.dumps(CALLING_CODE),
        })

    def post(self, request):
        vehicle_form, header = self.get_form_class(request.path_info)
        vehicle_form = vehicle_form(data=request.POST)
        user_form = UserForm(data=request.POST)
        if vehicle_form.is_valid() and (request.user.is_authenticated() or user_form.is_valid()):
            return self.form_valid(vehicle_form, user_form)
        return render(request, self.template_name, {
            'car_form': vehicle_form,
            'max_image_number': max_images,
            'header': header,
            'images_tmp': get_tmp_image_urls(request.META['CSRF_COOKIE']),
            'calling_code': json.dumps(CALLING_CODE),
        })

    def form_valid(self, vehicle_form, user_form):
        brand_name = vehicle_form.cleaned_data.get('brand')
        model_name = vehicle_form.cleaned_data.get('model')
        color_name = vehicle_form.cleaned_data.get('color')
        city_name = vehicle_form.cleaned_data.get('city')
        if self.request.user.is_anonymous():
            user = User.objects.get_or_create(email=user_form.cleaned_data.get('email'),
                                              defaults={'phones': self.request.POST.getlist('phones')})[0]
            EmailAddress.objects.get_or_create(email=user.email, defaults={'user_id': user.id})
        else:
            user = self.request.user
        if 'hide_phones' in self.request.POST:
            if not user.hide_phones:
                user.hide_phones = True
                user.save()
        else:
            if user.hide_phones:
                user.hide_phones = False
                user.save()
        user_verified = user.is_verified()
        color_inside_name = vehicle_form.cleaned_data.get('color_inside')
        brand = Brand.objects.get_or_create(brand_name=brand_name)
        city = City.objects.get_or_create(name=city_name, country=vehicle_form.cleaned_data.get('country'))
        model = Model.objects.get_or_create(brand=brand[0], model_name=model_name)
        vehicle = vehicle_form.save(commit=False)
        if color_name:
            color = Color.objects.get_or_create(color=color_name)
            vehicle.color = color[0]
        if color_inside_name:
            color_inside = Color.objects.get_or_create(color=color_inside_name)
            vehicle.color_inside = color_inside[0]
        vehicle.city = city[0]
        vehicle.phones = self.request.POST.getlist('phones')
        vehicle.brand = brand[0]
        vehicle.model = model[0]
        vehicle.options = json.dumps([vehicle.options])
        vehicle.author = user
        if not user.is_authenticated() or not user_verified:
            vehicle.is_active = False
        vehicle.save()
        handnle_form_images(obj=vehicle,
                            img_related_key='images',
                            csrf=self.request.META['CSRF_COOKIE'],
                            images_tmp=self.request.POST.getlist('image_tmp'),
                            images_files=self.request.FILES.getlist('image'))
        if vehicle.__class__.__name__ == 'Car':
            token = Token.objects.create(user=user, car=vehicle)
        else:
            token = Token.objects.create(user=user, moto=vehicle)

        if (not user.is_authenticated()) or (not user_verified):
            send_notification_after_adding_post(user, token)
            return HttpResponseRedirect(reverse('users:email_verification_sent', kwargs={'email': user.email}))
        send_notification_after_adding_post(user, token)
        if user.is_authenticated() or user_verified:
            vehicle.post_on_facebook()
        return HttpResponseRedirect(vehicle.get_absolute_url())


def get_brands(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        brands = Brand.objects.filter(brand_name__istartswith=q)[:20]
        results = []
        for brand in brands:
            brand_json = {}
            brand_json['brand_name'] = brand.brand_name
            results.append(brand_json['brand_name'])
        data = json.dumps(results)
        return HttpResponse(data)
    else:
        return HttpResponse(status=403)


def get_colors(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        colors = Color.objects.filter(color__istartswith=q)[:20]
        results = []
        for color in colors:
            color_json = {}
            color_json['color'] = color.color
            results.append(color_json['color'])
        data = json.dumps(results)
        return HttpResponse(data)
    else:
        return HttpResponse(status=403)


def get_models(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        brand = request.GET.get('id_brand', '')
        models = Model.objects.filter(model_name__istartswith=q)[:20]
        results = []
        for model in models:
            if model.brand.brand_name == brand or brand == "":
                model_json = {}
                model_json['model_name'] = model.model_name
                results.append(model_json['model_name'])
        data = json.dumps(results)
        return HttpResponse(data)
    else:
        return HttpResponse(status=403)


def get_city(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        foreign_key = request.GET.get('id_country', '')
        objects = City.objects.filter(name__istartswith=q)[:20]
        results = []
        for object in objects:
            if foreign_key == "" or object.country_id == int(foreign_key):
                object_json = {}
                object_json['city'] = object.name
                results.append(object_json['city'])
        data = json.dumps(results)
        return HttpResponse(data)
    else:
        return HttpResponse(status=403)


class HomeView(TemplateView):
    template_name = 'pages/home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        list_vehicles = Car.objects.filter(
            images__isnull=False,
            price__isnull=False,
            is_active=True
        ).order_by('-creation_date').distinct('creation_date')[:4]
        list_realty = Realty.objects.filter(
            images__isnull=False,
            is_active=True
        ).order_by('-creation_date').distinct('creation_date')[:4]
        context['list_vehicles'] = list_vehicles
        context['list_realty'] = list_realty
        context['vehicles_count'] = SearchQuerySet().filter(
            big_category='vehicles', sub_category=1
        ).count()
        context['realty_count'] = SearchQuerySet().filter(
            big_category='realty'
        ).count()
        context['jobs_count'] = SearchQuerySet().filter(
            big_category='jobs'
        ).count()
        context['classifieds_count'] = SearchQuerySet().filter(
            big_category='gooddeals'
        ).count()
        return context


class ImageAsyncUpload(View):
    def post(self, request):
        uploaded_file = request.FILES['image']
        folder = u'%s/tmp/%s/' % (settings.MEDIA_ROOT, request.META['CSRF_COOKIE'])
        if not os.path.exists(folder):
            os.makedirs(folder)
        path = os.path.join(folder, uploaded_file.name)
        f = open(path, 'wb')
        f.write(uploaded_file.read())
        f.close()
        return HttpResponse(json.dumps({
            'url': u'%stmp/%s/%s' % (settings.MEDIA_URL,
                                     request.META['CSRF_COOKIE'],
                                     uploaded_file.name),
            'name': uploaded_file.name
        }))
