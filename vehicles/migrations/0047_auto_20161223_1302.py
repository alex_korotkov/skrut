# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0046_auto_20161202_1353'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='seller_type',
            field=models.PositiveIntegerField(default=1, verbose_name='Seller type', choices=[(1, 'Private seller'), (2, 'Professional  seller')]),
        ),
        migrations.AddField(
            model_name='moto',
            name='seller_type',
            field=models.PositiveIntegerField(default=1, verbose_name='Seller type', choices=[(1, 'Private seller'), (2, 'Professional  seller')]),
        ),
    ]
