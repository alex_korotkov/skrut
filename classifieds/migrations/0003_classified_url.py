# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('classifieds', '0002_auto_20161005_1446'),
    ]

    operations = [
        migrations.AddField(
            model_name='classified',
            name='url',
            field=models.CharField(max_length=300, null=True, blank=True),
        ),
    ]
