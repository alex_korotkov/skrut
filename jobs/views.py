# -*- coding: utf-8 -*-
from collections import OrderedDict
import copy, urllib
import simplejson as json
from allauth.account.models import EmailAddress
from django.views.generic import ListView, TemplateView, DetailView, View
from django.shortcuts import render
from django_ajax.mixin import AJAXMixin
from haystack.query import SearchQuerySet

from django.http.response import HttpResponse
from django.http import HttpResponseRedirect
from jobs.form import JobForm, EmployerForm, EmployerSelectForm
from luxdjango.users.form import UserForm
from luxdjango.users.models import User, Token
from luxdjango.users.utils2 import send_notification_after_adding_post
from luxdjango.utils import get_user_data, post_to_facebook
from realty.models import City
from vehicles.models import Country
from geoip.utils import get_location_by_ip
from vehicles.views import CALLING_CODE
from .models import Job, Employer, WorkHours, Category, Contract
from haystack.utils.geo import Point, D
from django.core.urlresolvers import reverse


class AddJobView(TemplateView):
    template_name = 'jobs/add_job.html'
    job_form = JobForm()

    def get(self, request, *args, **kwargs):
        job_form = JobForm()
        employer_form = EmployerForm(prefix="employer")
        user_form = None
        employer_select_form = None
        if request.user.is_authenticated():
            employer_select_form = EmployerSelectForm()
            employer_select_form.fields['employer'].queryset = Employer.objects.filter(user=request.user)
        else:
            user_form = UserForm()
        return render(request, self.template_name,
                      {"user_form": user_form,
                       "job_form": job_form,
                       "employer_form": employer_form,
                       "employer_select_form": employer_select_form,
                       'calling_code': json.dumps(CALLING_CODE),
                       })

    def post(self, request):
        job_form = JobForm(data=request.POST)
        employer_form = EmployerForm(data=request.POST, prefix="employer")
        user_form = None
        employer_select_form = None
        if request.user.is_authenticated():
            employer_select_form = EmployerSelectForm()
            employer_select_form.fields['employer'].queryset = Employer.objects.filter(user=request.user)
        else:
            user_form = UserForm(data=request.POST)
        if job_form.is_valid() and employer_form.is_valid() and (
                    request.user.is_authenticated() or user_form.is_valid()):
            return self.form_valid(request, job_form, user_form, employer_form)
        return render(request, self.template_name, {
            "job_form": job_form,
            "user_form": user_form,
            "employer_form": employer_form,
            "employer_select_form": employer_select_form,
            'calling_code': json.dumps(CALLING_CODE),
        })

    def form_valid(self, request, job_form, user_form, employer_form):
        city_name = job_form.cleaned_data.get('city')
        country = job_form.cleaned_data.get('country')
        city = City.objects.get_or_create(name=city_name, country=country)
        if request.user.is_anonymous():
            user = User.objects.get_or_create(email=user_form.cleaned_data.get('email'),
                                              defaults={'phones': request.POST.getlist('employer-phones')})[0]
            EmailAddress.objects.get_or_create(email=user.email, defaults={'user_id': user.id})
        else:
            user = request.user
        if 'hide_phones' in request.POST:
            if not user.hide_phones:
                user.hide_phones = True
                user.save()
        else:
            if user.hide_phones:
                user.hide_phones = False
                user.save()
        user_verified = user.is_verified()
        employer = employer_form.save(commit=False)
        job = job_form.save(commit=False)
        employer, flag = Employer.objects.get_or_create(
            name=employer.name,
            site=employer.site,
            email=employer.email,
            phones=request.POST.getlist('employer-phones'),
            country=country,
            address=employer.address,
            user=user,
        )
        job.city = city[0]
        job.employer = employer
        job.author = user
        if not user.is_authenticated() or not user_verified:
            job.is_active = False
        job.save()
        job_form.save_m2m()
        token = Token.objects.create(user=user, job=job)
        if (not user.is_authenticated()) or (not user_verified):
            send_notification_after_adding_post(user, token)
            return HttpResponseRedirect(reverse('users:email_verification_sent', kwargs={'email': user.email}))
        send_notification_after_adding_post(user, token)
        if user.is_authenticated() or user_verified:
            job.post_on_facebook()
        return HttpResponseRedirect(job.get_absolute_url())


class EditJobView(TemplateView):
    template_name = 'jobs/add_job.html'
    job_form = JobForm()
    home_url = '/'

    def get(self, request, pk, *args, **kwargs):
        job = Job.objects.get(pk=pk)
        if request.user != job.author:
            return HttpResponseRedirect(self.home_url)
        employer = None
        try:
            employer = Employer.objects.get(pk=job.employer.id)
        except:
            pass
        employer_form = EmployerForm(instance=employer, prefix="employer")
        job_form = JobForm(instance=job)
        job_form.fields['city'].initial = job.city
        return render(request, self.template_name,
                      {"user_form": None,
                       "job_form": job_form,
                       "employer_form": employer_form,
                       'calling_code': json.dumps(CALLING_CODE), })

    def post(self, request, pk):
        job = Job.objects.get(pk=pk)
        employer = None
        try:
            employer = Employer.objects.get(pk=job.employer.id)
        except:
            pass
        employer_form = EmployerForm(data=request.POST, instance=employer, prefix="employer")
        job_form = JobForm(data=request.POST, instance=job)
        if job_form.is_valid() and employer_form.is_valid():
            return self.form_valid(request, job_form, employer_form)
        return render(request, self.template_name,
                      {"job_form": job_form,
                       'user_form': None,
                       "employer_form": employer_form,
                       'calling_code': json.dumps(CALLING_CODE), })

    def form_valid(self, request, job_form, employer_form):
        city_name = job_form.cleaned_data.get('city')
        country = job_form.cleaned_data.get('country')
        city = City.objects.get_or_create(name=city_name, country=country)
        if 'hide_phones' in request.POST:
            if not request.user.hide_phones:
                request.user.hide_phones = True
                request.user.save()
        else:
            if request.user.hide_phones:
                request.user.hide_phones = False
                request.user.save()
        employer = employer_form.save(commit=False)
        employer.phones = request.POST.getlist('employer-phones')
        employer.country = country
        employer.user = request.user
        job = job_form.save(commit=False)
        employer.save()
        job.city = city[0]
        job.employer = employer
        job.author = request.user
        job.save()
        job_form.save_m2m()
        return HttpResponseRedirect(reverse('jobs:details', kwargs={'pk': job.id}))


def get_city(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        foreign_key = request.GET.get('id_country', '')
        objects = City.objects.filter(name__istartswith=q)[:20]
        results = []
        for object in objects:
            if foreign_key == "" or object.country_id == int(foreign_key):
                object_json = {}
                object_json['city'] = object.name
                results.append(object_json['city'])
        data = json.dumps(results)
        return HttpResponse(data)
    else:
        return HttpResponse(status=403)


def get_employer(request):
    if request.is_ajax():
        foreign_key = request.GET.get('id_employer', '')
        employer = Employer.objects.get(pk=int(foreign_key))
        result = {
            'name': employer.name,
            'site': employer.site,
            'email': employer.email,
            'address': employer.address,
            'phones': employer.phones
        }
        data = json.dumps(result)
        return HttpResponse(data)
    else:
        return HttpResponse(status=403)


class SearchView(ListView):
    template_name = 'jobs/search.html'
    queryset = Job.objects.none()

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data()
        context['location'] = get_location_by_ip(self.request)
        context['list_of_country'] = Country.objects.all()
        return context


class DetailJobView(DetailView):
    model = Job

    def get(self, context, pk):
        if self.request.is_ajax():
            data = get_user_data(Job, pk)
            return HttpResponse(data)
        else:
            return super(DetailJobView, self).get(context, pk)


class ElasticSearchAjax(AJAXMixin, TemplateView):
    FACET = OrderedDict([
        ('category_index', (Category, 'name')),
        ('contract_index', (Contract, 'contract')),
        ('work_hours_index', (WorkHours, 'work_hours')),
    ])

    def __init__(self):
        self.q = SearchQuerySet().filter(big_category='jobs')

    def queryset(self):
        q = self.q
        r = self.request.GET

        for p in self.FACET:
            if p in r and r.get(p):
                params = {p + '__in': r.get(p).split(',')}
                q = q.filter(**params)

        if r.get('q'):
            q = q.filter(text=r.get('q'))

        if r.get('city'):
            q = q.filter(city_index=r['city'])

        if r.get('countries'):
            country_ids = r['countries'].split(',')
            q = q.filter(country_index__in=country_ids)
            self.cities = City.objects.filter(country_id__in=country_ids)
        else:
            self.cities = City.objects.all()

        distance = r.get('distance')
        if distance and distance != '1000.00':
            distance = D(km=int(distance.replace('.00', '')))

            if 'location' in self.request.COOKIES:
                location = json.loads(urllib.unquote(self.request.COOKIES['location']))
                latitude = location['coords']['lat']
                longitude = location['coords']['lng']
            # else:
            #     location = get_location_by_ip(self.request)
            #     latitude = location.location.latitude
            #     longitude = location.location.longitude
            if location:
                user_point = Point(float(longitude), float(latitude))
                q = q.dwithin('location_index', user_point, distance)

        if r.get('sort'):
            q = q.order_by(r['sort'])
        else:
            q = q.order_by('-date_index')

        return q

    def faceted(self, search_result):
        fin_counts = OrderedDict()
        for f in self.FACET:
            search_result = search_result.facet(f)
        counts = search_result.facet_counts().get('fields', {})
        for f in self.FACET:
            if f in counts:
                if self.FACET[f]:
                    name_dict = {int(index): None for index, count in counts[f]}
                    names = self.FACET[f][0].objects.filter(
                        pk__in=name_dict.keys()
                    ).only('id', self.FACET[f][1])
                    for i in names:
                        name_dict[i.id] = getattr(i, self.FACET[f][1])
                    result = [
                        {'index': index, 'count': count, 'name': name_dict[int(index)]}
                        for index, count in counts[f]
                        ]
                    fin_counts.update({f: sorted(result, key=lambda k: k['name'])})
                else:
                    fin_counts.update(
                        {f: [{'index': index, 'count': count, 'name': index} for index, count in counts[f]]}
                    )
        category_index = self.request.GET.get('category_index')
        if not fin_counts['category_index'] and category_index:
            category = Category.objects.get(pk=category_index)
            fin_counts['category_index'] = [{'index': category.id, 'count': 0, 'name': category.name}]
        return fin_counts

    def check_boxes_filters(self, facets):
        check_boxes_filters = copy.deepcopy(facets)
        return check_boxes_filters

    def get(self, request, *args, **kwargs):
        search_result = self.queryset()
        facets = self.faceted(search_result)
        if request.GET.get('show'):
            post_count = int(request.GET['show'])
        else:
            post_count = 10
        template = ''
        if request.GET.get('style') and request.GET.get('style') == 'grid':
            template = 'jobs/grid_item_preview.html'
            post_count = 32
        else:
            template = 'jobs/item_preview.html'
        search_list_result = lambda: render(
            request,
            template,
            {'query': True, 'object_list': search_result, 'post_count': post_count}
        )

        left_sidebar_filters = lambda: render(
            request,
            'jobs/left_sidebar_filters.html',
            {
                'facets': self.check_boxes_filters(facets),
                'conditions': request.GET,
                'cities': self.cities,
                'selected_city': int(request.GET.get('city', 0)),
            }
        )
        if not request.GET.get('scroll'):
            AJAX_TEMPLATE = {
                'inner-fragments': {'#search-list-result': search_list_result(),
                                    '#left-sidebar-filters': left_sidebar_filters(), },
            }
        else:
            AJAX_TEMPLATE = {
                'append-fragments': {'#search-list-result': search_list_result(), }
            }

        return AJAX_TEMPLATE
