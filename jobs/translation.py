# -*- coding: utf-8 -*-
from modeltranslation.translator import register, TranslationOptions

from .models import Category, Contract, WorkHours


@register(Category)
class CountryTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Contract)
class ContractTranslationOptions(TranslationOptions):
    fields = ('contract',)


@register(WorkHours)
class WorkHoursTranslationOptions(TranslationOptions):
    fields = ('work_hours',)
