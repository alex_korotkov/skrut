from django import template
from django.utils.html import strip_tags
from vehicles.models import Car

register = template.Library()


@register.filter()
def key(d, key_name):
    return d.get(key_name)

@register.filter()
def intkey(d, key):
    v = d.get(key)
    return int(v) if v else None

@register.assignment_tag()
def getbykey(d, key):
    return d.get(key)

@register.filter()
def striptags(value):
    return strip_tags(value)
