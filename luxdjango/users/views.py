# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
import json, re
import datetime
from django.utils import timezone
from django.contrib.auth import login
from django.core.urlresolvers import resolve
from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.utils.translation import ugettext as _
from django.shortcuts import redirect
from allauth.account.models import EmailAddress
from django.views.generic import ListView, RedirectView, UpdateView, View

from braces.views import LoginRequiredMixin
from django.views.generic import TemplateView

from classifieds.models import Classified, SubCategory, SubSubCategory
from jobs.models import Job, WorkHours, Contract
from jobs.models import Category as JobCategory
from classifieds.models import Category as ClassifiedsCategory
from .models import User, Subscription, Token
from vehicles.models import Country, Car, Brand, Model, Moto
from realty.models import PropertyType, PropertyTypeType, City, Realty

MODELS = {'cars': Car, 'realties': Realty, 'moto': Moto, 'classifieds': Classified, 'jobs': Job}
MODELS_FOR_EMAIL = {
    'car': 'vehicles:edit-car',
    'moto': 'vehicles:edit-moto',
    'job': 'jobs:edit',
    'classified': 'classifieds:edit',
    'realty': 'realty:edit'
}


class UserDetailView(LoginRequiredMixin, TemplateView):
    template_name = 'users/user_detail.html'
    model = User
    slug_field = "email"
    slug_url_kwarg = "email"


class UserPostsView(LoginRequiredMixin, TemplateView):
    template_name = 'users/user_posts.html'
    model = User
    # These next two lines tell the view to index lookups by email
    slug_field = "email"
    slug_url_kwarg = "email"

    def get_context_data(self, **kwargs):
        context = super(UserPostsView, self).get_context_data(**kwargs)
        delta = timezone.now() - datetime.timedelta(days=30)
        context['validate'] = {}
        for name, model in MODELS.iteritems():
            context[name] = model.objects.filter(author=self.request.user.id, update_date__gt=delta)
        for name, model in MODELS.iteritems():
            objects = model.objects.filter(author=self.request.user.id, update_date__lt=delta)
            if objects:
                context['validate'][name] = objects
        return context


class UserSubscriptionsView(LoginRequiredMixin, TemplateView):
    template_name = 'users/user_subscriptions.html'
    model = User
    # These next two lines tell the view to index lookups by email
    slug_field = "email"
    slug_url_kwarg = "email"

    def get_context_data(self, **kwargs):
        context = super(UserSubscriptionsView, self).get_context_data(**kwargs)
        subscriptions = self.request.user.subscription_set.all()
        jobs = {}
        realties = {}
        cars = {}
        moto = {}
        classifieds = {}

        for s in subscriptions:
            raw_filters = json.loads(s.filters)
            filters = {}
            if 'brand_index' in raw_filters:
                raw_filters['brand_index'] = Brand.objects.get(pk=raw_filters['brand_index']).brand_name
            if 'model_index' in raw_filters:
                raw_filters['model_index'] = Model.objects.get(pk=raw_filters['model_index']).model_name
            if 'work_hours_index' in raw_filters:
                raw_filters['work_hours_index'] = WorkHours.objects.get(pk=raw_filters['work_hours_index']).work_hours
            if 'category_index' in raw_filters:
                if s.category == 3:
                    raw_filters['category_index'] = JobCategory.objects.get(pk=raw_filters['category_index']).name
                else:
                    raw_filters['category_index'] = ClassifiedsCategory.objects.get(
                        pk=raw_filters['category_index']).name
            if 'contract_index' in raw_filters:
                raw_filters['contract_index'] = Contract.objects.get(pk=raw_filters['contract_index']).contract
            if 'sub_category_index' in raw_filters:
                raw_filters['sub_category_index'] = SubCategory.objects.get(pk=raw_filters['sub_category_index']).name
            if 'sub_sub_category_index' in raw_filters:
                raw_filters['sub_sub_category_index'] = SubSubCategory.objects.get(
                    pk=raw_filters['sub_sub_category_index']).name
            if 'property_type_type' in raw_filters:
                filters['Property type'] = PropertyTypeType.objects.filter(
                    id=raw_filters['property_type_type']
                ).first().name
            elif 'property_type' in raw_filters:
                filters['Property type'] = PropertyType.objects.filter(
                    id=raw_filters['property_type']
                ).first().name
            if 'city' in raw_filters:
                filters['City'] = City.objects.get(id=raw_filters['city'])
            if 'deal' in raw_filters:
                filters[_('Deal')] = _('Sale') if raw_filters['deal'] == 1 else _('Rent')

            for key in raw_filters:
                key2 = None
                if key.endswith('_index'):
                    key2 = key.replace('_index', '')
                elif key == 'q' and raw_filters[key]:
                    key2 = 'text'
                elif key == 'countries' and raw_filters[key]:
                    key2 = key
                    country_ids = [int(x) for x in str(raw_filters[key]).split(',')]
                    country_names = Country.objects.filter(
                        id__in=country_ids
                    ).values_list('name', flat=True)
                    raw_filters[key] = ', '.join(country_names)
                elif key == 'distance':
                    # user location should be in user model to filter search by distance
                    continue
                elif key in ['price_index', 'year_index', 'mileage_index', 'year_index', 'category', 'title']:
                    key2 = key
                    raw_filters[key] = raw_filters[key].replace(',', ' - ')
                if key2:
                    key2 = key2.replace('_', ' ').capitalize()
                    filters[key2] = raw_filters[key]
                    if key2 not in ['Brand', 'Countries']:
                        filters[key2] = filters[key2].lower()
            if not filters:
                filters['Without filters'] = 'any updates'
            if s.category == 3:
                jobs[s.id] = filters
            elif s.category == 2:
                realties[s.id] = filters
            elif s.category == 4:
                classifieds[s.id] = filters
            elif s.category == 5:
                moto[s.id] = filters
            else:
                cars[s.id] = filters

        context['subscriptions'] = []
        if cars:
            context['subscriptions'].append((cars, _('cars')))
        if moto:
            context['subscriptions'].append((moto, _('motorbikes')))
        if realties:
            context['subscriptions'].append((realties, _('realties')))
        if jobs:
            context['subscriptions'].append((jobs, _('jobs')))
        if classifieds:
            context['subscriptions'].append((classifieds, _('classifieds')))
        return context


class UserRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse("users:detail")


class TokenAuth(TemplateView):
    template_name = 'users/user_detail.html'
    msg = ''

    def get(self, request, token, action):
        token = Token.objects.filter(token=token).first()
        if not token:
            self.msg = _(u"Error, token is invalid or expired")
            self.template_name = 'message.html'
        else:
            if action == 'unsubscribe':
                subs = Subscription.objects.get(token=token)
                subs.delete()
                self.msg = _(u"You subscription was deleted")
                self.template_name = 'message.html'
                return super(TokenAuth, self).get(request)
            post = ''
            path = ''
            for i in MODELS_FOR_EMAIL:
                post = getattr(token, i)
                path = MODELS_FOR_EMAIL[i]
                if post:
                    break
            user = token.user
            if user != request.user or not user.is_authenticated:
                user.is_authenticated = True
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                login(self.request, user)
            if not user.is_verified():
                self.msg = _(u'Your email have been validated')
                user.emailaddress_set.filter(email=user.email).update(verified=True)
            if action == 'validate' or action == 'edit' or 'activate':
                post.is_active = True
                post.post_on_facebook()
                post.save()
                if action == 'edit':
                    return redirect(path, pk=post.id)
            elif action == 'delete':
                post.delete()
                self.msg = _(u"You post was deleted")
                self.template_name = 'message.html'
        return super(TokenAuth, self).get(request)

    def get_context_data(self, **kwargs):
        context = super(TokenAuth, self).get_context_data(**kwargs)
        if self.msg:
            context.update({'message': self.msg})
        return context


class UserUpdateView(LoginRequiredMixin, UpdateView):
    fields = ['email', ]
    model = User

    # send the user back to their own page after a successful update
    def get_success_url(self):
        return reverse("users:detail")

    def get_object(self):
        # Only get the User record for the user making the request
        return User.objects.get(email=self.request.user.email)


class UserListView(LoginRequiredMixin, ListView):
    model = User
    # These next two lines tell the view to index lookups by email
    slug_field = "email"
    slug_url_kwarg = "email"


class Subscribe(View):
    def post(self, request):
        category = re.match(r'^[^\/]+\/\/[^\/]+\/(\w+)', request.META['HTTP_REFERER']).groups()[0]
        if category == 'vehicles':
            sub_category = re.match(r'^[^\/]+\/\/[^\/]+\/+[^\/](\w+)', request.META['HTTP_REFERER']).groups()[0]
            if 'motorbike' in sub_category:
                category = 5
            else:
                category = 1
        elif category == 'realty':
            category = 2
        elif category == 'jobs':
            category = 3
        elif category == 'classifieds':
            category = 4
        user = None
        if request.user.is_authenticated():
            user = request.user
        elif request.POST.get('email'):
            user, flag = User.objects.get_or_create(email=request.POST.get('email'))
        if user:
            data = json.loads(request.POST['filters'])
            if 'page' in data:
                del data['page']
            if 'scroll' in data:
                del data['scroll']
            for key in ['year_index', 'mileage_index']:
                if key in data:
                    data[key] = data[key].replace('.00', '')
            Subscription.objects.get_or_create(filters=json.dumps(data),
                                               user=user,
                                               category=category,
                                               token=Token.objects.create(user=user)
                                               )
            return HttpResponse(status=204)
        else:
            return HttpResponse('Not authenticated!', status=400)


class DeleteSubscriptions(LoginRequiredMixin, View):
    def post(self, request):
        ids = json.loads(request.POST['subscriptions'])
        request.user.subscription_set.filter(id__in=ids).delete()
        return HttpResponse(status=204)


class HidePhones(LoginRequiredMixin, View):
    def post(self, request):
        active = request.POST.get('hide_phones') == 'true'
        request.user.hide_phones = active
        request.user.save()
        return HttpResponse(status=204)


class DeletePosts(LoginRequiredMixin, View):
    def post(self, request):
        ids = json.loads(request.POST['posts'])
        cars = []
        realties = []
        jobs = []
        moto = []
        classifieds = []
        for i in ids:
            if 'V' in i:
                cars.append(i.replace('V', ''))
            elif 'M' in i:
                moto.append(i.replace('M', ''))
            elif 'R' in i:
                realties.append(i.replace('R', ''))
            elif 'C' in i:
                classifieds.append(i.replace('C', ''))
            else:
                jobs.append(i.replace('J', ''))
        if classifieds:
            for i in Classified.objects.filter(pk__in=classifieds):
                i.delete()
        if cars:
            for i in Car.objects.filter(pk__in=cars):
                i.delete()
        if moto:
            for i in Moto.objects.filter(pk__in=moto):
                i.delete()
        if realties:
            for i in Realty.objects.filter(pk__in=realties):
                i.delete()
        if jobs:
            for i in Job.objects.filter(pk__in=jobs):
                i.delete()
        return HttpResponse(status=204)


class UpdatePost(LoginRequiredMixin, View):
    def post(self, request):
        post = json.loads(request.POST['post'])
        if 'V' in post:
            car = post.replace('V', '')
            item = Car.objects.get(pk=car)
        elif 'M' in post:
            moto = post.replace('M', '')
            item = Moto.objects.get(pk=moto)
        elif 'R' in post:
            realty = post.replace('R', '')
            item = Realty.objects.get(pk=realty)
        elif 'C' in post:
            classified = post.replace('C', '')
            item = Classified.objects.get(pk=classified)
        else:
            job = post.replace('J', '')
            item = Job.objects.filter(pk=job)
        item.update_date = timezone.now()
        item.save()
        return HttpResponse(status=204)


def social_redirect(request):
    next_url = request.GET.get('url', ).replace('|', '?').replace('^', '&')
    if next_url:
        return redirect(to=next_url)
    return redirect(to=settings.LOGIN_REDIRECT_URL)


class EmailVerificationSent(TemplateView):
    template_name = 'account/verification_sent.html'

    def get_context_data(self, email, **kwargs):
        context = super(EmailVerificationSent, self).get_context_data(**kwargs)
        context['email'] = email
        return context



class CheckView(View):

    def get(self, request):
        for key in request.META.keys():
            print(key, request.META[key])
        return HttpResponse(request.META['REMOTE_ADDR'])