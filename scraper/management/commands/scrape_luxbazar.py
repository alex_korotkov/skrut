import os
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from django.core.management.base import BaseCommand, CommandError

from scraper.luxbazar_scraper import spiders


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        os.environ['SCRAPY_PROJECT'] = 'luxbazar'
        process = CrawlerProcess(get_project_settings())
        process.crawl(spiders.LuxbazarJobsSpider)
        process.start()
