import re
from django.contrib.postgres.fields import ArrayField
from django.core.validators import RegexValidator
from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _

from AdminConfig.models import FacebookPageAccessToken
from luxdjango.utils import post_to_facebook
from vehicles.models import SELLER_CHOICES


class Category(models.Model):
    name = models.CharField(max_length=100)
    name_variants = ArrayField(models.CharField(max_length=60), null=True)

    def __unicode__(self):
        return self.name


class CandidateLevel(models.Model):
    level = models.CharField(max_length=100)

    def __unicode__(self):
        return self.level


class Contract(models.Model):
    contract = models.CharField(max_length=100)

    def __unicode__(self):
        return self.contract


class Employer(models.Model):
    name = models.CharField(max_length=100)
    site = models.CharField(max_length=100, blank=True)
    email = models.EmailField(blank=True)
    address = models.CharField(max_length=100, blank=True)
    phones2 = models.CharField(max_length=100, blank=True)
    phones = ArrayField(
        models.CharField(max_length=24, validators=[
            RegexValidator(r'^[\s\+0-9]{6,17}$', 'Enter a valid phone number.')
        ]),
        null=True,
    )
    country = models.ForeignKey('vehicles.Country', null=True)
    user = models.ForeignKey('users.User', null=True, blank=True)

    def __unicode__(self):
        return self.name


class WorkHours(models.Model):
    work_hours = models.CharField(max_length=50)

    def __unicode__(self):
        return self.work_hours


class Job(models.Model):
    url = models.URLField(null=True, blank=True)
    title = models.CharField(max_length=300)
    address = models.CharField(max_length=100, blank=True)
    description = models.TextField()
    creation_date = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey('users.User', null=True, blank=True)
    city = models.ForeignKey('realty.City', blank=True, null=True)
    country = models.ForeignKey('vehicles.Country')
    employer = models.ForeignKey(Employer, null=True, blank=True)
    contract = models.ManyToManyField(Contract, blank=True)
    category = models.ManyToManyField(Category)
    work_hours = models.ManyToManyField(WorkHours, blank=True)  # full time, part time
    candidate_level = models.ManyToManyField(CandidateLevel, blank=True)
    creation_date = models.DateTimeField(default=timezone.now)
    update_date = models.DateTimeField(default=timezone.now)
    is_active = models.BooleanField(default=True)
    seller_type = models.PositiveIntegerField(_('Seller type'), choices=SELLER_CHOICES, default=1)

    @property
    def domain(self):
        if self.url:
            domain = re.search(r'\/\/([^\/]+)', self.url)
            if domain:
                return domain.groups()[0]
        return ''

    def get_absolute_url(self):
        return reverse('jobs:details', kwargs={'pk': self.id})

    def post_on_facebook(self):
        post_to_facebook(
            self.title,
            self.get_absolute_url(),
            self.description,
            self,
        )

    def update_update_date(self):
        self.update_date = timezone.now()
        self.save()

    @property
    def big_category(self):
        return u'jobs'


class JobFacebookPost(models.Model):
    job = models.ForeignKey(Job, on_delete=models.CASCADE, related_name='facebookpost_set')
    post_id = models.CharField(_('post id'), max_length=50)
    access_token = models.ForeignKey(FacebookPageAccessToken, on_delete=models.CASCADE)
