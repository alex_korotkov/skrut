# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0015_remove_car_price_history'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='price_history',
            field=django.contrib.postgres.fields.ArrayField(size=None, null=True, base_field=models.IntegerField(), blank=True),
        ),
    ]
