# -*- coding: utf-8 -*-
import logging
from scrapy.exceptions import DropItem
from scrapy.http import Request
from scrapy.pipelines.images import ImagesPipeline

from luxdjango.settings.common import MEDIA_ROOT
from classifieds.models import Classified, Category, SubCategory, SubSubCategory, ClassifiedImage
from realty.models import City, Realty
from vehicles.models import Car, Moto
from jobs.models import Job
from scraper.jobs_scraper.pipelines import ObjModelPipeline as JobModelPipeline
from scraper.vehicles_scraper.pipelines import ObjModelPipeline as CarModelPipeline, \
                                               StoreImgPipeline as VehiclesStoreImgPipeline,\
                                               SaveImgModelPipeline as VehiclesSaveImgModelPipeline,\
                                               COUNTRIES
from scraper.realty_scraper.pipelines import ObjModelPipeline as RealtyModelPipeline, \
                                             SaveImgModelPipeline as RealtySaveImgModelPipeline,\
                                             StoreImgPipeline as RealtyStoreImgPipeline
from scraper.utils import img_path
from .settings import IMAGES_STORE

CLASSIFIED_FIELDS = [x.attname for x in Classified._meta.fields if x.attname != 'id']
SUBCATEGORIES = [(x.id, x.name_en.lower(), x.name_fr.lower()) for x in SubCategory.objects.all()]
SUBSUBCATEGORIES = [(x.id, x.name_en.lower(), x.name_fr.lower()) for x in SubSubCategory.objects.all()]

logger = logging.getLogger('luxbazar_pipline')

class LuxbazarScraperPipeline(object):
    def process_item(self, item, spider):
        if item.__class__.__name__ == 'JobItem':
            return JobModelPipeline().process_item(item, spider)
        elif item.__class__.__name__ == 'CarItem':
            return CarModelPipeline().process_item(item, spider)
        elif item.__class__.__name__ == 'RealtyItem':
            return RealtyModelPipeline().process_item(item, spider)
        elif item.__class__.__name__ == 'ClassifiedItem':
            logger.info('///// Classified item process /////')
            if 'new_price' in item or 'update' in item:
                return self.process_item_update(item, spider)
            else:
                return self.process_new_item(item, spider)

    def process_new_item(self, item, spider):
        logger.info('/// process new item ///', item['url'])
        values_dict = {}
        if item.get('category'):
            category = Category.objects.filter(name__iexact=item['category']).first()
            if not category:
                category = Category.objects.create(name=item['category'])
            values_dict['category_id'] = category.id
            if item.get('sub_category'):
                for i in SUBCATEGORIES:
                    if item['sub_category'].lower() in i:
                        values_dict['sub_category_id'] = i[0]
                if not values_dict.get('sub_category_id'):
                    if item['sub_category'] == u'Gesundheitszubehör':
                        sub_category = SubCategory.objects.get(pk=47)
                    else:
                        sub_category = SubCategory.objects.filter(
                            category_id=category.id, name_fr__iexact=item['sub_category']
                        ).first()
                    if not sub_category:
                        sub_category = SubCategory.objects.create(
                            category_id=category.id, name_fr=item['sub_category']
                        )
                    values_dict['sub_category_id'] = sub_category.id
                if item.get('sub_sub_category'):
                    if item['sub_sub_category'] == u'Matériels':
                        values_dict['sub_sub_category_id'] = 28
                    elif item.get('sub_sub_category'):
                        for i in SUBSUBCATEGORIES:
                            if item['sub_sub_category'].lower() in i:
                                values_dict['sub_sub_category_id'] = i[0]
                        if not values_dict.get('sub_sub_category_id'):
                            sub_sub_category = SubSubCategory.objects.filter(
                                sub_category_id=sub_category.id,
                                name_fr=item['sub_sub_category'],
                            ).first()
                            if not sub_sub_category:
                                sub_sub_category = SubSubCategory.objects.create(
                                    sub_category_id=sub_category.id,
                                    name_fr__iexact=item['sub_sub_category'],
                                )
                            values_dict['sub_sub_category_id'] = sub_sub_category.id
        if item.get('country'):
            for c in COUNTRIES:
                if item['country'] in c['names']:
                    values_dict['country_id'] = c['id']
                    break

        if item.get('city') and values_dict.get('country_id'):
            city = City.objects.filter(
                name__iexact=item['city'], country_id=values_dict['country_id']
            ).first()
            if not city:
                city = City.objects.create(
                    name=item['city'], country_id=values_dict['country_id']
                )
            values_dict['city_id'] = city.id
        for field in CLASSIFIED_FIELDS:
            value = item.get(field)
            if value:
                values_dict[field] = value

        item['model_obj'] = Classified.objects.create(**values_dict)
        return item

    def process_item_update(self, item, spider):
        if item.get('new_price'):
            logger.info('/// process price update')
            item['model_obj'].update_price(item['new_price'])
        item['model_obj'].update_update_date()
        raise DropItem('Price updated')


class StoreImgPipeline(ImagesPipeline):
    def process_item(self, item, spider):
        if isinstance(item['model_obj'], Car):
            self.sub_folder_name = 'car-sc'
        elif isinstance(item['model_obj'], Moto):
            self.sub_folder_name = 'moto-sc'
        elif isinstance(item['model_obj'], Classified):
            self.sub_folder_name = 'classified-sc'

        return super(StoreImgPipeline, self).process_item(item, spider)

    def file_path(self, request, response=None, info=None):
        logger.info('/// file path ///')
        return img_path(self.sub_folder_name, request.url)


class SaveImgModelPipeline(object):

    def process_item(self, item, spider):
        logger.info('/// process img model save ///')
        if isinstance(item['model_obj'], Car) or isinstance(item['model_obj'], Moto):
            item = VehiclesSaveImgModelPipeline().process_item(item, spider)
        if isinstance(item['model_obj'], Realty):
            item = RealtySaveImgModelPipeline().process_item(item, spider)
        elif isinstance(item['model_obj'], Classified):
            for img in item.get('images', []):
                ClassifiedImage.objects.create(
                    classified_id=item['model_obj'].id, image=img['path']
                )
        return item
