from django.core.management import BaseCommand

from luxdjango.users.utils import send_notification


class Command(BaseCommand):
    def handle(self, *args, **options):
        send_notification()
