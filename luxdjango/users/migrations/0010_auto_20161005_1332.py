# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0009_auto_20160912_1004'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscription',
            name='category',
            field=models.PositiveIntegerField(choices=[(1, b'vehicles'), (2, b'realty'), (3, b'jobs')]),
        ),
    ]
