# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0041_auto_20161027_1123'),
    ]

    operations = [
        migrations.AlterField(
            model_name='carimage',
            name='car',
            field=models.ForeignKey(related_name='images', to='vehicles.Car'),
        ),
        migrations.AlterField(
            model_name='motoimage',
            name='moto',
            field=models.ForeignKey(related_name='images', to='vehicles.Moto'),
        ),
    ]
