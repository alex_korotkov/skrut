# -*- coding: utf-8 -*-
from luxdjango.utils import img_upload_to

def img_path(folder_name, url):
    # as scrapy by default convert images to jpg add this extention to the end of the url
    filename = url + '.jpg'
    # as scrapy calls "file_path" several times we have to path 'unique_name=True'
    # to gets same path
    return img_upload_to(None, filename, folder_name, True)