# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('AdminConfig', '0004_facebookpageaccesstoken'),
    ]

    operations = [
        migrations.AlterField(
            model_name='facebookpageaccesstoken',
            name='name',
            field=models.CharField(max_length=200),
        ),
    ]
