# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.postgres.fields
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0019_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realty',
            name='phones',
            field=django.contrib.postgres.fields.ArrayField(null=True, base_field=models.CharField(max_length=24, validators=[django.core.validators.RegexValidator(b'^[\\s \\+ 0-9]{6,17}$', b'Enter a valid phone number.')]), size=None),
        ),
    ]
