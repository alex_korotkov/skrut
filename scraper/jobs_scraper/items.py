# -*- coding: utf-8 -*-
import scrapy


class JobItem(scrapy.Item):
    title = scrapy.Field()
    category = scrapy.Field()
    country = scrapy.Field()
    city = scrapy.Field()
    address = scrapy.Field()
    candidate_level = scrapy.Field()
    work_hours = scrapy.Field()
    contract = scrapy.Field()
    employer = scrapy.Field()
    description = scrapy.Field()
    url = scrapy.Field()
    country_id = scrapy.Field()
    city_id = scrapy.Field()
    default_country_id = scrapy.Field()
    model_obj = scrapy.Field()
    update = scrapy.Field()
