# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0004_auto_20160919_1154'),
    ]

    operations = [
        migrations.AddField(
            model_name='contract',
            name='contract_en',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='contract',
            name='contract_fr',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='workhours',
            name='work_hours_en',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='workhours',
            name='work_hours_fr',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
