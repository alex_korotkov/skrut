# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.postgres.fields
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0036_auto_20160830_1304'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='phones',
            field=django.contrib.postgres.fields.ArrayField(size=None, null=True, base_field=models.CharField(max_length=24, validators=[django.core.validators.RegexValidator('^[\\s\\+0-9]{6,17}$', 'Enter a valid phone number.')]), blank=True),
        ),
    ]
