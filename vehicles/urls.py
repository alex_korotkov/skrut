# -*- coding: utf-8 -*-
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.VehicleView.as_view(), name="all_vehicles"),
    url(r'^all-vehicles-ajax/$', views.AllVehiclesElasticSearchAjax.as_view(), name="all_vehicles_ajax"),
    url(r'^add_car', views.AddCarView.as_view(), name="add-car"),
    url(r'^add_motorbike', views.AddCarView.as_view(), name="add-moto"),
    # url(r'^cars/search/', views.MainCarView.as_view(), name="car_search"),
    # url(r'^motorbikes/search/', views.MainCarView.as_view(), name="moto_search"),
    url(r'^car/details/(?P<pk>[0-9]+)/$', views.DetailCarView.as_view(), name='car-details'),
    url(r'^motorbike/details/(?P<pk>[0-9]+)/$', views.DetailMotoView.as_view(), name='moto-details'),
    url(r'^search-ajax/', views.ElasticSearchAjax.as_view(), name='search-ajax'),
    url(r'^search/autocomplete/', views.autocomplete, name='autocomplete'),
    url(r'^postadd/$', views.AddCarView.as_view(), name="AddCar"),
    url(r'^get_models/', views.get_models, name='get_models'),
    url(r'^get_colors/', views.get_colors, name='get_colors'),
    url(r'^get_brands/', views.get_brands, name='get_brands'),
    url(r'^get_city/', views.get_city, name='get_city'),
    url(r'^car/edit/(?P<pk>\d+)/$', views.EditVehicleView.as_view(), name='edit-car'),
    url(r'^motorbike/edit/(?P<pk>\d+)/$', views.EditVehicleView.as_view(), name='edit-moto'),
    url(r'^delete-image/$', view=views.DeleteImage.as_view()),
]
