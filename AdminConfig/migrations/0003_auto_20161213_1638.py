# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('AdminConfig', '0002_auto_20161213_1633'),
    ]

    operations = [
        migrations.RenameField(
            model_name='hidecontacts',
            old_name='domen',
            new_name='domain',
        ),
    ]
