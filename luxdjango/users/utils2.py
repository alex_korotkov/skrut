# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.template import Template as DjangoTemplate
from AdminConfig.models import Template
from django.template import Context


def send_notification_after_adding_post(user, token):
    site = Site.objects.get_current()
    title = u"%s notification" % site.name
    email_content = DjangoTemplate(get_template_text('user_notification_after_adding_post')).render(
        Context({'site': site, 'token': token}))
    email = EmailMessage(title, email_content, settings.EMAIL_HOST_USER, to=[user.email])
    email.content_subtype = 'html'
    email.send()


def get_template_text(template_name):
    templates_text = ''
    for i in ['header', template_name, 'footer']:
        template = Template.objects.filter(name=i)
        if template:
            templates_text += template[0].template
    return templates_text
