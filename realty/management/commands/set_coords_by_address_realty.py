# -*- coding: utf-8 -*-

# Django imports
import lxml
import json
import requests
import time
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from realty.models import Realty, City
from optparse import make_option

import urllib2


class Command(BaseCommand):
    help = 'Parse gmaps'
    _url = "https://maps.googleapis.com/maps/api/geocode/json?&key=AIzaSyAWDqHgVtEEGJpOtzDSrbAxHbuL9PkCQlo&language=ru&address=%s"

    option_list = BaseCommand.option_list + (
        make_option('--city',
                    action='store_true',
                    help='Delete poll'),
        make_option('--put_city_coords_to_ads',
                    action='store_true',
                    help='Close poll'),
        make_option('--realty',
                    action='store_true',
                    help='Close poll'),
    )


    def get_coords(self, address):
        # type: (address) -> str
        try:
            data = requests.get(self._url % address)
            print self._url % address
            if data:
                result = json.loads(data.content)
                return result['results'][0]['geometry']['location']
        except Exception as ex:
            self.stdout.write("Error: %s" % ex.message)
        return None

    def handle(self, *args, **options):

        def processing(obj):
            for o in obj.objects.all():
                if options['realty']:
                    if not o.address:
                        continue
                    coords = self.get_coords("{0}, {1}".format(o.city, o.country))
                    o.order_by('-creation_date')
                    if not o.address:
                        continue
                else:
                    coords = self.get_coords("{0}, {1}".format(o.name.encode('utf-8'), o.country.name.encode('utf-8')))
                    print 'trying ', o.name, o.country
                if coords:
                    o.latitude = coords['lat']
                    o.longitude = coords['lng']
                    o.save()
                    print "Success with: ", coords

        if options['city']:
            processing(City)
            self.stdout.write('Fix successfully.\n')
        if options['put_city_coords_to_ads']:
            for ad in Realty.objects.all():
                if ad.city:
                    ad.longitude = ad.city.longitude
                    ad.latitude = ad.city.latitude
                    ad.save()
            self.stdout.write('Fix successfully.\n')
        elif options['realty']:
            processing(Realty)
            self.stdout.write('Fix successfully.\n')
        else:
            self.stdout.write('Please specify option.\n')


