# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0010_job_is_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='job',
            name='update_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
