# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0002_auto_20160912_1553'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='name_variants',
            field=django.contrib.postgres.fields.ArrayField(null=True, base_field=models.CharField(max_length=60), size=None),
        ),
    ]
