# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0022_seller_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='realty',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
