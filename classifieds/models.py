# -*- coding: utf-8 -*-
from django.contrib.postgres.fields import ArrayField
from django.core.urlresolvers import reverse
from django.core.validators import RegexValidator
from django.db import models
import datetime, hashlib, re
from django.utils import timezone
from django.utils.text import force_unicode
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse

from AdminConfig.models import FacebookPageAccessToken
from realty.models import City
from vehicles.models import Country, SELLER_CHOICES
from luxdjango.utils import img_upload_to, post_to_facebook


class Category(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name


class SubCategory(models.Model):
    name = models.CharField(max_length=100)
    category = models.ForeignKey(Category)

    def __unicode__(self):
        return self.name


class SubSubCategory(models.Model):
    name = models.CharField(max_length=100)
    sub_category = models.ForeignKey(SubCategory)

    def __unicode__(self):
        return self.name


class Classified(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    category = models.ForeignKey(Category)
    sub_category = models.ForeignKey(SubCategory, null=True, blank=True)
    sub_sub_category = models.ForeignKey(SubSubCategory, null=True, blank=True)
    country = models.ForeignKey(Country, on_delete=models.PROTECT, null=True)
    city = models.ForeignKey(City, null=True, blank=True, on_delete=models.PROTECT)
    price = models.PositiveIntegerField(_('Price'), null=True, blank=True)
    creation_date = models.DateTimeField(default=timezone.now)
    update_date = models.DateTimeField(default=timezone.now)
    url = models.CharField(max_length=300, null=True, blank=True)
    email = models.EmailField(blank=True)
    author = models.ForeignKey('users.User', null=True, blank=True)
    price_history = ArrayField(models.IntegerField(), blank=True, null=True)
    phones = ArrayField(
        models.CharField(max_length=24, validators=[
            RegexValidator(r'^[\s \+ 0-9]{6,17}$', 'Enter a valid phone number.')
        ]),
        null=True, blank=True
    )
    is_active = models.BooleanField(default=True)
    seller_type = models.PositiveIntegerField(_('Seller type'), choices=SELLER_CHOICES, default=1)

    @property
    def domain(self):
        if self.url:
            domain = re.search(r'\/\/([^\/]+)', self.url)
            if domain:
                return domain.groups()[0]
        return ''

    @property
    def big_category(self):
        if self.sub_category_id in (3, 18, 26, 39, 67):
            return u'vehicles'
        elif self.category_id == 3:
            return u'pets'
        elif self.category_id == 2:
            return u'sports'
        else:
            return u'gooddeals'

    @property
    def big_sub_category(self):
        return self.category

    def get_absolute_url(self):
        return reverse('classifieds:details', kwargs={'pk': self.id})

    def update_price(self, new_price):
        if self.price != new_price:
            if self.price_history:
                index = len(self.price_history) - 1
                if self.price and self.price != self.price_history[index]:
                    self.price_history.append(self.price)
                elif new_price == self.price_history[index]:
                    del self.price_history[index]
            elif self.price:
                self.price_history = [self.price]
            self.price = new_price
            self.save()

    def update_update_date(self):
        self.update_date = timezone.now
        self.save()

    def post_on_facebook(self):
        post_to_facebook(
            self.title,
            self.get_absolute_url(),
            self.description,
            self,
            self.images.all()[0].image.url if self.images.all() else None
        )


class ClassifiedImage(models.Model):
    classified = models.ForeignKey(Classified, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(upload_to=img_upload_to)


class FacebookPost(models.Model):
    classified = models.ForeignKey(Classified, on_delete=models.CASCADE, related_name='facebookpost_set')
    post_id = models.CharField(_('post id'), max_length=50)
    access_token = models.ForeignKey(FacebookPageAccessToken, on_delete=models.CASCADE)
