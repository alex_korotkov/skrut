from haystack import indexes
from .models import Job


class JobIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title_index = indexes.CharField(model_attr='title', faceted=True, default=None)
    country_index = indexes.IntegerField(model_attr='country_id', faceted=True, default=None)
    city_index = indexes.IntegerField(model_attr='city_id', default=None)
    employer_index = indexes.CharField(model_attr='employer__name', default=None)
    date_index = indexes.DateTimeField(model_attr='creation_date')
    contract_index = indexes.MultiValueField()
    candidate_level_index = indexes.MultiValueField()
    work_hours_index = indexes.MultiValueField()
    category_index = indexes.MultiValueField()
    big_category = indexes.CharField(model_attr='big_category', faceted=True)

    def prepare_contract_index(self, obj):
        return [contract.pk for contract in obj.contract.all()] or None

    def prepare_candidate_level_index(self, obj):
        return [candidate_level.pk for candidate_level in obj.candidate_level.all()] or None

    def prepare_work_hours_index(self, obj):
        return [work_hours.pk for work_hours in obj.work_hours.all()] or None

    def prepare_category_index(self, obj):
        return [category.pk for category in obj.category.all()] or None

    def get_model(self):
        return Job

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(is_active=True)
