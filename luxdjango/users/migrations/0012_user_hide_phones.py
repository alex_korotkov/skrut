# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0011_auto_20161201_1250'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='hide_phones',
            field=models.BooleanField(default=False),
        ),
    ]
