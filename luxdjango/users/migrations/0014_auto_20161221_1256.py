# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0013_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscription',
            name='location_json',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='location_json',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
