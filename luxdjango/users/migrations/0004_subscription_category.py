# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_subscription_last_notification'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscription',
            name='category',
            field=models.PositiveIntegerField(default=1, choices=[(1, b'vehicles'), (2, b'realty')]),
            preserve_default=False,
        ),
    ]
