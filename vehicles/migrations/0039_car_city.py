# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0021_auto_20160908_1358'),
        ('vehicles', '0038_auto_20160907_1323'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='city',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, blank=True, to='realty.City', null=True),
        ),
    ]
