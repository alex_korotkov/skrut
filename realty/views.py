# -*- coding: utf-8 -*-
import copy, os, urllib
import simplejson as json
from collections import defaultdict
from django.core.urlresolvers import reverse
from django.conf import settings
from django.forms import model_to_dict
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse
from django.shortcuts import render
from django.utils.translation import ugettext as _
from django.views.generic import ListView, TemplateView, DetailView, View
from django_ajax.mixin import AJAXMixin
from geoip.utils import get_location_by_ip
from allauth.account.models import EmailAddress
from haystack.query import SearchQuerySet
from haystack.utils.geo import Point, D
from sorl import thumbnail

from luxdjango.users.form import UserForm
from luxdjango.users.models import User, Token
from luxdjango.users.utils2 import send_notification_after_adding_post
from luxdjango.utils import handnle_form_images, get_tmp_image_urls, get_user_data, post_to_facebook
from realty.form import RealtyForm
from vehicles.views import CALLING_CODE
from .models import Realty, RealtyImage, PropertyType, PropertyTypeType, City
from vehicles.models import Country
from realty.form import max_images

energy_class = Realty.ENERGY_CHOICES
property_types = list(PropertyType.objects.all())
cities = []
property_type_types = list(PropertyTypeType.objects.all().order_by('name'))
property_tt_dict = {x.id: x for x in property_type_types}


class DeleteImage(View):
    def post(self, request):
        images = json.loads(request.POST['images'])
        RealtyImage.objects.filter(pk=images).delete()
        return HttpResponse(status=204)


class EditRealtyView(TemplateView):
    template_name = 'realty/add_realty.html'
    realty_form = RealtyForm()
    home_url = '/'

    def get(self, request, pk, *args, **kwargs):
        realty = Realty.objects.get(pk=pk)
        if request.user != realty.author:
            return HttpResponseRedirect(self.home_url)
        realty_form = RealtyForm(instance=realty)
        realty_form.fields['city'].initial = realty.city
        images = realty.images.all()
        return render(request, self.template_name, {
            "realty_form": realty_form,
            "header": 'realty',
            "images": images,
            'user_form': None,
            'calling_code': json.dumps(CALLING_CODE),
            "images_tmp": get_tmp_image_urls(request.META['CSRF_COOKIE']),
            'categories': get_options()
        })

    def post(self, request, pk):
        realty = Realty.objects.get(pk=pk)
        realty_form = RealtyForm(data=request.POST, instance=realty)
        images = realty.images.all()
        if realty_form.is_valid():
            return self.form_valid(realty_form, request)
        return render(request, self.template_name,
                      {"realty_form": realty_form,
                       "images": images,
                       "header": 'realty',
                       "images_tmp": get_tmp_image_urls(request.META['CSRF_COOKIE']),
                       'user_form': None,
                       'calling_code': json.dumps(CALLING_CODE),
                       'categories': get_options()})

    def form_valid(self, realty_form, request):
        city_name = realty_form.cleaned_data.get('city')
        phones = request.POST.getlist('phones')
        city = City.objects.get_or_create(name=city_name, country=realty_form.cleaned_data.get('country'))
        realty = realty_form.save(commit=False)
        realty.city = city[0]
        realty.phones = phones
        if 'hide_phones' in request.POST:
            if not request.user.hide_phones:
                request.user.hide_phones = True
                request.user.save()
        else:
            if request.user.hide_phones:
                request.user.hide_phones = False
                request.user.save()
        if not request.user.phones:
            request.user.phones = phones.split(',')
            request.user.save()
        if not realty.author:
            realty.author = request.user
        realty.save()
        handnle_form_images(obj=realty,
                            img_related_key='images',
                            csrf=self.request.META['CSRF_COOKIE'],
                            images_tmp=self.request.POST.getlist('image_tmp'),
                            images_files=self.request.FILES.getlist('image'),
                            images_saved=self.request.POST.getlist('image_saved'))
        return HttpResponseRedirect(realty.get_absolute_url())


class SearchView(ListView):
    template_name = 'realty/realty_search.html'
    queryset = Realty.objects.none()

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data()
        context['location'] = get_location_by_ip(self.request)
        context['list_of_country'] = Country.objects.all()
        return context


class DetailRealtyView(DetailView):
    model = Realty

    def get(self, context, pk):
        if self.request.is_ajax():
            data = get_user_data(self.model, pk)
            return HttpResponse(data)
        else:
            return super(DetailRealtyView, self).get(context, pk)


class AddRealtyView(TemplateView):
    template_name = 'realty/add_realty.html'
    realty_form = RealtyForm()
    success_url = '/'

    def get(self, request, *args, **kwargs):
        realty_form = RealtyForm()
        user_form = UserForm() if not request.user.is_authenticated() else None
        return render(request, self.template_name,
                      {"realty_form": realty_form,
                       'header': 'realty',
                       'images_tmp': get_tmp_image_urls(request.META['CSRF_COOKIE']),
                       'user_form': user_form,
                       'calling_code': json.dumps(CALLING_CODE),
                       'categories': get_options()})

    def post(self, request):
        realty_form = RealtyForm(data=request.POST)
        user_form = UserForm(data=request.POST) if not request.user.is_authenticated() else None
        if realty_form.is_valid() and (request.user.is_authenticated() or user_form.is_valid()):
            return self.form_valid(request, realty_form, user_form)
        return render(request, self.template_name, {
            "realty_form": realty_form,
            'image_counter': xrange(max_images),
            'header': 'realty',
            'images_tmp': get_tmp_image_urls(request.META['CSRF_COOKIE']),
            'user_form': user_form,
            'calling_code': json.dumps(CALLING_CODE),
            'categories': get_options()})

    def form_valid(self, request, realty_form, user_form):
        city_name = realty_form.cleaned_data.get('city')
        if request.user.is_anonymous():
            user = User.objects.get_or_create(email=user_form.cleaned_data.get('email'),
                                              defaults={'phones': request.POST.getlist('phones')})[0]
            EmailAddress.objects.get_or_create(email=user.email, defaults={'user_id': user.id})
        else:
            user = request.user
        if 'hide_phones' in request.POST:
            if not user.hide_phones:
                user.hide_phones = True
                user.save()
        else:
            if user.hide_phones:
                user.hide_phones = False
                user.save()
        user_verified = user.is_verified()
        city = City.objects.get_or_create(name=city_name, country=realty_form.cleaned_data.get('country'))
        realty = realty_form.save(commit=False)
        coords = request.POST.get('coords')
        if coords:
            coords = coords.split(',')
            realty.latitude = float(coords[0])
            realty.longitude = float(coords[1])
        realty.city = city[0]
        realty.phones = request.POST.getlist('phones')
        realty.author = user
        if not user.is_authenticated() or not user_verified:
            realty.is_active = False
        realty.save()
        token = Token.objects.create(user=user, realty=realty)
        handnle_form_images(obj=realty,
                            img_related_key='images',
                            csrf=self.request.META['CSRF_COOKIE'],
                            images_tmp=self.request.POST.getlist('image_tmp'),
                            images_files=self.request.FILES.getlist('image'))
        if (not user.is_authenticated()) or (not user_verified):
            send_notification_after_adding_post(user, token)
            return HttpResponseRedirect(reverse('users:email_verification_sent', kwargs={'email': user.email}))
        send_notification_after_adding_post(user, token)
        if user.is_authenticated() or user_verified:
            realty.post_on_facebook()
        return HttpResponseRedirect(realty.get_absolute_url())


def get_city(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        foreign_key = request.GET.get('id_country', '')
        objects = City.objects.filter(name__istartswith=q)[:20]
        results = []
        for object in objects:
            if foreign_key == "" or object.country_id == int(foreign_key):
                object_json = {}
                object_json['city'] = object.name
                results.append(object_json['city'])
        data = json.dumps(results)
        return HttpResponse(data)
    else:
        return HttpResponse(status=403)


def get_options():
    property_type_types = PropertyTypeType.objects.all().values_list('id', 'name', 'property_type_id')
    result = {}
    for property_type_type_id, name, property_id in property_type_types:
        if result.get(property_id):
            result[property_id].append((property_type_type_id, name))
        else:
            result[property_id] = [(property_type_type_id, name)]
    return json.dumps(result)


class ElasticSearchAjax(AJAXMixin, TemplateView):
    FACET = [
        # 'country',
        'rooms',
        'energy_class',
        'property_type_type',
    ]

    RANGE_FILTER = [
        'land_area',
        'area',
        'year_index',
        'price_index',
    ]

    def __init__(self):
        self.city_coordinates = {'latitude': 49.6116, 'longitude': 6.1319}
        self.q = SearchQuerySet().filter(big_category='realty')

    def queryset(self):
        q = self.q
        r = self.request.GET

        if r.get('q'):
            q = q.filter(text=r.get('q'))

        for p in r:
            if p in self.FACET:
                params = {p: r.get(p)}
                q = q.filter(**params)
            elif p in self.RANGE_FILTER:
                y = r.get(p).split(',')

                params = {p + '__range': [int(y[0].replace('.', '')), int(y[1].replace('.', ''))]}
                q = q.filter(**params)

        if r.get('property_type'):
            q = q.filter(property_type=r['property_type'])

        if r.get('city'):
            q = q.filter(city=r['city'])
            c = City.objects.get(id=r['city'])
            self.city_coordinates = {'latitude': c.latitude, 'longitude': c.longitude}

        if r.get('countries'):
            country_ids = str(r['countries']).split(',')
            q = q.filter(country__in=country_ids)
            self.cities = City.objects.filter(country_id__in=country_ids)
        else:
            self.cities = City.objects.all()

        if r.get('deal'):
            q = q.filter(deal=r['deal'])

        if r.get('energy_class'):
            q = q.filter(energy_class=r['energy_class'])

        distance = r.get('distance')
        if distance and distance != '1000.00':
            distance = D(km=int(distance.replace('.00', '')))

            if 'location' in self.request.COOKIES:
                location = json.loads(urllib.unquote(self.request.COOKIES['location']))
                latitude = location['coords']['lat']
                longitude = location['coords']['lng']
            # else:
            #     location = get_location_by_ip(self.request)
            #     latitude = location.location.latitude
            #     longitude = location.location.longitude
            if location:
                user_point = Point(float(longitude), float(latitude))
                q = q.dwithin('location_index', user_point, distance)

        if r.get('coordinates'):
            latitude = r.get('coordinates').split(',')[0]
            longitude = r.get('coordinates').split(',')[1]
            point = Point(float(longitude), float(latitude))
            q = q.dwithin('location_index', point, D(km=0.1))

        if r.get('sort'):
            q = q.order_by(r['sort'])
        else:
            q = q.order_by('-date_index')

        return q

    def faceted(self, search_result):
        for f in self.FACET:
            search_result = search_result.facet(f)
        return search_result.facet_counts()

    def check_boxes_filters(self, facets):
        check_boxes_filters = copy.deepcopy(facets)
        if 'country' in check_boxes_filters.keys():
            del check_boxes_filters["country"]
        if 'energy_class' in check_boxes_filters.keys():
            del check_boxes_filters["energy_class"]
        if 'property_type_type' in check_boxes_filters.keys():
            del check_boxes_filters["property_type_type"]
        if 'rooms' in check_boxes_filters.keys():
            check_boxes_filters["rooms"] = sorted(check_boxes_filters["rooms"])
        return check_boxes_filters

    def check_boxes_filters_property_type_type(self, facets):
        check_boxes_filters = copy.deepcopy(facets)
        if 'country' in check_boxes_filters.keys():
            del check_boxes_filters["country"]
        if 'energy_class' in check_boxes_filters.keys():
            del check_boxes_filters["energy_class"]
        return check_boxes_filters

    def check_boxes_filters_energy(self, facets):
        check_boxes_filters = copy.deepcopy(facets)
        if 'country' in check_boxes_filters.keys():
            del check_boxes_filters["country"]
        if 'property_type_type' in check_boxes_filters.keys():
            del check_boxes_filters["property_type_type"]
        return check_boxes_filters

    def get(self, request, *args, **kwargs):
        if request.GET.get('show'):
            post_count = int(request.GET['show'])
        else:
            post_count = 10
        template = ''
        if request.GET.get('style') and request.GET.get('style') == 'grid':
            template = 'realty/grid_item_preview.html'
            post_count = 32
        else:
            template = 'realty/item_preview.html'
        search_result = self.queryset()

        facets = self.faceted(search_result).get('fields', {})

        search_list_result = lambda: render(
            request,
            template,
            {'query': True, 'object_list': search_result, 'post_count': post_count}
        )

        left_sidebar_filters = lambda: render(
            request,
            'realty/left_sidebar_filters.html',
            {
                'facets': self.check_boxes_filters(facets),
                'conditions': request.GET,
                'cities': self.cities,
                'property_type_type': sorted(self.check_boxes_filters_property_type_type(facets)['property_type_type']),
                'energy_class': sorted(self.check_boxes_filters_energy(facets)['energy_class']),
                'property_types': property_types,
                'property_type_types': property_type_types,
                'selected_pt': int(request.GET.get('property_type', 0)),
                'selected_city': int(request.GET.get('city', 0)),
                'property_tt_dict': property_tt_dict,
            }
        )
        if not request.GET.get('scroll'):
            AJAX_TEMPLATE = {
                'inner-fragments': {'#search-list-result': search_list_result(),
                                    '#left-sidebar-filters': left_sidebar_filters(), },
            }
        else:
            AJAX_TEMPLATE = {
                'append-fragments': {'#search-list-result': search_list_result(), }
            }

        return AJAX_TEMPLATE


class MapView(TemplateView):
    template_name = 'realty/map.html'

    def get_context_data(self, **kwargs):
        context = super(MapView, self).get_context_data(**kwargs)
        context['property_types'] = property_types
        context['property_type_types'] = property_type_types
        context['energy_class'] = Realty.ENERGY_CHOICES
        return context


class MapAjaxView(View):
    def __init__(self):
        self.sq = SearchQuerySet().filter(big_category='realty')
        self.sort = '-date_index'
        self.sort_dict = {
            'date_index': 'creation_date',
            '-date_index': '-creation_date',
            'price_index': 'price',
            '-price_index': '-price',
            'year_index': 'year_built',
            '-year_index': '-year_built',
        }

    def get(self, request):
        action = request.GET.get('action')
        if action and hasattr(self, action):
            return getattr(self, action)(request)
        else:
            return HttpResponse('Action parameter not provided or incorrect', status=400)

    def get_coordinates(self, request):
        sq = self.handle_general_filters(request.GET)

        if 'distance' in request.GET:
            distance = D(km=float(request.GET['distance']))
            point = Point(float(request.GET['lng']), float(request.GET['lat']))
            sq = sq.dwithin('location_index', point, distance)
        else:
            south_west = Point(float(request.GET['west']), float(request.GET['south']))
            noth_east = Point(float(request.GET['east']), float(request.GET['north']))
            sq = sq.within('location_index', south_west, noth_east)
        location_facets = sq.facet('location_index').facet_counts()['fields']['location_index']
        if location_facets:
            return HttpResponse(json.dumps(location_facets))
        else:
            return HttpResponse(json.dumps({'error': _('No results found, change filter parameters')}))

    def get_data_by_coords(self, request):
        start_index = int(request.GET.get('page', 1)) * 10 - 10
        sq = self.handle_general_filters(request.GET)
        ids = sq.filter(
            location_index_exact=request.GET['key_coords']
        ).order_by(self.sort).values_list('pk', flat=True)[start_index: start_index + 10]
        realties = Realty.objects.filter(id__in=ids).order_by(self.sort_dict.get(self.sort, '-creation_date'))
        result = []
        for item in realties:
            dict_item = model_to_dict(item, fields=['id', 'price', ])
            if item.property_type_type_id:
                dict_item['property_type'] = item.property_type_type.name
            else:
                dict_item['property_type'] = item.property_type.name

            for image in item.images.all()[:1]:
                dict_item.setdefault('images', []).append(image.image.url)
                thumb = thumbnail.get_thumbnail(image.image, "100x100", crop='center')
                dict_item.setdefault('thumbnails', []).append(thumb.url)
            if 'images' not in dict_item:
                dict_item['images'] = ['/static/assets/base/img/nopic.png']
                dict_item['thumbnails'] = ['/static/assets/base/img/nopic100.png']
            if item.energy_class:
                dict_item['energy_class'] = item.energy_class
            if item.area:
                dict_item['area'] = item.area
            if item.land_area:
                dict_item['land_area'] = item.land_area

            dict_item['deal'] = item.get_deal_display()
            dict_item['url'] = item.get_absolute_url()

            result.append(dict_item)
        return HttpResponse(json.dumps(result))

    def handle_general_filters(self, params):
        sq = self.sq
        if 'deal' in params:
            sq = sq.filter(deal=params['deal'])
        if 'property_type_type[]' in params:
            sq = sq.filter(property_type_type__in=params.getlist('property_type_type[]'))
        elif 'property_type' in params:
            sq = sq.filter(property_type=params['property_type'])
        if 'energy_class[]' in params:
            sq = sq.filter(energy_class__in=params.getlist('energy_class[]'))
        if 'land_area_min' in params:
            sq = sq.filter(land_area__gte=params['land_area_min'])
        if 'land_area_max' in params:
            sq = sq.filter(land_area__lte=params['land_area_max'])
        if 'area_min' in params:
            sq = sq.filter(area__gte=params['area_min'])
        if 'area_max' in params:
            sq = sq.filter(area__lte=params['area_max'])
        if 'price_index_min' in params:
            sq = sq.filter(price_index__gte=params['price_index_min'])
        if 'price_index_max' in params:
            sq = sq.filter(price_index__lte=params['price_index_max'])
        if 'year_index_min' in params:
            sq = sq.filter(year_index__gte=params['year_index_min'])
        if 'year_index_max' in params:
            sq = sq.filter(year_index__lte=params['year_index_max'])
        if 'q' in params:
            sq = sq.filter(text__contains=params['q'])
        if 'sort' in params:
            self.sort = params['sort']
        return sq
