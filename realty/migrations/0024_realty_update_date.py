# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0023_realty_is_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='realty',
            name='update_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
