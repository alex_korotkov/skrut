# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0042_auto_20161031_1801'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='moto',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
