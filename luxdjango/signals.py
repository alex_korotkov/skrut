# -*- coding: utf-8 -*-
from haystack.exceptions import NotHandled
from haystack.signals import RealtimeSignalProcessor

class MyRealtimeSignalProcessor(RealtimeSignalProcessor):

    def handle_save(self, sender, instance, **kwargs):
        if hasattr(instance, 'is_active') and instance.is_active == False:
            self.handle_delete(sender, instance, **kwargs)
        else:
            using_backends = self.connection_router.for_write(instance=instance)
            for using in using_backends:
                try:
                    index = self.connections[using].get_unified_index().get_index(sender)
                    index.update_object(instance, using=using)
                except NotHandled:
                    pass