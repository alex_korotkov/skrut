# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0043_auto_20161122_0401'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='update_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='moto',
            name='update_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
