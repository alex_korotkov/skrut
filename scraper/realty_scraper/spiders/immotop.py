# -*- coding: utf-8 -*-
import scrapy, re
from ..items import RealtyItem
from realty.models import Realty

from scrapy.shell import inspect_response


class ImmotopSpider(scrapy.Spider):
    name = 'immotop'
    allowed_domains = ['immotop.lu']
    start_urls = ['http://www.immotop.lu/search/+/index1.html']
    custom_settings = {
        'COOKIES_ENABLED': True
    }
    adpp = 15 # adv per page

    def parse(self, response):
        self.logger.info('//// start parsing ////')

        adv_list = response.xpath('//div[@id="listings_div"]/div[starts-with(@id, "listing_")]')
        for adv in adv_list:
            link = adv.xpath('./a[@class="property_content"]/@href').extract_first()
            url = response.urljoin(link)
            realty = Realty.objects.filter(url=url).only('price').first()
            title = adv.xpath('.//h2/text()').extract_first()
            if re.match(r'^\s*\xe0', title):
                pass
            if not realty:
                yield scrapy.Request(url, callback=self.parse_detail)
            else:
                self.logger.info('/// already exists ///')
                price = adv.xpath('.//div[@class="price"]/text()').extract_first()
                if price and u'\u2013' in price:
                    self.logger.info('/// combo price ///')
                else:
                    price = re.sub(r'\D', '', price) if price else None
                    price = int(price) if price else None
                    item = RealtyItem()
                    item['model_obj'] = realty
                    if price != realty.price:
                        item['new_price'] = price
                    item['update'] = True
                    yield item

        ads_amount = int(response.xpath('//strong[@id="cur_count"]/text()').extract_first())
        pages_amount = ads_amount / self.adpp
        current_page = int(re.search(r'\d+', response.url).group())
        if current_page < pages_amount:
            next_page = current_page + 1
            self.logger.info('///////////////////// go to next page /////////////////////')
            yield scrapy.Request(
                'http://www.immotop.lu/search/index%s.html' % next_page, callback=self.parse
            )

    def parse_detail(self, response):
        self.logger.info('/// parse detail ///')
        item = RealtyItem()
        item['url'] = response.url

        header = response.xpath('//div[@id="header"]/h1/text()').extract_first()
        if not header:
            inspect_response(response, self)

        item['deal'] = 2 if 'louer' in header else 1

        breadcrumbs = response.xpath('//div[@id="bc_div"]/a/text()').extract()
        if breadcrumbs[2].strip() == 'Neuf':
            item['new_building'] = True
        if len(breadcrumbs) > 4:
            item['property_type'] = breadcrumbs[4].strip()
        else:
            item['property_type'] = breadcrumbs[3].strip()

        detail_block = response.xpath('//div[starts-with(@id, "listing_")]/div[@class="well"]')
        tag_block = detail_block.xpath(
            './div/span[starts-with(@class, "listing_tags")][1]/parent::div'
        )
        info_block = detail_block.xpath('.//div[@id="adInfo"]')
        seller_block = detail_block.xpath('.//div[@id="ainf"]')
        location_block = detail_block.xpath('.//span[text()="Pays:"]/ancestor::div[2]')

        price = detail_block.xpath('./div/div[@class="bolder"]/text()').extract_first()
        price = price.strip() if price else ''
        if price and '\u2013' not in price:
            item['price'] = re.sub(r'\D', '', price)


        energy = tag_block.xpath('./div[@class="tips my_energy"]/span/text()').extract()
        if energy:
            energy = [x.strip() for x in energy]
            if energy[0]:
                item['energy_class'] = energy[0]
            if energy[1]:
                item['termal_protection_class'] = energy[1]

        area = tag_block.xpath('./span[starts-with(@data-tip, "Surface (")]/text()').extract_first()
        area = re.search(r'\d+', area) if area else None
        if area:
            item['area'] = area.group()

        land_area = tag_block.xpath(
            './span[starts-with(@data-tip, "Surface du")]/text()'
        ).extract_first()
        if land_area:
            land_area = re.sub(r'\s', '', land_area)
            land_area = re.search(r'\d+[.,]?\d{0,2}', land_area)
        if land_area:
            item['land_area'] = land_area.group().replace(',', '.')

        rooms = tag_block.xpath('./span[starts-with(@data-tip, "Chambres")]/text()').extract_first()
        rooms = re.sub(r'\D', '', rooms) if rooms else None
        if rooms:
            item['rooms'] = rooms

        bathroom = tag_block.xpath('./span[starts-with(@data-tip, "Salles")]/text()').extract_first()
        if bathroom:
            bathroom = re.sub(r'\D', '', bathroom)
            item['bathroom'] = bathroom if bathroom else 1

        floor = tag_block.xpath(u'./span[contains(text(), "\xc9tage")]/text()').extract_first()
        floor = re.sub(r'\D', '', floor) if floor else None
        if floor:
            item['floor'] = floor

        total_floors = tag_block.xpath(
            './span[starts-with(text(), "Total d")]/text()'
        ).extract_first()
        total_floors = re.sub(r'\D', '', total_floors) if total_floors else None
        if total_floors:
            item['total_floors'] = total_floors

        garages= tag_block.xpath('./span[contains(text(), "Garage")]/text()').extract_first()
        if garages:
            garages = re.sub(r'\D', '', garages)
            item['garage'] = garages if garages else 1

        garden = tag_block.xpath('./span[starts-with(text(), "Jardin")]/text()').extract_first()
        if garden:
            garden = re.search(r'\d+[.,]?\d{0,2}', garden)
            item['garden'] = garden.group().replace(',', '.') if garden else 1

        terrace = tag_block.xpath('./span[starts-with(text(), "Terrasse")]/text()').extract_first()
        if terrace:
            terrace = re.search(r'\d+', terrace)
            item['terrace'] = terrace.group() if terrace else 1

        balcony = tag_block.xpath('./span[starts-with(text(), "Balcon")]/text()').extract_first()
        if balcony:
            balcony = re.search(r'\d+', balcony)
            item['balcony'] = balcony.group() if balcony else 1

        year_built = tag_block.xpath(
            './span[contains(@data-tip, "de construction")]/text()'
        ).extract_first()
        year_built = year_built.strip() if year_built else None
        if year_built:
            item['year_built'] = year_built

        image_urls = detail_block.xpath('.//ul[@id="eff_holder"]/li/img/@src').extract()
        if image_urls:
            item['image_urls'] = image_urls

        description = info_block.xpath(
            './/h2[starts-with(text(), "Description")]/following-sibling::div/text()'
        ).extract()
        if description:
            description = [x.strip() for x in description if x.strip()]
            item['description'] = '<br>'.join(description)

        seller_type = seller_block.xpath('./div/em/text()').extract_first()
        if seller_type == u'Cet objet vous est propos\xe9 par l\u2019annonceur:':
            item['seller_type'] = 1 # private
            seller = seller_block.xpath('.//span[@class="bolder"]/text()').extract_first()
            seller = seller.strip() if seller else None
        else:
            item['seller_type'] = 2 # professional
            seller = seller_block.xpath('.//div[@class="bolder"]/text()').extract_first()
            seller = seller.strip() if seller else None
        if seller:
            item['seller'] = seller

        phones = seller_block.xpath('./div[@class="hide dealer_phones"]/div/text()').extract()
        phones = [re.search(r'[+\d\s().-]+', x) for x in phones]
        phones = [x.group().strip() for x in phones]
        phones = [x for x in phones if x]
        if phones:
            item['phones'] = phones

        if location_block:
            country = location_block.xpath('.//span[text()="Pays:"]/following-sibling::span/text()'
            ).extract_first()
            if country:
                item['country'] = country
            city = location_block.xpath(
                u'.//span[text()="Localit\xe9:"]/following-sibling::span/text()'
            ).extract_first()
            if not city:
                city = location_block.xpath(
                    u'.//span[text()="Ville:"]/following-sibling::span/text()'
                ).extract_first()
            if not city:
                city = location_block.xpath(
                    u'.//span[text()="Commune:"]/following-sibling::span/text()'
                ).extract_first()
            address = location_block.xpath(
                './/span[text()="Adresse:"]/following-sibling::span/text()'
            ).extract_first()
            if address:
                item['address'] = address
        else:
            title = response.xpath('//head/title/text()').extract_first()
            country = re.search(r'\(\w+\)', title)
            if country:
                item['country'] = country.group()[1:-1]
                city = re.search(r'(?<=(endre|louer) \xe0 )[^( \()]+', title)
                city = city.group() if city else None
        if city:
            if '/' in city:
                city.replace('/', '-')
            item['city'] = city.strip()

        yield item

