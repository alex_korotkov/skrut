# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import datetime, json, random, re

from django.contrib.postgres.fields import ArrayField
from django.core import validators
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone
from django.utils.text import force_unicode
from django.utils.translation import ugettext as _
from django.core.validators import RegexValidator

from AdminConfig.models import FacebookPageAccessToken
from luxdjango.utils import img_upload_to, get_geo_point, post_to_facebook

YEAR_CHOICES = [(r, r) for r in range(datetime.datetime.now().year, 1886, -1)]
MONTH_CHOICES = [(r, r) for r in range(1, 13)]
YEARMONTH_INPUT_FORMATS = (
    '%m-%Y', '%m/%Y', '%m/%y',  # '2006-10', '10/2006', '10/06'
)


class Manufacturer(models.Model):
    manufacturer_code = models.CharField(max_length=50, null=True, verbose_name=_('Manufacturer Code'))
    manufacturer_name = models.CharField(max_length=50, null=True, verbose_name=_('Manufacturer Name'))
    manufacturer_details = models.TextField(null=True, verbose_name=_('Manufacturer Detials'))

    def __unicode__(self):
        return "%s %s %s" % (self.manufacturer_code, self.manufacturer_name, self.manufacturer_details)


class Brand(models.Model):
    # Vehicle = models.ForeignKey(Vehicle)
    brand_code = models.CharField(max_length=30, blank=True, null=True, verbose_name=_('Brand Code'))
    brand_name = models.CharField(max_length=50, blank=True, null=True, verbose_name=_('Brand Name'))

    def __unicode__(self):
        return self.brand_name


class Model(models.Model):
    brand = models.ForeignKey(Brand)
    model_code = models.CharField(max_length=50, verbose_name=_('Model Code'))
    model_name = models.CharField(max_length=50, verbose_name=_('Model Name'))

    def __unicode__(self):
        return unicode(self.model_name)


SELLER_CHOICES = ((1, _('Private seller')),
                  (2, _('Professional  seller')))


class Seller(models.Model):
    name = models.CharField(_('Seller'), max_length=100, null=True)
    phones = models.CharField(_('Phone'), max_length=100, null=True)
    location = models.CharField(_('Address'), max_length=100, null=True)
    email = models.EmailField(null=True, blank=True)
    seller_type = models.PositiveIntegerField(_('Seller type'), choices=SELLER_CHOICES, default=1)

    def __unicode__(self):
        return unicode(self.name)

    @property
    def phone_list(self):
        if self.phones:
            return self.phones.split(',')
        return []


class Country(models.Model):
    name = models.CharField(_('Country'), max_length=50)
    calling_code = models.CharField(max_length=10)

    def __unicode__(self):
        return self.name


class BaseColor(models.Model):
    color = models.CharField(max_length=50)


class Color(models.Model):
    color = models.CharField(max_length=50)
    base_color = models.ForeignKey(BaseColor, null=True, blank=True, related_name='uscolor')

    def __unicode__(self):
        return self.color


class Vehicle(models.Model):
    VEHICLE_CHOICES = (
        (1, _('Car')),
        (2, _('MotorCycle')),
        (3, _('Utility'))
    )

    country = models.ForeignKey(Country, null=True)
    vehicle_type = models.PositiveIntegerField(choices=VEHICLE_CHOICES, default=1)
    brand = models.ForeignKey(Brand, null=True)
    model = models.ForeignKey(Model, null=True)
    model_type = models.CharField(_('Model type'), max_length=100, null=True, blank=True)
    model_month = models.IntegerField(_('Model Month'), choices=MONTH_CHOICES, null=True, blank=True)
    model_year = models.IntegerField(_('Model year'), choices=YEAR_CHOICES, null=True, blank=True)
    seller = models.ForeignKey(Seller, null=True)
    km = models.PositiveIntegerField(_('Mileage'), null=True, blank=True)
    price = models.PositiveIntegerField(_('Price'), null=True, blank=True)
    energy = models.CharField(_('Fuel'), max_length=50, blank=True)
    gear_box = models.CharField(_('Gear Box'), max_length=50, blank=True)
    cylinder = models.PositiveIntegerField(_('Cylinder volume'), null=True, blank=True)
    power = models.PositiveIntegerField(_('Power'), null=True, blank=True)
    color = models.ForeignKey(Color, null=True, blank=True)
    url = models.URLField("URL", null=True, blank=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    creation_date = models.DateTimeField(default=timezone.now)
    update_date = models.DateTimeField(default=timezone.now)
    places = models.PositiveIntegerField(blank=True, null=True)
    weight = models.PositiveIntegerField(blank=True, null=True)
    price_history = ArrayField(models.IntegerField(), blank=True, null=True)
    options = models.TextField(blank=True)
    description = models.TextField(blank=True)
    author = models.ForeignKey('users.User', null=True, blank=True)
    city = models.ForeignKey("realty.City", null=True, blank=True, on_delete=models.PROTECT)
    phones = ArrayField(
        models.CharField(max_length=24, validators=[
            RegexValidator(r'^[\s\+0-9]{6,17}$', 'Enter a valid phone number.')
        ]),
        null=True,
    )
    is_active = models.BooleanField(default=True)
    seller_type = models.PositiveIntegerField(_('Seller type'), choices=SELLER_CHOICES, default=1)
    # need_moderation = models.BooleanField(_('Need moderation'), default=False)
    # user_ip = models.CharField(max_length=50)

    class Meta:
        abstract = True

    @property
    def big_category(self):
        return u'vehicles'

    def get_options(self):
        return json.loads(self.options)

    def post_on_facebook(self):
        post_to_facebook(
            '{} {}'.format(self.brand.brand_name, self.model.model_name),
            self.get_absolute_url(),
            self.description,
            self,
            self.images.all()[0].image.url if self.images.all() else None
        )

    def __unicode__(self):
        return unicode(self.brand.brand_name)

    def get_location(self):
        if self.latitude and self.longitude:
            return get_geo_point(longitude=self.longitude,
                                 latitude=self.latitude)
        return None

    def update_price(self, new_price):
        if self.price != new_price:
            if self.price_history:
                index = len(self.price_history) - 1
                if self.price and self.price != self.price_history[index]:
                    self.price_history.append(self.price)
                elif new_price == self.price_history[index]:
                    del self.price_history[index]
            elif self.price:
                self.price_history = [self.price]
            self.price = new_price
            self.save()

    def update_update_date(self):
        self.update_date = timezone.now()
        self.save()


    @property
    def domain(self):
        if self.url:
            domain = re.search(r'\/\/([^\/]+)', self.url)
            if domain:
                return domain.groups()[0]
        return ''


class Car(Vehicle):
    doors = models.PositiveIntegerField(_('Doors'), null=True, blank=True)
    finition = models.CharField(_('Finition'), max_length=50, null=True, blank=True)
    color_inside = models.ForeignKey(Color, null=True, related_name='car_inside', blank=True)

    def get_absolute_url(self):
        return reverse('vehicles:car-details', kwargs={'pk': self.id})

    @property
    def sub_category(self):
        return 1

    @property
    def sub_category_name(self):
        return _('Cars')


class CarImage(models.Model):
    car = models.ForeignKey(Car, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(upload_to=img_upload_to)


class Moto(Vehicle):
    def get_absolute_url(self):
        return reverse('vehicles:moto-details', kwargs={'pk': self.id})

    @property
    def sub_category(self):
        return 2

    @property
    def sub_category_name(self):
        return _('Motorbikes')


class MotoImage(models.Model):
    moto = models.ForeignKey(Moto, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(upload_to=img_upload_to)


class CarFacebookPost(models.Model):
    car = models.ForeignKey(Car, on_delete=models.CASCADE, related_name='facebookpost_set')
    post_id = models.CharField(_('post id'), max_length=50)
    access_token = models.ForeignKey(FacebookPageAccessToken, on_delete=models.CASCADE)


class MotoFacebookPost(models.Model):
    moto = models.ForeignKey(Moto, on_delete=models.CASCADE, related_name='facebookpost_set')
    post_id = models.CharField(_('post id'), max_length=50)
    access_token = models.ForeignKey(FacebookPageAccessToken, on_delete=models.CASCADE)
