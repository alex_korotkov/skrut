# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0045_auto_20161202_1351'),
    ]

    operations = [
        migrations.AlterField(
            model_name='basecolor',
            name='color',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='basecolor',
            name='color_en',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='basecolor',
            name='color_fr',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
