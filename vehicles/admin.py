from django.contrib import admin
from .models import Car, Brand, Model, Seller, Moto


# Register your models here.
class CarAdmin(admin.ModelAdmin):
    list_display = (
        'brand', 'model', 'model_type', 'model_year', 'km', 'price', 'energy',
        'gear_box', 'cylinder', 'power', 'doors', 'creation_date', 'phones', 'is_active', 'url',
    )
    admin_order_field = 'brand'

    # def seller_contact(self, obj):
    #     if obj.seller:
    #         print "here"
    #         print obj.seller.contact
    #         return obj.seller.contact
    #     print dir(obj.seller)
    # def seller_location(self, obj):
    #     if obj.seller:
    #         return  obj.seller.location

    # def seller(self, obj):
    #     if obj.seller:
    #         return obj.seller.seller
    #     # print dir(obj.seller.location)
    #     # import sys
    #     # sys.exit(1)
    #     # if obj:
    #     #   print obj.seller.seller
    #         # return obj.seller.contct

# class SellerAdmin(admin.ModelAdmin):
#     list_display = ('seller','contact','location')
#     admin_order_field = 'seller'
#     show_change_link = True

# model_seller.admin_order_field  = 'seller'  #Allows column order sorting
#    get_name.short_description = 'Author Name'  #Renames column head


class MotoAdmin(admin.ModelAdmin):
    list_display = (
        'brand', 'model', 'model_type', 'model_year', 'km', 'price', 'energy',
        'gear_box', 'cylinder', 'power', 'creation_date', 'phones', 'is_active', 'url',
    )


class SellerAdmin(admin.ModelAdmin):
    list_display = ('name', 'phones', 'email', 'seller_type')

admin.site.register(Brand)
admin.site.register(Model)
admin.site.register(Moto, MotoAdmin)
# admin.site.register(Seller, SellerAdmin)
admin.site.register(Car, CarAdmin)
admin.site.register(Seller, SellerAdmin)
