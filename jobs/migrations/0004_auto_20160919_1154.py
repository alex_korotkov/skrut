# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0003_category_name_variants'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='JobType',
            new_name='WorkHours',
        ),
        migrations.RenameField(
            model_name='job',
            old_name='job_type',
            new_name='work_hours',
        ),
        migrations.RenameField(
            model_name='workhours',
            old_name='job_type',
            new_name='work_hours',
        ),
        migrations.AddField(
            model_name='job',
            name='url',
            field=models.URLField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='contract',
            name='contract',
            field=models.CharField(max_length=100),
        ),
    ]
