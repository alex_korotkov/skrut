import uuid
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.postgres.fields import ArrayField
from django.core.validators import RegexValidator
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext as _

from classifieds.models import Classified
from jobs.models import Job
from luxdjango.users.managers import UserManager
from realty.models import Realty
from vehicles.models import Country, Car, Moto


class User(AbstractBaseUser):
    email = models.EmailField(verbose_name='email address', max_length=255, unique=True)
    is_active = models.BooleanField(default=True)
    hide_phones = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    user_country = models.ForeignKey(Country, verbose_name=_("User country"), null=True)
    location_json = models.CharField(max_length=500, null=True, blank=True)
    phones = ArrayField(
        models.CharField(max_length=24, validators=[
            RegexValidator(r'^[\s\+0-9]{6,17}$', 'Enter a valid phone number.')
        ]),
        null=True, blank=True
    )

    objects = UserManager()
    USERNAME_FIELD = 'email'

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __unicode__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    def is_verified(self):
        return (self.socialaccount_set.all().exists()
                or self.emailaddress_set.filter(email=self.email, verified=True).exists())

    @property
    def location(self):
        if self.location_json:
            return json.loads(self.location_json)
        else:
            return None


class Token(models.Model):
    user = models.ForeignKey(User)
    token = models.CharField(max_length=40, primary_key=True)
    created = models.DateTimeField(default=timezone.now)
    car = models.ForeignKey(Car, blank=True, null=True)
    moto = models.ForeignKey(Moto, blank=True, null=True)
    realty = models.ForeignKey(Realty, blank=True, null=True)
    job = models.ForeignKey(Job, blank=True, null=True)
    classified = models.ForeignKey(Classified, blank=True, null=True)

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = self.generate_token()
        return super(Token, self).save(*args, **kwargs)

    def generate_token(self):
        return uuid.uuid4().hex

    def __unicode__(self):
        return self.token


CATEGORY_CHOICES = ((1, 'vehicles'), (2, 'realty'), (3, 'jobs'))


class Subscription(models.Model):
    category = models.PositiveIntegerField(choices=CATEGORY_CHOICES, blank=False, default=1)
    token = models.ForeignKey(Token, blank=True, null=True)
    user = models.ForeignKey(User)
    filters = models.CharField(max_length=500)
    last_notification = models.DateTimeField(null=True, blank=True)
    category = models.PositiveIntegerField(choices=CATEGORY_CHOICES)
    location_json = models.CharField(max_length=500, null=True, blank=True)

    @property
    def location(self):
        if self.location_json:
            return json.loads(self.location_json)
        else:
            return None


class TrustedCountry(models.Model):
    country = models.ForeignKey(Country)