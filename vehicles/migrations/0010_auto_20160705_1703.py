# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0009_car_price_history'),
    ]

    operations = [
        migrations.AddField(
            model_name='country',
            name='name_en',
            field=models.CharField(max_length=50, null=True, verbose_name='Country'),
        ),
        migrations.AddField(
            model_name='country',
            name='name_fr',
            field=models.CharField(max_length=50, null=True, verbose_name='Country'),
        ),
        migrations.AlterField(
            model_name='country',
            name='name',
            field=models.CharField(default='empty value', max_length=50, verbose_name='Country'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='seller',
            name='location',
            field=models.CharField(max_length=100, null=True, verbose_name='Address'),
        ),
        migrations.AlterField(
            model_name='seller',
            name='name',
            field=models.CharField(max_length=100, null=True, verbose_name='Seller'),
        ),
        migrations.AlterField(
            model_name='seller',
            name='phones',
            field=models.CharField(max_length=100, null=True, verbose_name='Phone'),
        ),
    ]
