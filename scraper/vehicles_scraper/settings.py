from scraper.settings_common import *

BOT_NAME = 'scraper_vehicles'

SPIDER_MODULES = ['scraper.vehicles_scraper.spiders']
NEWSPIDER_MODULE = 'scraper.vehicles_scraper.spiders'

ITEM_PIPELINES = {
    'scraper.vehicles_scraper.pipelines.ObjModelPipeline': 300,
    'scraper.vehicles_scraper.pipelines.StoreImgPipeline': 301,
    'scraper.vehicles_scraper.pipelines.SaveImgModelPipeline': 302
}
