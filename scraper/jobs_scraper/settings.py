from scraper.settings_common import *

BOT_NAME = 'scraper_jobs'

SPIDER_MODULES = ['scraper.jobs_scraper.spiders']
NEWSPIDER_MODULE = 'scraper.jobs_scraper.spiders'

ITEM_PIPELINES = {
    'scraper.jobs_scraper.pipelines.ObjModelPipeline': 300,
}