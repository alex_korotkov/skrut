from django.contrib import admin

from .models import Job, Employer


class JobAdmin(admin.ModelAdmin):
    list_display = (
        'title', 'country', 'city', 'address', 'is_active', 'creation_date', 'url'
    )


class EmployerAdmin(admin.ModelAdmin):
    list_display = ('name', 'site', 'email', 'phones', 'country', 'address')


admin.site.register(Job, JobAdmin)
admin.site.register(Employer, EmployerAdmin)