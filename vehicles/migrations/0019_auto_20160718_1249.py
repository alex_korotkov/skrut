# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0018_remove_car_vehicle_type'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Vehicle',
        ),
        migrations.AddField(
            model_name='car',
            name='vehicle_type',
            field=models.PositiveIntegerField(default=1, choices=[(1, 'Car'), (2, 'MotorCycle'), (3, 'Utility')]),
        ),
    ]
