# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0003_realty_land_area'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realty',
            name='land_area',
            field=models.DecimalField(null=True, max_digits=7, decimal_places=2),
        ),
    ]
