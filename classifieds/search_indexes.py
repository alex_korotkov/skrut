from haystack import indexes

from .models import Classified


class ClassifiedIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title_index = indexes.CharField(model_attr='title', faceted=True, default=None)
    category_index = indexes.IntegerField(model_attr='category_id', faceted=True, default=None)
    sub_category_index = indexes.IntegerField(model_attr='sub_category_id', faceted=True, default=None)
    sub_sub_category_index = indexes.IntegerField(model_attr='sub_sub_category_id', faceted=True, default=None)
    city_index = indexes.IntegerField(model_attr='city_id', faceted=True, default=None)
    price_index = indexes.IntegerField(model_attr='price', faceted=True, default=None)
    country_index = indexes.IntegerField(model_attr='country_id', faceted=True, default=None)
    date_index = indexes.DateTimeField(model_attr='creation_date')
    big_category = indexes.CharField(model_attr='big_category', faceted=True)

    def get_model(self):
        return Classified

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(is_active=True)
