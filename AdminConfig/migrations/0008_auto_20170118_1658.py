# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('AdminConfig', '0007_template'),
    ]

    operations = [
        migrations.AlterField(
            model_name='template',
            name='template',
            field=ckeditor.fields.RichTextField(blank=True),
        ),
    ]
