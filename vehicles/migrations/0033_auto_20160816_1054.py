# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0032_auto_20160816_0944'),
    ]

    operations = [
        migrations.RunSQL(
            "ALTER TABLE vehicles_car ALTER COLUMN power TYPE integer USING power::integer",
            state_operations = [
                migrations.AlterField(
                    model_name='car',
                    name='power',
                    field=models.PositiveIntegerField(null=True, verbose_name='Power', blank=True),
                ),
            ]
        )
    ]
