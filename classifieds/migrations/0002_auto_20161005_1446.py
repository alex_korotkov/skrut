# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import classifieds.models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0021_auto_20160908_1358'),
        ('vehicles', '0039_car_city'),
        ('classifieds', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClassifiedImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=classifieds.models.img_upload_to)),
            ],
        ),
        migrations.CreateModel(
            name='Seller',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, null=True, verbose_name='Seller')),
                ('phones', models.CharField(max_length=100, null=True, verbose_name='Phone')),
                ('email', models.EmailField(max_length=254, blank=True)),
                ('seller_type', models.PositiveIntegerField(default=1, verbose_name='Seller type', choices=[(1, 'Private seller'), (2, 'Professional  seller')])),
            ],
        ),
        migrations.AddField(
            model_name='classified',
            name='city',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, blank=True, to='realty.City', null=True),
        ),
        migrations.AddField(
            model_name='classified',
            name='country',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='vehicles.Country', null=True),
        ),
        migrations.AddField(
            model_name='classified',
            name='creation_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='classified',
            name='price',
            field=models.PositiveIntegerField(null=True, verbose_name='Price', blank=True),
        ),
        migrations.AddField(
            model_name='classifiedimage',
            name='classified',
            field=models.ForeignKey(to='classifieds.Classified'),
        ),
        migrations.AddField(
            model_name='classified',
            name='seller',
            field=models.ForeignKey(to='classifieds.Seller', null=True),
        ),
    ]
