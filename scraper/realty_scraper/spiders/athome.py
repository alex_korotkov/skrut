# -*- coding: utf-8 -*-
import scrapy, re
from ..items import RealtyItem
from realty.models import Realty

class AthomeSpider(scrapy.Spider):
    name = 'athome'
    allowed_domains = ['athome.lu', 'athome.eu']
    start_urls = [
        'http://www.athome.lu/recherche/resultats/tr/by/st/id,desc',
        'http://www.athome.lu/recherche/resultats/tr/rt/st/id,desc',
    ]

    def parse(self, response):
        adv_list = response.xpath('//div[starts-with(@class, "search-conteneur")]/article')
        for adv in adv_list:
            price = adv.xpath('.//div[@class="price"]/text()').extract_first()
            details = adv.xpath('.//div[@class="details"]/text()').extract_first()
            if (u'disponible' not in details) and (u'\xe0' not in price):
                link = adv.xpath('.//a[@class="photo main"]/@href').extract_first()
                url = response.urljoin(link)
                realty = Realty.objects.filter(url=url).only('price').first()
                if not realty:
                    logo = adv.xpath('.//a[@class="logo"]').extract_first()
                    seller_type = 2 if logo else 1
                    yield scrapy.Request(url, callback=self.parse_detail, meta={'seller_type': seller_type})
                else:
                    self.logger.info('/// already exists ///')
                    price = re.sub(r'\D', '', price)
                    price = int(price) if price else None
                    item = RealtyItem()
                    item['model_obj'] = realty
                    if price != realty.price:
                        item['new_price'] = price
                    item['update'] = True
                    yield item

        next_page = response.xpath('//a[@rel="next"]/@href').extract_first()
        if next_page:
            self.logger.info('///////////////////// go to next page /////////////////////')
            yield scrapy.Request(response.urljoin(next_page), callback=self.parse)

    def parse_detail(self, response):
        self.logger.info('/// parse detail ///')
        item = RealtyItem()
        item['url'] = response.url
        item['seller_type'] = response.meta['seller_type']
        script = response.xpath('//script[contains(text(), "country")]/text()').extract_first()

        country = re.search(r'(?<=country":")[^"]*', script).group() if script else None
        if not country:
            country = response.xpath('//div[@itemprop="country-name"]/text()').extract_first()
        if country:
            item['country'] = country.strip()
            city = re.search(r'(?<=city":")[^"]*', script).group() if script else None
            if not city:
                city = response.xpath(
                    '//div[@id="offer-situation-description"]//span[@class="city"]/text()'
                ).extract_first()
            if city:
                city = re.search(r'[^,]+', city).group()
                item['city'] = city.strip()
            title = response.xpath('//meta[@name="og:title"]/@content').extract_first()
            item['deal'] = 1 if u'\xe0 vendre' in title else 2

            property_type = re.search(r'(.+)(?=\xe0 (vendre|louer))', title).group().strip()
            if 'Garage' in property_type:
                property_type = 'Garage'
            elif 'Jardin' in property_type:
                property_type = 'Terrain'
            elif 'Location de vacances' in property_type:
                property_type = 'Chambre'
            item['property_type'] = property_type.strip()

            description = response.xpath('//div[@id="offer-description"]//p/text()').extract()
            if description:
                description = [x.strip() for x in description if x.strip()]
                if description:
                    item['description'] = '<br>'.join(description)

            resume_block = response.xpath('//div[@class="resume clearfix"]')

            price = resume_block.xpath(
                './/div[starts-with(@class, "price")]/span/text()'
            ).extract_first()
            price = re.sub(r'\D', '', price) if price else None
            if price:
                item['price'] = price

            rooms = resume_block.xpath(
                './/i[@class="athome-icon-bedroom"]/parent::span/text()'
            ).extract_first()
            rooms = rooms.strip() if rooms else None
            if rooms:
                item['rooms'] = rooms

            garage = resume_block.xpath(
                './/i[@class="athome-icon-car"]/parent::span/text()'
            ).extract_first()
            garage = garage.strip() if garage else None
            if garage:
                item['garage'] = garage

            area = response.xpath('//div[@class="living_surface"]/span/text()').extract_first()
            area = re.search(r'\d+', area) if area else None
            if area:
                item['area'] = area.group()

            land_area = response.xpath('//div[@class="ground_surface"]/span/text()').extract_first()
            land_area = re.search(r'\d+(.|,)?\d{0,2}', land_area) if land_area else None
            if land_area:
                item['land_area'] = land_area.group().replace(',', '.')


            info_block = response.xpath('//div[@id="other_info"]')

            year_built = info_block.xpath(
                u'.//p[contains(text(), "Ann\xe9e de construction")]/span/text()'
            ).extract_first()
            if year_built:
                if 'neuf' in year_built:
                    item['new_building'] = True
                else:
                    year_built = re.sub(r'\D', '', year_built)
                    if year_built and len(year_built) == 4:
                        item['year_built'] = year_built

            energy = info_block.xpath('.//div[@class="offerEnergyTag"]/div/text()').extract()
            if energy:
                item['energy_class'] = energy[0]
                if len(energy) > 1:
                    item['termal_protection_class'] = energy[1]

            floor = info_block.xpath(
                u'.//p[contains(text(), "\xc9tage")]/span/text()'
            ).extract_first()
            if floor:
                floor = floor.strip()
                if floor:
                    item['floor'] = floor

            spec_block = response.xpath('//div[@id="specifications"]')
            balcony = spec_block.xpath('.//span[text()="Balcon"]')
            if balcony:
                item['balcony'] = 1

            terrace = spec_block.xpath(
                './/span[text()="Terrasse"]/following-sibling::span/text()'
            ).extract_first()
            if terrace:
                if terrace == 'oui':
                    item['terrace'] = 1
                else:
                    terrace = re.search(r'\d+', terrace)
                    if terrace:
                        item['terrace'] = terrace.group()

            garden = spec_block.xpath(
                './/span[text()="Jardin"]/following-sibling::span/text()'
            ).extract_first()
            if garden:
                if garden in ['oui', '1']:
                    item['garden'] = 1
                else:
                    garden = re.search(r'\d+(.|,)?\d{0,2}', garden)
                    if garden:
                        item['garden'] = round(int(garden.group().replace(',', '.'))/100.0, 2)  # translate meters to ares

            bathroom = spec_block.xpath('.//span[text()="Salle de bain"]')
            if bathroom:
                item['bathroom'] = 1

            agency_block = response.xpath('//div[@class="agency_infos"]')
            if not agency_block:
                agency_block = response.xpath('//div[@class="user_infos"]')
            if agency_block:
                agency_block = agency_block[0]
                raw_phones = agency_block.xpath(
                    './/a[starts-with(@class, "button callAgencyIco")]/@onclick'
                ).extract_first()
                raw_phones = re.search(r'<span>.+</span>', raw_phones) if raw_phones else None
                if raw_phones:
                    raw_phones = raw_phones.group()
                    phones = re.findall(r'[\+\d][\d\s-]+', raw_phones)
                    phones = [x.strip() for x in phones if x.strip()]
                    if phones:
                        item['phones'] = phones

                seller = agency_block.xpath('.//div[@class="agency_name"]/text()').extract_first()
                if seller:
                    item['seller'] = seller

            image_urls = response.xpath('//div[@id="miniaturesbar"]/a/@data-fancybox-href').extract()
            if image_urls:
                item['image_urls'] = image_urls

            yield item






