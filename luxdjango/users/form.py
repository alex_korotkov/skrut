# -*- coding: utf-8 -*-
from django import forms
from django.forms import Form, BooleanField, EmailInput, CheckboxInput

from luxdjango.users.models import User
from django.utils.translation import ugettext as _


class UserForm(forms.Form):
    email = forms.EmailField(required=True, widget=forms.EmailInput(
        attrs={'class': 'form-control  c-square c-theme', 'title': 'email'},
    ))
