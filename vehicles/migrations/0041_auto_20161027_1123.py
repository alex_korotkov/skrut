# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import vehicles.models
import django.db.models.deletion
from django.conf import settings
import django.contrib.postgres.fields
import django.utils.timezone
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0022_seller_email'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('vehicles', '0040_seller_email'),
    ]

    operations = [
        migrations.CreateModel(
            name='Moto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vehicle_type', models.PositiveIntegerField(default=1, choices=[(1, 'Car'), (2, 'MotorCycle'), (3, 'Utility')])),
                ('model_type', models.CharField(max_length=100, null=True, verbose_name='Model type', blank=True)),
                ('model_month', models.IntegerField(blank=True, null=True, verbose_name='Model Month', choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10), (11, 11), (12, 12)])),
                ('model_year', models.IntegerField(blank=True, null=True, verbose_name='Model year', choices=[(2016, 2016), (2015, 2015), (2014, 2014), (2013, 2013), (2012, 2012), (2011, 2011), (2010, 2010), (2009, 2009), (2008, 2008), (2007, 2007), (2006, 2006), (2005, 2005), (2004, 2004), (2003, 2003), (2002, 2002), (2001, 2001), (2000, 2000), (1999, 1999), (1998, 1998), (1997, 1997), (1996, 1996), (1995, 1995), (1994, 1994), (1993, 1993), (1992, 1992), (1991, 1991), (1990, 1990), (1989, 1989), (1988, 1988), (1987, 1987), (1986, 1986), (1985, 1985), (1984, 1984), (1983, 1983), (1982, 1982), (1981, 1981), (1980, 1980), (1979, 1979), (1978, 1978), (1977, 1977), (1976, 1976), (1975, 1975), (1974, 1974), (1973, 1973), (1972, 1972), (1971, 1971), (1970, 1970), (1969, 1969), (1968, 1968), (1967, 1967), (1966, 1966), (1965, 1965), (1964, 1964), (1963, 1963), (1962, 1962), (1961, 1961), (1960, 1960), (1959, 1959), (1958, 1958), (1957, 1957), (1956, 1956), (1955, 1955), (1954, 1954), (1953, 1953), (1952, 1952), (1951, 1951), (1950, 1950), (1949, 1949), (1948, 1948), (1947, 1947), (1946, 1946), (1945, 1945), (1944, 1944), (1943, 1943), (1942, 1942), (1941, 1941), (1940, 1940), (1939, 1939), (1938, 1938), (1937, 1937), (1936, 1936), (1935, 1935), (1934, 1934), (1933, 1933), (1932, 1932), (1931, 1931), (1930, 1930), (1929, 1929), (1928, 1928), (1927, 1927), (1926, 1926), (1925, 1925), (1924, 1924), (1923, 1923), (1922, 1922), (1921, 1921), (1920, 1920), (1919, 1919), (1918, 1918), (1917, 1917), (1916, 1916), (1915, 1915), (1914, 1914), (1913, 1913), (1912, 1912), (1911, 1911), (1910, 1910), (1909, 1909), (1908, 1908), (1907, 1907), (1906, 1906), (1905, 1905), (1904, 1904), (1903, 1903), (1902, 1902), (1901, 1901), (1900, 1900), (1899, 1899), (1898, 1898), (1897, 1897), (1896, 1896), (1895, 1895), (1894, 1894), (1893, 1893), (1892, 1892), (1891, 1891), (1890, 1890), (1889, 1889), (1888, 1888), (1887, 1887)])),
                ('km', models.PositiveIntegerField(null=True, verbose_name='Mileage', blank=True)),
                ('price', models.PositiveIntegerField(null=True, verbose_name='Price', blank=True)),
                ('energy', models.CharField(max_length=50, verbose_name='Fuel', blank=True)),
                ('gear_box', models.CharField(max_length=50, verbose_name='Gear Box', blank=True)),
                ('cylinder', models.PositiveIntegerField(null=True, verbose_name='Cylinder volume', blank=True)),
                ('power', models.PositiveIntegerField(null=True, verbose_name='Power', blank=True)),
                ('url', models.URLField(null=True, verbose_name='URL', blank=True)),
                ('latitude', models.FloatField(null=True, blank=True)),
                ('longitude', models.FloatField(null=True, blank=True)),
                ('creation_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('places', models.PositiveIntegerField(null=True, blank=True)),
                ('weight', models.PositiveIntegerField(null=True, blank=True)),
                ('price_history', django.contrib.postgres.fields.ArrayField(size=None, null=True, base_field=models.IntegerField(), blank=True)),
                ('options', models.TextField(blank=True)),
                ('description', models.TextField(blank=True)),
                ('phones', django.contrib.postgres.fields.ArrayField(null=True, base_field=models.CharField(max_length=24, validators=[django.core.validators.RegexValidator('^[\\s\\+0-9]{6,17}$', 'Enter a valid phone number.')]), size=None)),
                ('author', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('brand', models.ForeignKey(to='vehicles.Brand', null=True)),
                ('city', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, blank=True, to='realty.City', null=True)),
                ('color', models.ForeignKey(blank=True, to='vehicles.Color', null=True)),
                ('country', models.ForeignKey(to='vehicles.Country', null=True)),
                ('model', models.ForeignKey(to='vehicles.Model', null=True)),
                ('seller', models.ForeignKey(to='vehicles.Seller', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MotoImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=vehicles.models.img_upload_to)),
                ('moto', models.ForeignKey(to='vehicles.Moto')),
            ],
        ),
        migrations.AlterField(
            model_name='carimage',
            name='image',
            field=models.ImageField(upload_to=vehicles.models.img_upload_to),
        ),
    ]
