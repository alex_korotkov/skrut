# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0025_auto_20161211_2141'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realtyimage',
            name='realty',
            field=models.ForeignKey(to='realty.Realty'),
        ),
    ]
