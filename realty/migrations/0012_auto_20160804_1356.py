# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0011_auto_20160803_1813'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realty',
            name='bathroom',
            field=models.PositiveIntegerField(null=True, verbose_name='Bathrooms', blank=True),
        ),
        migrations.AlterField(
            model_name='realty',
            name='year_build',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='Year built', validators=[django.core.validators.MaxValueValidator(2016), django.core.validators.MinValueValidator(1900)]),
        ),
    ]
