from haystack.routers import BaseRouter


class CarRouter(BaseRouter):
    def for_write(self, **hints):
        return 'cars'

    def for_read(self, **hints):
        return 'cars'


class MotoRouter(BaseRouter):
    def for_write(self, **hints):
        return 'motorbikes'

    def for_read(self, **hints):
        return 'motorbikes'