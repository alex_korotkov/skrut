# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0027_auto_20160804_1356'),
    ]

    operations = [
        migrations.AlterField(
            model_name='car',
            name='color',
            field=models.ForeignKey(blank=True, to='vehicles.Color', null=True),
        ),
        migrations.AlterField(
            model_name='car',
            name='color_inside',
            field=models.ForeignKey(related_name='car_inside', blank=True, to='vehicles.Color', null=True),
        ),
        migrations.AlterField(
            model_name='car',
            name='cylinder',
            field=models.CharField(max_length=50, null=True, verbose_name='Cylinder volume', blank=True),
        ),
        migrations.AlterField(
            model_name='car',
            name='doors',
            field=models.PositiveIntegerField(null=True, verbose_name='Doors', blank=True),
        ),
        migrations.AlterField(
            model_name='car',
            name='energy',
            field=models.CharField(max_length=50, null=True, verbose_name='Fuel', blank=True),
        ),
        migrations.AlterField(
            model_name='car',
            name='finition',
            field=models.CharField(max_length=50, null=True, verbose_name='Finition', blank=True),
        ),
        migrations.AlterField(
            model_name='car',
            name='gear_box',
            field=models.CharField(max_length=50, null=True, verbose_name='Gear Box', blank=True),
        ),
        migrations.AlterField(
            model_name='car',
            name='km',
            field=models.PositiveIntegerField(null=True, verbose_name='Mileage', blank=True),
        ),
        migrations.AlterField(
            model_name='car',
            name='power',
            field=models.CharField(max_length=50, null=True, verbose_name='Power', blank=True),
        ),
        migrations.AlterField(
            model_name='car',
            name='price',
            field=models.PositiveIntegerField(null=True, verbose_name='Price', blank=True),
        ),
    ]
