# -*- coding: utf-8 -*-
import logging
from django.utils import timezone
from scrapy.exceptions import DropItem
from scrapy.http import Request
from scrapy.pipelines.images import ImagesPipeline

from scraper.utils import img_path
from vehicles.models import Country
from realty.models import PropertyType, PropertyTypeType, Seller, Realty, City, RealtyImage

PROPERTY_TYPES = PropertyType.objects.all().values_list('id', 'name_fr', 'name_en')
PROPERTY_TYPE_TYPES = PropertyTypeType.objects.all().values_list(
    'id', 'property_type_id', 'name_fr', 'name_en'
)
COUNTRIES = [
    {'id': x[0], 'names': x[1:]}
    for x in Country.objects.all().values_list('id', 'name_en', 'name_fr')
]
REALTY_FIELDS = [x.attname for x in Realty._meta.fields if x.attname not in ['id']]

logger = logging.getLogger('realty_pipline')

class ObjModelPipeline(object):
    def process_item(self, item, spider):
        logger.info('///// realty item process /////')
        if 'new_price' in item or 'update' in item:
            return self.process_item_update(item, spider)
        else:
            return self.process_new_item(item, spider)

    def process_new_item(self, item, spider):
        logger.info('/// process new item ///', item['url'])
        values_dict = {}
        for ptt in PROPERTY_TYPE_TYPES:
            if item['property_type'] in ptt:
                values_dict['property_type_type_id'] = ptt[0]
                values_dict['property_type_id'] = ptt[1]

        if not values_dict.get('property_type_id'):
            for pt in PROPERTY_TYPES:
                if item['property_type'] in pt:
                    values_dict['property_type_id'] = pt[0]

        if item.get('seller'):
            seller = None
            if item['seller'].__class__.__name__ == 'dict':
                if item['seller'].get('name'):
                    seller = Seller.objects.filter(
                        name__iexact=item['seller']['name']
                    ).first()
                    if not seller:
                        seller = Seller.objects.create(**item['seller'])
            else:
                seller = Seller.objects.filter(name__iexact=item['seller']).first()
                if not seller:
                    seller = Seller.objects.create(name=item['seller'])
            if seller:
                values_dict['seller_id'] = seller.id

        seller_type = item.get('seller_type')
        if seller_type:
            values_dict['seller_type'] = seller_type

        if item.get('country'):
            for c in COUNTRIES:
                if item['country'] in c['names']:
                    values_dict['country_id'] = c['id']

        if item.get('city') and values_dict.get('country_id'):
            city = City.objects.filter(
                name__iexact=item['city'], country_id=values_dict['country_id'],
            ).first()
            if not city:
                city = City.objects.create(
                    name=item['city'], country_id=values_dict['country_id'],
                )
            values_dict['city_id'] = city.id
            if not item.get('latitude') and not item.get('longitude'):
                values_dict['latitude'] = city.latitude
                values_dict['longitude'] = city.longitude

        for field in REALTY_FIELDS:
            value = item.get(field)
            if value:
                values_dict[field] = value

        realty = Realty.objects.create(**values_dict)
        item['model_obj'] = realty
        return item

    def process_item_update(self, item, spider):
        if item.get('new_price'):
            logger.info('/// process price update')
            item['model_obj'].update_price(item['new_price'])
        item['model_obj'].update_update_date()
        raise DropItem('Price updated')


class StoreImgPipeline(ImagesPipeline):

    def file_path(self, request, response=None, info=None):
        logger.info('//// file path realty ////')
        return img_path('realty-sc', request.url)


class SaveImgModelPipeline(object):
    def process_item(self, item, spider):
        logger.info('/// process img model save ///')
        for img in item.get('images', []):
            RealtyImage.objects.create(realty_id=item['model_obj'].id, image=img['path'])
        return item
