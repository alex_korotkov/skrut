from haystack.routers import BaseRouter


class ClassifiedsRouter(BaseRouter):
    def for_write(self, **hints):
        return 'classifieds'

    def for_read(self, **hints):
        return 'classifieds'