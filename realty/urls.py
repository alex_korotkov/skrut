# -*- coding: utf-8 -*-
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^get_city/', views.get_city, name='get_city'),
    url(r'^details/(?P<pk>\d+)/$', views.DetailRealtyView.as_view(), name='details'),
    url(r'^edit/(?P<pk>\d+)/$', views.EditRealtyView.as_view(), name='edit'),
    url(r'^add/', views.AddRealtyView.as_view(), name="add-realty"),
    url(r'^search/', views.SearchView.as_view(), name='search'),
    url(r'^search-ajax/', views.ElasticSearchAjax.as_view(), name='search-ajax'),
    url(r'^delete-image/$', view=views.DeleteImage.as_view()),
    url(r'^map/$', view=views.MapView.as_view(), name='map'),
    url(r'^map/ajax/$', view=views.MapAjaxView.as_view(), name='map_ajax'),
]
