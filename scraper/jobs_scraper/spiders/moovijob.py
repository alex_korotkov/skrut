# -*- coding: utf-8 -*-
from sets import Set

import scrapy, re
from ..items import JobItem
import simplejson as json
from jobs.models import Job


class MoovijobJobsSpider(scrapy.Spider):
    name = 'Moovijob'
    allowed_domains = ['moovijob.com']
    start_urls = [
        'http://www.moovijob.com/solr/offer/select/?&bq=&format=json&fq=localization:(%22lu%22)-slug_company:edita-s-a&indent=true&json.wrf=JSON_CALLBACK&q=*:*&qf=company%5E5000+title%5E5000+title_token%5E4000+company_token%5E4000+job_tag+job_tag_token+job_category+job_category_token+description&rows=100&sort=ptime+desc&start=0&wt=json'
    ]

    def parse(self, response):
        self.logger.info('///// parse list /////')
        jsonresponse = json.loads(response.body_as_unicode())
        max_pages = jsonresponse['response']['numFound']
        next_page = re.sub(r'(?<=rows=)\d+', str(max_pages), response.url)
        yield scrapy.Request(next_page, callback=self.all_posts)

    def all_posts(self, response):
        jsonresponse = json.loads(response.body_as_unicode())
        companies = Set([i['slug_company'] for i in jsonresponse['response']['docs']])
        for item in companies:
            yield scrapy.Request('http://www.moovijob.com/company/' + item + '/?format=json',
                                 callback=self.parse_detail)

    def parse_detail(self, response):
        self.logger.info('///// parse detail /////')
        jsonresponse = json.loads(response.body_as_unicode())
        item = JobItem()
        employer = {}
        employer_data = jsonresponse['company']
        employer['name'] = employer_data['title']
        site = employer_data['website']
        if site:
            employer['site'] = site
        email = employer_data['email_contact']
        if email:
            employer['email'] = email
        address = employer_data['street']
        if address:
            employer['address'] = address
        phones = employer_data['office_phone']
        if phones:
            employer['phones'] = phones.split(',')
        employer['country_id'] = 35

        for job in jsonresponse['offers']:
            item['url'] = "http://www.moovijob.com/company/%s/job/%s/%s" % (
                job['slug_company'], job['language'], job['slug']
            )
            if job.get('expired'):
                expired_jobs = Job.objects.filter(url=item['url'])
                if expired_jobs:
                    expired_jobs.delete()
                    self.logger.info('/// expired///')
            elif Job.objects.filter(url=item['url']).exists():
                self.logger.info('/// already exists ///')
                item = JobItem()
                item['model_obj'] = Job.objects.filter(url=url).first()
                item['update'] = True
                yield item
            else:
                item['employer'] = employer
                item['title'] = job['title']
                work_hours = job.get('fulltime')
                if work_hours != None:
                    item['work_hours'] = ['Temps plein'] if work_hours else ['Temps partiel']
                item['category'] = [i['title'] for i in job.get('offer_categories')]
                candidate_level = job.get('title_job_degree_level')
                if candidate_level:
                    item['candidate_level'] = candidate_level

                item['contract'] = [i['title'] for i in job.get('offer_contract_types')]

                city = job.get('city')
                if city:
                    if city.lower() == 'luxemburg':
                        item['city'] = 'Luxembourg'
                    else:
                        item['city'] = city
                item['default_country_id'] = 35
                item['description'] = job.get('description')
                yield item
