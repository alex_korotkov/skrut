# -*- coding: utf-8 -*-
import json
from collections import OrderedDict
from django.shortcuts import render
from django.views.generic import TemplateView
from django_ajax.mixin import AJAXMixin
from haystack.query import SearchQuerySet

from realty.models import City



class ElasticSearchAjax(AJAXMixin, TemplateView):

    def get(self, request, *args, **kwargs):
        q = SearchQuerySet()
        r = request.GET

        q = q.filter(text=r.get('q'))

        q = q.facet('big_category')
        counts = q.facet_counts()
        counts = counts.get('fields', {}).get('big_category', {})
        if counts:
            counts = {x[0]: x[1] for x in counts}
        else:
            counts = {'notfound': 1}
        return counts
