$(document).ready(function () {
    $('.change-lang').on('click', function () {
        var lang = this.getAttribute('data-lang-code');
        var form = $('#lang-form');
        form.find('input[name="language"]').val(lang);
        form.submit();
    });

    $('#subscribe-btn').on('click', function () {
        if ($('#sign-out').length) {
            var filters = getSubscriptionFilters();
            if (filters) subscribeToNotification(filters);
        } else {
            $('#login-or-enter-email-form').modal('show');
            setCookie('subscribe', 'true', min=10);
        }
    });

    $('#email_for_subs').submit(function (event) {
        event.preventDefault();
        var filters = getSubscriptionFilters();
        if (filters) {
            subscribeToNotification(filters,
                                    $('#email_for_subs :input[name=login]').val());
        }
        $('#login-or-enter-email-form').modal('hide');
    });

    $("#form-login-modal, #form-signup-modal").submit(function (e) {
        var input = $('<input />').attr('type', 'hidden')
            .attr('name', "next")
            .attr('value', location.pathname + location.search);
        input.appendTo(this);
    });
    $(".socialaccount_provider").attr('next', get_next_path());
    $(".socialaccount_provider").attr(
        'href', $(".socialaccount_provider").attr('href') + "&next=" + get_next_path()
    );
    $(".socialaccount_provider").click(function () {
        $(".socialaccount_provider").attr(
            'href', "/accounts/facebook/login/?process=login&next=" + get_next_path()
        );
        $(".socialaccount_provider").attr(
            'next', "/accounts/facebook/login/?process=login&next=" + get_next_path()
        );
    });
});

function getSubscriptionFilters() {
    var filters = null;
    if (window.djangoContext && djangoContext.detailSubscriptionData) {
        filters = djangoContext.detailSubscriptionData;
    } else if (window.parameters) {
        filters =  parameters;
    }
    return filters;
}

function get_next_path() {
    return '/users/social_login_redirect/?url=' + location.pathname + location.search.replace('?', '|').replace(/&/g, '^');
}

function setCookie(key, value, days, min) {
    var expires = new Date();
    if (min) {
        expires.setTime(expires.getTime() + min * 1000)
    } else {
        days = days || 60;
        expires.setTime(expires.getTime() + days * 24 * 60 * 60 * 1000);
    }
    document.cookie = key + '=' + value + '; path=/' + '; expires=' + expires.toUTCString();
}

function subscribeToNotification(parameters, email) {
    if (getCookie('subscribe') == 'true') {
        setCookie('subscribe', 'false', min=5);
    }
    var data = {filters: JSON.stringify(parameters)};
    if (email) data.email = email;
    $.ajax({
        method: 'post',
        url: djangoContext.urls.subscribe,
        data: data,
        dataType: 'josn',
    }).done(function() {
        $('#subscribed-mess').modal('show');
    }).fail(function() {
        alert('Error');
    });
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    crossDomain: false,
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type)) {
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }

});

function preventNonNumericInput(event) {
    if (!(event.charCode > 47 && event.charCode < 58 || event.charCode === 0)) {
        event.preventDefault();
    }
}

function getLocation(callback) {
    var location = getCookie('location');
    if (!location) {
        getLocationWithAPI(callback)
    } else {
        location = JSON.parse(decodeURI(location));
        callback(location);
    }
}

function getLocationWithAPI(callback) {
    if (window.google == undefined) {
        loadGoogleMapsAPI(getLocationWithAPI, callback);
    } else if (navigator.geolocation) {
        var mytimeout = setTimeout(onError, 5000);
        navigator.geolocation.getCurrentPosition(
            onSuccess, onError
        );
        function onSuccess(pos) {
            clearTimeout(mytimeout);
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                location: {lat: pos.coords.latitude, lng: pos.coords.longitude}
            }, function (resp) {
                setLocationCookie(resp[0], callback);
            });
        }
        function onError(err) {
            clearTimeout(mytimeout);
            console.log(err)
            if (err !== undefined) alert('Unable to locate. ' + err.message);
            if (callback) callback(null)
        }
    }
     else {
        if (callback) callback(null);
        alert('You browser doesnt support geolocation.');
    }
}

function setLocationCookie(place, callback) {
    var area_types = ['street_number', 'route', 'locality', 'administrative_area_level_1', 'country'];
    var location = {};
    location.coords = place.geometry.location.toJSON();
    location.place_id = place.place_id;
    place.address_components.forEach(function (item) {
        var item_type = item.types[0];
        if (area_types.indexOf(item_type) != -1) {
            location[item_type] = item.long_name;
            if (item_type == 'country') {
                location['country_code'] = item.short_name;
            }
        }
    });
    location.formatted_address = place.formatted_address;
    setCookie('location', encodeURI(JSON.stringify(location)));
    if (callback) callback(location);
}

function loadGoogleMapsAPI(callback, callback_arg) {
    loadGoogleMapsCallback.callback = callback;
    loadGoogleMapsCallback.callback_arg = callback_arg;
    var sc = document.createElement('script');
    sc.setAttribute('type', 'text/javascript');
    sc.setAttribute('src', djangoContext.urls.google_maps);
    document.getElementsByTagName('head')[0].appendChild(sc);
}

function loadGoogleMapsCallback() {
    var callback = loadGoogleMapsCallback.callback;
    var callback_arg = loadGoogleMapsCallback.callback_arg;
    loadGoogleMapsCallback.callback = undefined;
    loadGoogleMapsCallback.callback_arg = undefined;
    if (callback) callback(callback_arg);
}


