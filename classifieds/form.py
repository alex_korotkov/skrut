# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms

from realty.form import max_images
from .models import Classified, ClassifiedImage
from realty.models import City
from django.forms import inlineformset_factory, EmailInput, RadioSelect
from django.forms import ModelForm, TextInput, Select, Textarea, NumberInput, FileInput


class ClassifiedForm(forms.ModelForm):
    city = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control  c-square c-theme',
                'title': 'City',
                'required': True,
            }
        )
    )

    class Meta:
        model = Classified
        exclude = ('url', 'city', 'creation_date', 'is_active', 'update_date')
        widgets = {
            'seller_type': RadioSelect(
                attrs={'class': 'c-radio', 'title': 'Seller type'}),
            'country': Select(
                attrs={
                    'required': True,
                    'class': 'form-control  c-square c-theme',
                    'title': 'Country'
                }),
            'price': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Price'}),
            'category': Select(
                attrs={
                    'class': 'form-control  c-square c-theme',
                    'title': 'category',
                    'required': True
                }),
            'sub_category': Select(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'sub category'}),
            'sub_sub_category': Select(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'sub sub category'}),
            'title': TextInput(
                attrs={
                    'class': 'form-control  c-square c-theme',
                    'title': 'title',
                    'required': True
                }),
            'options': Textarea(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'options'}),
            'description': Textarea(
                attrs={
                    'class': 'form-control  c-square c-theme',
                    'title': 'Description',
                    'required': True
                }),
            'phones': TextInput(
                attrs={
                    'class': 'form-control  c-square c-theme',
                    'title': 'Phones',
                    'required': True
                }),
            'email': EmailInput(
                attrs={
                    'class': 'form-control  c-square c-theme',
                    'title': 'Phones',
                    'required': True
                }),
        }


ClassifiedFormset = inlineformset_factory(
    Classified,
    ClassifiedImage,
    fields=['image'],
    extra=max_images,
    widgets={'image': FileInput(attrs={
        'class': 'fav form-control  c-square c-theme'
    })}
)
