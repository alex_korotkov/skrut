# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from django.forms import TextInput, Select, Textarea, NumberInput, FileInput, RadioSelect
from .models import Car, CarImage, Vehicle, Moto, MotoImage
from django.forms.models import inlineformset_factory
from realty.form import max_images


class VehiclesForm(forms.ModelForm):
    color = forms.CharField(required=False,
                            widget=forms.TextInput(
                                attrs={'class': 'form-control  c-square c-theme', 'title': 'Color'}))
    brand = forms.CharField(required=True,
                            widget=forms.TextInput(
                                attrs={
                                    'class': 'form-control  c-square c-theme',
                                    'required': True,
                                    'title': 'Brand'
                                }))
    model = forms.CharField(required=True,
                            widget=forms.TextInput(
                                attrs={
                                    'class': 'form-control  c-square c-theme',
                                    'required': True,
                                    'title': 'Model'
                                }))
    city = forms.CharField(required=True,
                           widget=forms.TextInput(
                               attrs={
                                   'class': 'form-control  c-square c-theme',
                                   'required': True,
                                   'title': 'City'
                               }))

    class Meta:
        model = Vehicle
        exclude = ['seller', 'city', 'url', 'price_history', 'creation_date', 'update_date',
                   'latitude', 'update_date', 'longitude', 'weight', 'brand', 'model', 'color',
                   'color_inside', 'vehicle_type', 'is_active']
        widgets = {
            'seller_type': RadioSelect(
                attrs={'class': 'c-radio', 'title': 'Seller type'}),
            'country': Select(
                attrs={
                    'class': 'form-control  c-square c-theme',
                    'required': True,
                    'title': 'Country',
                }),
            'model_type': TextInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Model Type'}),
            'model_month': Select(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Model Month',
                       'placeholder': 'Model Month'}),
            'model_year': Select(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Model Year'}),
            'km': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Kilo Meters'}),
            'price': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Price'}),
            'energy': TextInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Energy'}),
            'gear_box': TextInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Gear box'}),
            'cylinder': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Cylinder'}),
            'power': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Power'}),
            'places': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Places'}),
            'options': Textarea(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Options'}),
            'description': Textarea(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Description'}),
            'phones': TextInput(
                attrs={
                    'class': 'form-control  c-square c-theme',
                    'required': True,
                    'title': 'Phones'
                }),
        }


class CarForm(VehiclesForm):
    color_inside = forms.CharField(required=False,
                                   widget=forms.TextInput(
                                       attrs={'class': 'form-control  c-square c-theme', 'title': 'Color inside'}))

    class Meta(VehiclesForm.Meta):
        model = Car
        widgets = VehiclesForm.Meta.widgets.copy()
        widgets.update({
            'doors': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Doors'}),
            'finition': TextInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Finition'}),
        })


class MotoForm(VehiclesForm):
    class Meta(VehiclesForm.Meta):
        model = Moto
        exclude = tuple(list(VehiclesForm.Meta.exclude) + ['doors', 'finition'])
        widgets = VehiclesForm.Meta.widgets.copy()


CarFormSet = inlineformset_factory(
    Car,
    CarImage,
    fields=['image'],
    extra=max_images,
    widgets={'image': FileInput(attrs={
        'class': 'fav form-control  c-square c-theme'
    })}
)

MotoFormSet = inlineformset_factory(
    Moto,
    MotoImage,
    fields=['image'],
    extra=max_images,
    widgets={'image': FileInput(attrs={
        'class': 'fav form-control  c-square c-theme'
    })}
)
