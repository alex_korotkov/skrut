# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0021_auto_20160720_0954'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='color',
            name='base_color',
        ),
    ]
