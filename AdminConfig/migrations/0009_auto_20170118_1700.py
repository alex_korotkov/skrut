# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('AdminConfig', '0008_auto_20170118_1658'),
    ]

    operations = [
        migrations.AlterField(
            model_name='template',
            name='template',
            field=ckeditor.fields.RichTextField(null=True, blank=True),
        ),
    ]
