from __future__ import with_statement
from fabric.api import *
from fabric.contrib.console import confirm
from luxdjango.settings.production import DATABASE_PASSWORD, DATABASE_NAME


def h():
    print('hello')


def db_create():
    with settings(warn_only=True):
        if local('sudo -i -u postgres psql -c "CREATE DATABASE {};"'.format(DATABASE_NAME)).failed:
            print('Data base already exist')

        if local(
                'sudo -i -u postgres psql -c "CREATE USER skruut WITH password \'{p}\';'
                'GRANT ALL PRIVILEGES ON DATABASE {n} TO {n};"'.format(n=DATABASE_NAME, p=DATABASE_PASSWORD)).failed:
            print ('Role {} already exist'.format(DATABASE_NAME))

        answer = prompt('Type "y" to load dump otherwise it will apply migrations and fixtures')
        if answer == 'y':
            db_infile()
        else:
            local('./manage.py migrate ')
            fixtures()

def db_infile():
    local('sudo -u postgres psql %s < %s' % (DATABASE_NAME, 'db.sql'))


def db_remove():
    with settings(warn_only=True):
        result = local('sudo -i -u postgres psql -c "DROP DATABASE skruut;"', capture=True)
    if result.failed and not confirm("Tests failed. Continue anyway to drop user?"):
        abort("Aborting at user request.")
    local('sudo -i -u postgres psql -c "DROP USER skruut;"')


def fixtures():
    local("./manage.py loaddata vehicles/migrations/fixtures/countries_with_translation.json")
    local("./manage.py loaddata loaddata vehicles/migrations/fixtures/colors.json")
    local("./manage.py loaddata realty/migrations/fixtures/property_types.json")


def deploy():
    local("git reset HEAD --hard")
    local("git pull")
    local("./manage.py collectstatic")
    local("./manage.py migrate")
    local('sudo supervisorctl restart skruut')
    print("Production server has Been Successfully Deployed")
