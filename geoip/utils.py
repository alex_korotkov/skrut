from geoip2 import database as geodb
from django.conf import settings
from django.utils.translation import get_language

def get_location_by_ip(request):
    ip = request.META['REMOTE_ADDR']
    geo_reader = geodb.Reader(settings.GEOIP_PATH, locales=[get_language()])
    try:
        location = geo_reader.city(ip)
    except:
        location = None
    return location
