# -*- coding: utf-8 -*-
from modeltranslation.translator import register, TranslationOptions

from classifieds.models import Category, SubCategory, SubSubCategory


@register(Category)
class CategoryTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(SubCategory)
class SubCategoryTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(SubSubCategory)
class SubSubCategoryTranslationOptions(TranslationOptions):
    fields = ('name',)
