# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('AdminConfig', '0004_facebookpageaccesstoken'),
        ('realty', '0028_realty_seller_type'),
    ]

    operations = [
        migrations.CreateModel(
            name='RealtyFacebookPost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('post_id', models.CharField(max_length=50, verbose_name='post id')),
                ('access_token', models.ForeignKey(to='AdminConfig.FacebookPageAccessToken')),
                ('realty', models.ForeignKey(related_name='facebookpost_set', to='realty.Realty')),
            ],
        ),
    ]
