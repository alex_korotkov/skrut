# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0029_realtyfacebookpost'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realty',
            name='year_built',
            field=models.PositiveIntegerField(null=True, verbose_name='Year built', blank=True),
        ),
    ]
