#!/bin/bash

# to generate commands first fill ENV variable, for example "/venv/luxdjango/bin/python"
# run command ./thisFile.sh > outFile
# add commands to cron from outFile

ENV=""
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" # current script dir

echo "SHELL=/bin/bash"
echo "1 1 * * * $ENV $DIR/manage.py scrape_vehicles"
echo "1 2 * * * $ENV $DIR/manage.py scrape_realty"
echo "1 3 * * * $ENV $DIR/manage.py scrape_jobs"
echo "1 4 * * * $ENV $DIR/manage.py scrape_luxbazar"
