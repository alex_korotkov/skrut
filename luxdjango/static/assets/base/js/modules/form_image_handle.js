$(document).ready(function () {
    var imgPreviewStub = '<div class="col-md-3 col-sm-3 col-xs-6 preview-block">'
                            + '<div style="position:relative; margin:auto; width:107px">'
                                + '<img class="image-stub" src="/static/assets/base/img/camera.svg">'
                                + '<input type="file" name="image" accept="image/*">'
                            + '</div>'
                        + '</div>';
    var max_img_num = 12;
    var gallery = $('.c-product-gallery-thumbnail');
    var advert_form = $('#advert-form');

    gallery.on('click', '.image-stub', function(e) {
        $(e.target).closest('div').find('input[type="file"]').click();
    });

    gallery.on('click', '.img-remove', function(e) {
        $(e.target).closest('.preview-block').remove();
        addImgStub();
    });

    gallery.on('change', 'input[name="image"]', function(e) {
        if (e.target.files[0].type.startsWith('image')) {
            var input = e.target;
            var previewBlock = $(e.target).closest('div');
            var img = previewBlock.find('.image-stub');
            img.addClass('image-preview').removeClass('image-stub');
            addImgStub();
            if (FileReader) {
                var reader = new FileReader();
                reader.onload = function(e) { img.attr('src', e.target.result); };
                reader.readAsDataURL(input.files[0]);
            } else {
                img.attr('src', '');
                img.closest('div').append(
                    '<p class="instead-preview">' + input.files[0].name + '</p>'
                );
            }
            if (FormData) {
                uploadImg(input);
            } else {
                previewBlock.append(createTrash());
            }
        } else {
            $(e.target).closest('.preview-block').append(
                '<input type="file" name="image" accept="image/*">'
            );
            $(e.target).remove();
            alert('Error, this is not an image!');
        }
    });

    addImgStub();

    advert_form.on('submit', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#ad-saving-window').modal('show');
        advert_form.off('submit');
        submitAdvertForm();
    });

    function submitAdvertForm() {
        if ($('.c-product-gallery-thumbnail .upload-progress').length > 0) {
            setTimeout(submitAdvertForm, 500);
        } else {
            advert_form.submit();
        }
    }

    function uploadImg(eInput) {
        var url = '/image-upload/';
        var xhr = new XMLHttpRequest();
        var fd = new FormData();
        var progressBar = createProgressBar();
        var previewBlock = $(eInput).closest('div');
        previewBlock.append(progressBar);

        xhr.open('POST', url, true);
        xhr.setRequestHeader('X-CSRFToken', getCookie('csrftoken'));
        xhr.upload.addEventListener('progress', function(e) {
            if (e.lengthComputable) {
                var percentage = Math.round(e.loaded * 100 / e.total);
                progressBar.css('width', percentage + '%');
            }
        }, false);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
                var response = JSON.parse(xhr.responseText)
                progressBar.remove();
                previewBlock.find('input[name="image"]').remove();
                previewBlock.append(
                    '<input type="text" value="'+ response.name
                    + '" class="image-uploaded" name="image_tmp">'
                );
            }
            previewBlock.append(createTrash());
        }

        fd.append('image', eInput.files[0])
        xhr.send(fd);
    }

    function createProgressBar() {
        var bar = $(document.createElement('div'));
        bar.addClass('upload-progress');
        return bar;
    }

    function createTrash() {
        return '<span class="img-remove glyphicon glyphicon-trash"></span>';
    }

    function addImgStub() {
        var stub_num = gallery.find('.image-stub').length;
        var img_num = gallery.find('.image-preview').length;
        var total = img_num + stub_num;
        var tail = total % 4;
        if (stub_num <= tail && total < max_img_num) {
            for (i = 0; i < 4 - tail; i++) {
                gallery.append(imgPreviewStub);
            }
        } else if (stub_num > tail && tail > 0) {
            gallery.find('.image-stub').slice(-tail).closest('.preview-block').remove();
        }
    }
});