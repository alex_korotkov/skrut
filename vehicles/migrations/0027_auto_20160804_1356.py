# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0026_auto_20160803_1412'),
    ]

    operations = [
        migrations.AlterField(
            model_name='car',
            name='cylinder',
            field=models.CharField(max_length=50, null=True, verbose_name='Cylinder volume'),
        ),
        migrations.AlterField(
            model_name='car',
            name='energy',
            field=models.CharField(max_length=50, null=True, verbose_name='Fuel'),
        ),
        migrations.AlterField(
            model_name='car',
            name='km',
            field=models.PositiveIntegerField(null=True, verbose_name='Mileage'),
        ),
        migrations.AlterField(
            model_name='car',
            name='model_type',
            field=models.CharField(max_length=50, null=True, verbose_name='Model type'),
        ),
        migrations.AlterField(
            model_name='car',
            name='url',
            field=models.URLField(null=True, verbose_name='URL', blank=True),
        ),
    ]
