# -*- coding: utf-8 -*-

# Define here the models for your scraped items
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class CarItem(scrapy.Item):
    brand = scrapy.Field()
    color = scrapy.Field()
    color_inside = scrapy.Field()
    country = scrapy.Field()
    cylinder = scrapy.Field()
    doors = scrapy.Field()
    energy = scrapy.Field()
    gear_box = scrapy.Field()
    km = scrapy.Field()
    latitude = scrapy.Field()
    longitude = scrapy.Field()
    marque = scrapy.Field()
    model = scrapy.Field()
    model_month = scrapy.Field()
    model_type = scrapy.Field()
    model_year = scrapy.Field()
    places = scrapy.Field()
    power = scrapy.Field()
    price = scrapy.Field()
    seller = scrapy.Field()
    url = scrapy.Field()
    options = scrapy.Field()
    description = scrapy.Field()
    phones = scrapy.Field()
    city = scrapy.Field()

    image_urls = scrapy.Field()
    images = scrapy.Field()

    id = scrapy.Field()
    new_price = scrapy.Field()
    model_obj = scrapy.Field()

    model_class = scrapy.Field()
    update = scrapy.Field()