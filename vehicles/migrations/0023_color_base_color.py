# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0022_remove_color_base_color'),
    ]

    operations = [
        migrations.AddField(
            model_name='color',
            name='base_color',
            field=models.ForeignKey(related_name='uscolor', blank=True, to='vehicles.BaseColor', null=True),
        ),
    ]
