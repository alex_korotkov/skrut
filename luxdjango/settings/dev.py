# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals


from .common import *

SITE_ID = 2

DOMAIN = 'http://127.0.0.1:8000'

try:
    from .local_settings import *
except ImportError:
    pass

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DATABASE_NAME,
        'USER': DATABASE_NAME,
        'PASSWORD': DATABASE_PASSWORD,
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

try:
    from .local_settings import *
except ImportError:
    pass
