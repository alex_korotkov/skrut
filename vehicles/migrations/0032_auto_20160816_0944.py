# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0031_auto_20160815_1441'),
    ]

    operations = [
        migrations.RunSQL(
            "ALTER TABLE vehicles_car ALTER COLUMN cylinder TYPE integer USING cylinder::integer;",
            state_operations = [
                migrations.AlterField(
                    model_name='car',
                    name='cylinder',
                    field=models.PositiveIntegerField(null=True, verbose_name='Cylinder volume', blank=True),
                ),
            ]
        )
    ]
