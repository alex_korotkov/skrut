# -*- coding: utf-8 -*-
import datetime
from django.utils.translation import ugettext as _
from django.contrib.sites.models import Site
from django.utils import timezone
from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import get_template

from classifieds.models import Classified
from jobs.models import Job
from luxdjango.users.models import Token, User
from luxdjango.users.utils2 import get_template_text
from realty.models import Realty
from vehicles.models import Car, Moto
from django.template import Template as DjangoTemplate
from django.template import Context

MODELS = {'cars': Car, 'realty': Realty, 'motorbikes': Moto, 'classifieds': Classified, 'jobs': Job}


def send_re_validation():
    delta = timezone.now() - datetime.timedelta(days=30)
    site = Site.objects.get_current()
    users = User.objects.filter(is_active=True).values_list('id', 'email')
    for user_id, user_email in users:
        result = {}
        for name, model in MODELS.iteritems():
            objects = model.objects.filter(author_id=user_id, update_date__lt=delta)
            if objects:
                result[name] = objects
        if result:
            email_content = DjangoTemplate(get_template_text('re_validation')).render(
                Context({'site': site, 'result': result}))
            email = EmailMessage(_('you posts'), email_content, settings.EMAIL_HOST_USER,
                                 to=[user_email])
            email.content_subtype = 'html'
            email.send()
