import os, scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from django.core.management.base import BaseCommand, CommandError

from scraper.jobs_scraper import spiders


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        os.environ['SCRAPY_PROJECT'] = 'jobs'
        process = CrawlerProcess(get_project_settings())
        process.crawl(spiders.FrJobsSpider)
        process.crawl(spiders.MonsterJobsSpider)
        process.crawl(spiders.MoovijobJobsSpider)
        process.crawl(spiders.JobfinderJobsSpider)
        process.start()
