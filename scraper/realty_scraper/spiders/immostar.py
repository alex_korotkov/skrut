# -*- coding: utf-8 -*-
import datetime
import scrapy, re
from decimal import Decimal
from ..items import RealtyItem
from realty.models import Realty, PropertyType, PropertyTypeType

from scrapy.shell import inspect_response


# def get_urls():
#     base_url = "http://www.immostar.lu/cherche/?q=&action=&search_a=&search_b=&ch=&pg=&order=&pmin=&pmax=&country=&region=&commune=&city=&new=&agence=&projet=&col1=&col2=&col3=&col4=&cigdl=&module=&view=&pg="
#     Countries = ['GM', 'BE', 'FR', 'IT', 'lu', 'PT']
#     urls = [base_url.replace('country=', 'country=' + i) for i in Countries]
#     return urls


class ImmostarSpider(scrapy.Spider):
    country = ''
    name = 'immostar'
    allowed_domains = ['immostar.lu']
    start_urls = [
        'http://www.immostar.lu/cherche/?q=&action=&search_a=&search_b=&ch=&pg=&order=&pmin=&pmax=&country=&region=&commune=&city=&new=&agence=&projet=&col1=&col2=&col3=&col4=&cigdl=&module=&view=&pg='
    ]

    def parse(self, response):
        countries = {'GM': 'Germany',
                     'BE': 'Belgium',
                     'FR': 'France',
                     'IT': 'Italy',
                     'lu': 'Luxembourg',
                     'PT': 'Portugal'}
        adv_list = response.xpath('//div[starts-with(@class, "list-container clearfix")]/div/article')
        for adv in adv_list:
            price = adv.xpath('.//h5[@class="price"]/text()').extract_first()
            details = adv.xpath('.//div[@class="detail"]/p/text()').extract_first()
            if (u'disponible' not in details) and (u'\xe0' not in price):
                link = adv.xpath(u'.//a[@title="Résultats"]/@href').extract_first()
                url = response.urljoin(link + '/')
                realty = Realty.objects.filter(url=url).only('price').first()
                if not realty:
                    country = re.search(r'(?<=country=).{2}', response.url).group()
                    country = countries.get(country)
                    self.country = country if country else 'Luxembourg'
                    yield scrapy.Request(url, callback=self.parse_detail)
                else:
                    self.logger.info('/// already exists ///')
                    price = re.sub(r'\D', '', price)
                    price = int(price) if price else None
                    item = RealtyItem()
                    item['model_obj'] = realty
                    if price != realty.price:
                        item['new_price'] = price
                    item['update'] = True
                    yield item
        current_page = re.search(r'(?<=pg=)\d+', response.url)
        if current_page:
            current_page = int(current_page.group())
            page_num = current_page + 1
            next_page = re.sub(r'pg=\d+', ('pg=' + str(page_num)), response.url)
            last_page = response.xpath('//a[@class="real-btn"]').extract()[-1]
            last_page = re.sub(r'\D', '', last_page)
            if int(last_page) == current_page:
                next_page = None
        else:
            page_num = 1
            next_page = re.sub(r'pg=', ('pg=' + str(page_num)), response.url)
        if next_page:
            self.logger.info('///////////////////// go to next page /////////////////////')
            yield scrapy.Request(response.urljoin(next_page), callback=self.parse)

    def parse_detail(self, response):
        new_realty_years = 5
        self.logger.info('/// parse detail ///')
        item = RealtyItem()
        item['url'] = response.url
        item['country'] = self.country
        title = response.xpath('//title/text()').extract_first().split(' ')
        item['city'] = re.sub(r'\W', '', title[-1]) if title else None
        property_type = title[0]
        if 'Garage' in property_type:
            property_type = 'Garage'
        elif 'Jardin' in property_type:
            property_type = 'Terrain'
        elif 'Location de vacances' in property_type:
            property_type = 'Chambre'
        item['property_type'] = property_type.strip()

        content_clearfix = response.xpath('//div[@class="content clearfix"]')

        description = content_clearfix.xpath('.//p/text()').extract()
        if description:
            description = [x.strip() for x in description if x.strip()]
            if description:
                item['description'] = '<br>'.join(description)
        realty_classes = content_clearfix.xpath('.//div/ul/p/img/@src').extract()
        if realty_classes:
            energy_class = re.search(r'.(?=\.png)', realty_classes[0]).group()
            if energy_class:
                item['energy_class'] = energy_class
            termal_protection_class = re.search(r'.(?=\.png)', realty_classes[1]).group()
            if termal_protection_class:
                item['termal_protection_class'] = termal_protection_class

        phones = response.xpath('//li[@class="office"]/text()').extract_first()
        if phones:
            phones = phones.replace('Bureau :', '').split()
            if phones:
                item['phones'] = phones
        seller = response.xpath('//div[@class="left-box"]/h3/text()').extract_first()
        if seller:
            item['seller'] = seller
        block_price = response.xpath('//h5[@class ="price"]/span/text()').extract()
        if block_price:
            item['deal'] = 1 if u'vendre' in block_price[0] else 2
            price = re.sub(r'\D', '', block_price[1])
            if price:
                item['price'] = price
        dict_details = {}
        list_details = response.xpath('//ul[@class ="additional-details clearfix"]/li')
        for detail in list_details:
            dict_details[detail.xpath('.//strong/text()').extract_first()] = detail.xpath(
                './/span/text()'
            ).extract_first()

        dict_features = {'bathroom': u'Salle de bain',
                         'balcony': u'Balcon',
                         'terrace': u'Terrasse',
                         'floor': u'Etage',
                         'total_floors': u"Nombre d'étage",
                         'garage': u'Garage',
                         'area': u'Surface',
                         'land_area': u'Terrain',
                         'garden': u'Jardin'
                         }
        for key, value in dict_features.iteritems():
            matching = filter(lambda x: value in x, dict_details.keys())
            if matching:
                ss = re.search(r'[\d.]+', dict_details[matching[0]]) if dict_details[matching[0]] else None
                result = ss.group().strip().split('.')[0] if ss else 1
                item[key] = result
        dict_features = {
            'bathroom': u'Salle de bain',
            'rooms': u'Chambres'
        }
        for key, value in dict_features.iteritems():
            matching = filter(lambda x: value in x, dict_details.keys())
            if matching:
                ss = re.search(r'[\d.]+', matching[0]) if matching[0] else None
                result = ss.group().strip().split('.')[0] if ss else 1
                item[key] = result
        matching = filter(lambda x: u'Année de construction' in x, dict_details.keys())
        if matching:
            ss = re.search(r'[\d]+', dict_details[matching[0]]) if dict_details[matching[0]] else None
            result = ss.group() if ss else None
            if result and ((int(result) + new_realty_years) >= datetime.datetime.now().year):
                item['new_building'] = True
            item['year_built'] = result

        image_urls = response.xpath('//ul[@class="slides"]/li/a/@href').extract()
        if image_urls:
            item['image_urls'] = image_urls
        yield item
