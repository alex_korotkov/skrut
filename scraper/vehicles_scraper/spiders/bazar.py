# -*- coding: utf-8 -*-
import datetime, json, re, scrapy

from ..items import CarItem
from vehicles.models import Car, Moto

from scrapy.shell import inspect_response

class BazarSpider(scrapy.Spider):
    name = 'bazar'
    allowed_domains = ['bazar.lu']
    start_urls = ['http://www.bazar.lu']

    def parse(self, response):
        self.logger.info('///start parsing///')
        yield scrapy.Request(
            'http://www.bazar.lu/Scripts/sql.exe?SqlDB=bazar&Sql=Search.phs&category=2',
            callback=self.go_to_list,
            meta={'model_class': 'Car'}
        )
        yield scrapy.Request(
            'http://www.bazar.lu/Scripts/sql.exe?SqlDB=bazar&Sql=Search.phs&category=7',
            callback=self.go_to_list,
            meta={'model_class': 'Moto'}
        )

    def go_to_list(self, response):
        yield scrapy.FormRequest.from_response(response,
                                               formxpath='//form[@id="search_form"]',
                                               formdata={'type_annonce_offre': 'on'},
                                               callback=self.parse_list,
                                               meta={'model_class': response.meta['model_class']})

    def parse_list(self, response):
        self.logger.info('/////////', response.meta)
        stop = False
        adv_list = response.css('div#search_result_content>div.item')
        ModelClass = Moto if response.meta['model_class'] == 'Moto' else Car
        two_weeks_ago = datetime.datetime.now() - datetime.timedelta(days=15)

        for adv in adv_list:
            ad_date = adv.css('div.search_result_entry_info_day').xpath('./text()').extract_first()
            ad_date = ad_date.strip() if ad_date else ''
            if ad_date and re.match(r'^\d{2}/\d{2}/\d{4}', ad_date):
                ad_date = datetime.datetime.strptime(ad_date, '%d/%m/%Y %H:%M')
                if ad_date < two_weeks_ago:
                    stop = True
                    self.logger.info('/////old adverts goes further, stop parsing////')
                    break
            if not stop:
                link = adv.xpath('./a/@href').extract_first()
                url = response.urljoin(link)
                vehicle = ModelClass.objects.filter(url=url).only('price').first()
                if not vehicle:
                    yield scrapy.Request(url,
                                         callback=self.parse_detail,
                                         meta={'model_class': response.meta['model_class']})
                else:
                    self.logger.info('///already exists///')
                    price = adv.xpath(
                        './/div[@class="search_result_entry_info_price"]/text()[2]'
                    ).extract_first().strip()
                    if price:
                        price = int(price)
                        item = CarItem()
                        item['model_obj'] = vehicle
                        if price != vehicle.price:
                            item['new_price'] = price
                        item['update'] = True
                        yield item

        if not stop:
            current_page_btn = response.xpath('//div[@class="page_link selectedPage"]')
            next_page_btn = current_page_btn.xpath('./following-sibling::div[1]')
            if next_page_btn.xpath('./@class').extract_first().strip() == 'page_link':
                next_page = next_page_btn.xpath('./div/text()').extract_first().strip()
                self.logger.info('//////////////////////go to next page////////////////////', next_page)
                yield scrapy.FormRequest.from_response(
                    response,
                    formxpath='//form[@id="search_form"]',
                    formdata={'page': str(int(next_page) + 10)},
                    callback=self.parse_list,
                    meta={'model_class': response.meta['model_class']}
                )

    def parse_detail(self, response):
        self.logger.info('///// parse detail /////')
        item = CarItem()
        item['url'] = response.url
        item['model_class'] = response.meta['model_class']
        item['seller'] = {}

        price = response.xpath('//span[@id="annonce_price_highlight"]/text()').extract_first()
        price = price.strip() if price else ''
        country = response.xpath('//div[@id="annonce_address_country"]/text()').extract_first()
        country = country.strip() if country else ''
        address = response.xpath('//div[@id="annonce_address_street"]/text()').extract_first()
        address = address.strip() if address else ''
        city = response.xpath('//div[@id="annonce_address_city"]/text()').extract_first()
        city = city.strip() if city else ''
        name = response.xpath('//div[@id="annonce_user_name"]/text()').extract_first()
        name = name.strip() if name else ''
        if price:
            item['price'] = price
        if country:
            item['country'] = country.strip()
        if address and city:
            item['seller']['location'] = '%s, %s' % (address, city)
        elif address or city:
            item['seller']['location'] = address or city
        if name:
            item['seller']['name'] = name

        detail_block = response.xpath('//div[@class="annonce_attributs"]')
        selector = './/div[@id="annonce_attribute_%s"]//span/text()'
        details_map = (
                #    car moto
            ('brand', 71, 86),
            ('model', 85, 87),
            ('model_type', 72, 72),
            ('gear_box', 73, 73),
            ('power', 74, 74),
            ('cylinder', 75, 75),
            ('km', 77, 77),
            ('energy', 79, 79),
            ('color', 80, 80),
            ('color_inside', 81),
            ('places', 116),
        )

        i = 1 if item['model_class'] == 'Car' else 2
        for d in details_map:
            if i < len(d):
                value = detail_block.xpath(selector % d[i]).extract_first()
                value = value.strip() if value else ''
                if value and value != '0':
                    item[d[0]] = value
        if not item.get('brand'):
            return

        doors = detail_block.xpath(selector % 76).extract_first()
        year_month = detail_block.xpath(selector % 83).extract_first()
        if doors:
            item['doors'] = doors.strip()[-1:]
        if year_month:
            year_month = year_month.strip()
            if re.match(r'^\d{4}.\d{2}.*$', year_month):
                item['model_year'] = year_month[:4]
                item['model_month'] = year_month[5:7]
            elif re.match(r'^.*\d{2}.\d{4}$', year_month):
                item['model_year'] = year_month[-4:]
                item['model_month'] = year_month[-7:-5]

        city = response.xpath('//div[@id="annonce_address_city"]/text()').extract_first()
        if city and re.search(r'\d', city):
            city = re.search(r'(?<=\d)\D+', city).group()
        city = city.strip() if city else None
        if city:
            item['city'] = city

        options = detail_block.xpath(
            './/div[@class="annonce_attribute_list_entry"]/text()'
        ).extract()
        if options:
            item['options'] = json.dumps([x.strip() for x in options])
        description = response.xpath(
            '//div[@class="annonce_entry_description_long"]/text()'
        ).extract_first()
        if description:
            item['description'] = description.strip()

        img_block = response.css("div.annonce_entry_images")
        img_links = img_block.xpath('.//figure/a/@href').extract()
        if img_links:
            item['image_urls'] = [response.urljoin(x) for x in img_links]

        match = re.search(r'(?<=_annonce=)\d+', response.url)
        id = match.group() if match else None
        if id:
            yield scrapy.Request(
                'http://www.bazar.lu/Scripts/sql.exe?SqlDB=bazar&Sql=Phone.phs&item=%s' % id,
                headers={'X-Requested-With': 'XMLHttpRequest'},
                callback=self.parse_phone,
                meta={'item': item}
            )
        else:
            yield item

    def parse_phone(self, response):
        self.logger.info('////parse phone/////')
        item = response.meta['item']
        phone = response.body.strip()
        if phone and phone != 'pas disponible':
            item['phones'] = [phone]
            item['seller']['phones'] = phone
        yield item

