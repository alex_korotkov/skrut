# -*- coding: utf-8 -*-
import scrapy, re, json

from classifieds.models import Classified
from realty.models import Realty
from scraper.jobs_scraper.items import JobItem

import simplejson as json
from jobs.models import Job
from scraper.luxbazar_scraper.items import ClassifiedItem
from scraper.realty_scraper.items import RealtyItem
from scraper.vehicles_scraper.items import CarItem
from vehicles.models import Car, Moto


class LuxbazarJobsSpider(scrapy.Spider):
    name = 'luxbazar'
    allowed_domains = ['www.luxbazar.lu/']
    start_urls = [
        'http://www.luxbazar.lu/adverts_json.php',
    ]

    def parse(self, response):
        self.logger.info('///// parse list /////')
        jsonresponse = json.loads(response.body_as_unicode())
        for advert in jsonresponse['adverts']:
            if advert['category'] == 'Jobs':
                url = "http://www.luxbazar.lu/Emploi/Ref%s/" % str(advert['id'])
                if Job.objects.filter(url=url).exists():
                    self.logger.info('/// already exists ///')
                else:
                    yield self.parse_jobs_detail(advert, url)

            elif advert['category'] == 'Cars':
                url = "http://www.luxbazar.lu/Auto/Ref%s/" % str(advert['id'])
                car = Car.objects.filter(url=url).only('price').first()
                if car:
                    self.logger.info('/// already exists ///')
                    price = advert.get('price_euro')
                    item = CarItem()
                    item['model_obj'] = car
                    if price and int(price) != car.price:
                        item['new_price'] = price
                    item['update'] = True
                    yield item
                else:
                    yield self.parse_vehicle_detail(advert, url, model_class='Car')

            elif advert['category'] == 'Bikes':
                url = "http://www.luxbazar.lu/Moto/Ref%s/" % str(advert['id'])
                moto = Moto.objects.filter(url=url).only('price').first()
                if moto:
                    self.logger.info('/// already exists ///')
                    price = advert.get('price_euro')
                    item = CarItem()
                    item['model_obj'] = moto
                    if price and int(price) != moto.price:
                        item['new_price'] = price
                    item['update'] = True
                    yield item
                else:
                    yield self.parse_vehicle_detail(advert, url, model_class='Moto')

            elif advert['category'] == 'Real Estate':
                url = "http://www.luxbazar.lu/Immobilier/Ref%s/" % str(advert['id'])
                realty = Realty.objects.filter(url=url).only('price').first()
                if realty:
                    self.logger.info('/// already exists ///')
                    price = advert.get('price_euro')
                    item = RealtyItem()
                    item['model_obj'] = realty
                    if price and int(price) != realty.price:
                        item['new_price'] = price
                    item['update'] = True
                    yield item
                else:
                    yield self.parse_realty_detail(advert, url)

            elif advert['category'] == 'Good deal' or advert['category'] == 'Pets'\
                                                   or advert['category'] == 'Sports and Leisure':
                url = "http://www.luxbazar.lu/Bonnes-Affaires/Ref%s/" % str(advert['id'])
                classified = Classified.objects.filter(url=url).only('price').first()
                if classified:
                    self.logger.info('/// already exists ///')
                    price = advert.get('price_euro')
                    item = ClassifiedItem()
                    item['model_obj'] = classified
                    if price and int(price) != classified.price:
                        item['new_price'] = price
                    item['update'] = True
                    yield item
                else:
                    yield self.parse_classified_detail(advert, url)

    def parse_jobs_detail(self, advert, url):
        self.logger.info('///// parse detail /////')
        item = JobItem()
        employer = {}
        item['url'] = url
        employer_data = advert['contact']
        email = employer_data['email']
        if email:
            employer['email'] = email
        phones = [employer_data['phone']]
        if phones:
            employer['phones'] = phones
        name = employer_data['name']
        if name:
            employer['name'] = name
        elif email:
            employer['name'] = email
        item['title'] = advert['title']
        details = advert.get('details')
        if details:
            work_hours = details.get('hours')
            if work_hours:
                item['work_hours'] = ['Temps plein'] if work_hours == u'Full time'\
                                                     else ['Temps partiel']
            contract = details.get('type')
            if contract:
                item['contract'] = [u'CDI'] if contract == u'Permanent' else [u'CDD']
        city = advert.get('city')
        if city:
            if city.lower() == 'luxemburg':
                item['city'] = 'Luxembourg'
            else:
                item['city'] = city
        country = advert.get('country')
        if country:
            item['country'] = country
            employer['country'] = country
        else:
            item['default_country_id'] = 35
            employer['country_id'] = 35
        if advert.get('description'):
            item['description'] = advert['description']
        item['category'] = [advert.get('category_lvl2')]
        if advert.get('category_lvl3'):
            item['category'].append(advert['category_lvl3'])
        item['employer'] = employer
        return item

    def parse_vehicle_detail(self, advert, url, model_class):
        item = CarItem()
        item['seller'] = {}
        item['url'] = url
        item['model_class'] = model_class
        fields_dict = {
            'brand': 'category_lvl2',
            'model': 'category_lvl3',
            'model_type': 'title',
            'description': 'description',
            'price': 'price_euro',
            'city': 'city',
            'country': 'country'
        }
        for key, value in fields_dict.iteritems():
            value = advert.get(value)
            if value:
                item[key] = value
        seller_type = advert['contact'].get('type')
        if seller_type:
            item['seller']['seller_type'] = 2 if seller_type == 'private' else 1
        phones = advert['contact'].get('phone')
        if phones:
            item['seller']['phones'] = phones
            item['phones'] = phones.split(',')

        if advert['contact'].get('company'):
            item['seller']['name'] = advert['contact']['company']
        elif advert['contact'].get('name'):
            item['seller']['name'] = advert['contact']['name']
        if advert['contact'].get('email'):
            item['seller']['email'] = advert['contact']['email']
            if not item['seller'].get('name'):
                item['seller']['name'] = advert['contact']['email']

        if advert.get('details'):
            details_fields = {
                'model_year': 'year',
                'km': 'mileage',
                'gear_box': 'gearbox',
                'energy': 'energy',
                'color': 'color',
                'color_inside': 'inside_color',
                'cylinder': 'cc',
            }
            for key, value in details_fields.iteritems():
                value = advert['details'].get(value)
                if value:
                    item[key] = value

        photos = advert.get('photo_ids')
        if photos:
            item['image_urls'] = [
                'http://www.luxbazar.lu/img.php?id=%s' % x for x in photos.split(',')
            ]

        return item

    def parse_realty_detail(self, advert, url):
        item = RealtyItem()
        item['url'] = url
        item['seller'] = {}

        realty_items = {
            'property_type': 'category_lvl3',
            'description': 'description',
            'price': 'price_euro',
            'city': 'city',
            'country': 'country'
        }
        property_type_dict = {
            'Commercial property': ['Business premises', 'Business premises'],
            'Farm': ['Land', 'Farmland'],
            'Apartment 1-2 rooms': ['Apartment', 'Flat'],
            'Lofts': ['Apartment', 'Loft'],
            'Site': ['Land', 'Land'],
            'Garage for Sale': ['Garage', 'Garage'],
            'Houses': ['House', 'House'],
            'Studio': ['Apartment', 'Flat'],
            'Apartment 3 room and more': ['Apartment', 'Flat'],
            'Offices': ['Business premises', 'Office'],
            'Garage for Rent': ['Garage', 'Garage'],
            'Immeubles': ['Business premises', 'Business premises'],
            'Rooms': ['Apartment', 'Room'],
        }
        for key, value in realty_items.iteritems():
            value = advert.get(value)
            if value:
                if key == 'property_type':
                    if value in property_type_dict.keys():
                        value = property_type_dict[value]
                        item['property_type'] = value[0]
                        item['property_type_type'] = value[1]
                    else:
                        item[key] = value.split()[0]
                else:
                    item[key] = value
        if not item.get('property_type') or not item.get('country') or item['country'] == 'Cape Verde':
            return None
        seller_type = advert['contact'].get('type')
        if seller_type:
            item['seller_type'] = 2 if seller_type == 'private' else 1
        if advert['contact'].get('company'):
            item['seller']['name'] = advert['contact']['company']
        elif advert['contact'].get('name'):
            item['seller']['name'] = advert['contact']['name']
        if advert['contact'].get('email'):
            item['seller']['email'] = advert['contact']['email']
            if not item['seller'].get('name'):
                item['seller']['name'] = advert['contact']['email']

        phones = advert['contact'].get('phone')
        if phones:
            item['phones'] = [phones]

        if advert.get('details'):
            details_items = {
                'land_area': 'site_area_ares',
                'year_built': 'year',
                'area': 'living_area_m2',
                'garden': 'garden_area_m2',
                'rooms': 'rooms',
                'floor': 'floors',
                'bathroom': 'bathrooms',
            }
            for key, value in details_items.iteritems():
                value = advert['details'].get(value)
                if value:
                    item[key] = value
            class_items = {
                'energy_class': 'energy_class',
                'termal_protection_class': 'gas_emission_class'
            }
            for key, value in class_items.iteritems():
                value = advert['details'].get(value)
                if value and len(value) == 1:
                    item[key] = value
        photos = advert.get('photo_ids')
        if photos:
            image_urls = []
            for photo in photos.split(','):
                image_urls.append('http://www.luxbazar.lu/img.php?id=' + photo)
            item['image_urls'] = image_urls
        deal = advert.get('category_lvl2')
        if deal:
            item['deal'] = 1 if 'Sale' in deal else 2
        return item

    def parse_classified_detail(self, advert, url):
        item = ClassifiedItem()
        item['url'] = url
        phones = advert['contact'].get('phone')
        if phones:
            item['phones'] = [phones]
        email = advert['contact'].get('email')
        if email:
            item['email'] = email

        car_items = {
            'title': 'title',
            'category': 'category',
            'sub_category': 'category_lvl2',
            'sub_sub_category': 'category_lvl3',
            'description': 'description',
            'price': 'price_euro',
            'city': 'city',
            'country': 'country',
            'options': 'details'
        }
        for key, value in car_items.iteritems():
            value = advert.get(value)
            if value:
                item[key] = value
        photos = advert.get('photo_ids')
        if photos:
            image_urls = []
            for photo in photos.split(','):
                image_urls.append('http://www.luxbazar.lu/img.php?id=' + photo)
            item['image_urls'] = image_urls
        options = advert.get('details')
        if options:
            item['options'] = json.dumps(options)
        return item
