from django.db import models
from ckeditor.fields import RichTextField


class HideContacts(models.Model):
    domain = models.CharField(max_length=100)
    hide_contacts = models.NullBooleanField()


class FacebookPageAccessToken(models.Model):
    name = models.CharField(max_length=200)
    facebook_access_token = models.CharField(max_length=200)
    post_on_facebook = models.NullBooleanField(default=True)


class Template(models.Model):
    name = models.CharField(max_length=100)
    template = RichTextField(blank=True, null=True)

    def __unicode__(self):
        return self.name
