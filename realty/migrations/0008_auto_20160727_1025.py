# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import realty.models


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0007_realty_garage'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realtyimage',
            name='image',
            field=models.ImageField(upload_to=realty.models.img_upload_to),
        ),
    ]
