# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from jobs.models import Job, Employer
from django.forms import TextInput, EmailInput, URLInput, Select, Textarea, CheckboxSelectMultiple, RadioSelect


class EmployerSelectForm(forms.Form):
    employer = forms.ModelChoiceField(empty_label="New employer", queryset=Employer.objects.none(), widget=forms.Select(
        attrs={'class': 'form-control  c-square c-theme', 'title': 'email'}))


class EmployerForm(forms.ModelForm):
    class Meta:
        model = Employer
        exclude = ('country',)
        widgets = {
            'name': TextInput(attrs={'required': True, 'class': 'form-control  c-square c-theme', 'title': 'name'}),
            'site': URLInput(attrs={'class': 'form-control  c-square c-theme', 'title': 'site'}),
            'email': EmailInput(attrs={'class': 'form-control  c-square c-theme', 'title': 'email'}),
            'address': TextInput(attrs={'class': 'form-control  c-square c-theme', 'title': 'address'}),
            'phones': TextInput(attrs={'class': 'form-control  c-square c-theme', 'title': 'Phones'}),
        }


class JobForm(forms.ModelForm):
    city = forms.CharField(required=True,
                           widget=forms.TextInput(
                               attrs={'class': 'form-control  c-square c-theme', 'title': 'City'}
                          ))

    class Meta:
        model = Job
        exclude = ('url', 'city', 'creation_date', 'is_active', 'update_date')
        widgets = {
            'seller_type': RadioSelect(
                attrs={'class': 'c-radio', 'title': 'Seller type'}),
            'title': TextInput(
                attrs={'class': 'form-control  c-square c-theme', 'required': True, 'title': 'title'}),
            'description': Textarea(
                attrs={'class': 'form-control  c-square c-theme', 'required': True, 'title': 'Description'}),
            'address': TextInput(
                attrs={
                    'class': 'form-control  c-square c-theme',
                    'title': 'address',
                    'id': 'id_job_address'
                }),
            'country': Select(
                attrs={'required': True, 'class': 'form-control  c-square c-theme', 'title': 'Country'}),
            'employer': Select(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'employer'}),

            'contract': CheckboxSelectMultiple(
                attrs={'class': 'c-check', 'title': 'contract'},
            ),
            'category': CheckboxSelectMultiple(
                attrs={'class': 'c-check', 'title': 'category'}
            ),
            'work_hours': CheckboxSelectMultiple(
                attrs={'class': 'c-check', 'title': 'work hours'}
            ),
            'candidate_level': CheckboxSelectMultiple(
                attrs={'class': 'c-check', 'title': 'candidate level'}
            ),

        }
