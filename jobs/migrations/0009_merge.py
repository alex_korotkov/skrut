# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0008_employer_user'),
        ('jobs', '0006_auto_20160921_1036'),
    ]

    operations = [
    ]
