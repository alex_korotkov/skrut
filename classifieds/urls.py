# -*- coding: utf-8 -*-
from django.conf.urls import url

from realty.views import get_city

from . import views

urlpatterns = [
    url(r'^good-deals/$', views.SearchView.as_view(), name='good_deals'),
    url(r'^pets/$', views.SearchView.as_view(), name='pets'),
    url(r'^sports-and-leisure/$', views.SearchView.as_view(), name='sports_and_leisure'),
    url(r'^search/$', views.SearchView.as_view(), name='search'),
    url(r'^search-ajax/', views.ElasticSearchAjax.as_view(), name='search-ajax'),
    url(r'^details/(?P<pk>\d+)/$', views.DetailClassifiedView.as_view(), name='details'),
    url(r'^add/', views.AddClassifiedView.as_view(), name="add-classified"),
    url(r'^get_city/', get_city, name='get_city'),
    url(r'^edit/(?P<pk>\d+)/$', views.EditClassifiedView.as_view(), name='edit'),
]
