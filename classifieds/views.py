# -*- coding: utf-8 -*-
from django.views.generic import ListView, TemplateView, DetailView, View
from django_ajax.mixin import AJAXMixin
from collections import OrderedDict, defaultdict
from realty.models import City
from django.core.urlresolvers import reverse
from vehicles.models import Country
from vehicles.views import CALLING_CODE
from .models import Classified, Category, SubCategory, SubSubCategory, ClassifiedImage
import copy, urllib
import simplejson as json
from .form import ClassifiedForm

from django.http import HttpResponseRedirect
from luxdjango.utils import handnle_form_images, get_tmp_image_urls, get_user_data, post_to_facebook
from django.http.response import HttpResponse
from django.shortcuts import render
from geoip.utils import get_location_by_ip
from allauth.account.models import EmailAddress
from haystack.query import SearchQuerySet
from haystack.utils.geo import Point, D

from luxdjango.users.form import UserForm
from luxdjango.users.models import User, Token
from luxdjango.users.utils2 import send_notification_after_adding_post
from realty.form import max_images




class SearchView(TemplateView):
    template_name = 'classifieds/search.html'

    def get_context_data(self):
        context = super(SearchView, self).get_context_data()
        context['list_of_country'] = Country.objects.all()
        return context



class GoodDealsView(SearchView):
    pass

class PetsView(SearchView):
    pass

class SprortsvView(SearchView):
    pass


class DetailClassifiedView(DetailView):
    model = Classified

    def get(self, context, pk):
        if self.request.is_ajax():
            data = get_user_data(Classified, pk)
            return HttpResponse(data)
        else:
            return super(DetailClassifiedView, self).get(context, pk)


class ElasticSearchAjax(AJAXMixin, TemplateView):
    FACET = OrderedDict([
        ('sub_category_index', (SubCategory, 'name')),
        ('sub_sub_category_index', (SubSubCategory, 'name')),
        ('country_index', None),
    ])

    RANGE_FILTER = [
        'price_index',
    ]

    def queryset(self):
        q = SearchQuerySet()
        if 'good-deals' in self.request.META['HTTP_REFERER']:
            q = q.filter(big_category=u'gooddeals')
        elif 'pets' in self.request.META['HTTP_REFERER']:
            q = q.filter(big_category='pets')
        elif 'sports-and-leisure' in self.request.META['HTTP_REFERER']:
            q = q.filter(big_category='sports')

        r = self.request.GET

        if r.get('q'):
            q = q.filter(text=r.get('q'))

        for p in r:
            if p in self.FACET:
                params = {p: r.get(p)}
                q = q.filter(**params)

        for p in r:
            if p in self.RANGE_FILTER:
                y = r.get(p).split(',')
                params = {p + '__range': [int(y[0].replace('.', '')), int(y[1].replace('.', ''))]}
                q = q.filter(**params)
        if r.get('category_index'):
            q = q.filter(category_index=r['category_index'])

        if r.get('city'):
            q = q.filter(city_index=r['city'])
        if r.get('countries'):
            country_ids = str(r['countries']).split(',')
            q = q.filter(country_index__in=country_ids)
            self.cities = City.objects.filter(country_id__in=country_ids)
        else:
            self.cities = City.objects.all()

        distance = r.get('distance')
        if distance and distance != '1000.00':
            distance = D(km=int(r.get('distance').replace('.00', '')))

            if 'location' in self.request.COOKIES:
                location = json.loads(urllib.unquote(self.request.COOKIES['location']))
                latitude = location['coords']['lat']
                longitude = location['coords']['lng']
            # else:
            #     location = get_location_by_ip(self.request)
            #     latitude = location.location.latitude
            #     longitude = location.location.longitude
            if location:
                user_point = Point(float(longitude), float(latitude))
                q = q.dwithin('location_index', user_point, distance)

        sort = r.get('sort')
        if sort:
            q = q.order_by(sort)
        else:
            q = q.order_by('-date_index')
        return q

    def faceted(self, search_result):
        fin_counts = OrderedDict()
        for f in self.FACET:
            search_result = search_result.facet(f)
        counts = search_result.facet_counts().get('fields', {})
        for f in self.FACET:
            if f in counts:
                if self.FACET[f]:
                    name_dict = {int(index): None for index, count in counts[f]}
                    names = self.FACET[f][0].objects.filter(
                        pk__in=name_dict.keys()
                    ).only('id', self.FACET[f][1])
                    for i in names:
                        name_dict[i.id] = getattr(i, self.FACET[f][1])
                    result = [
                        {'index': index, 'count': count, 'name': name_dict[int(index)]}
                        for index, count in counts[f]
                        ]
                    fin_counts.update({f: sorted(result, key=lambda k: k['name'])})
                else:
                    fin_counts.update(
                        {f: [{'index': index, 'count': count, 'name': index} for index, count in counts[f]]}
                    )
        return fin_counts

    def check_boxes_filters(self, facets):
        check_boxes_filters = copy.deepcopy(facets)
        if 'country_index' in check_boxes_filters:
            del check_boxes_filters["country_index"]
        return check_boxes_filters

    def get(self, request, *args, **kwargs):
        search_result = self.queryset()
        facets = self.faceted(search_result)

        if request.GET.get('show'):
            post_count = int(request.GET['show'])
        else:
            post_count = 10
        template = ''
        if request.GET.get('style') and request.GET.get('style') == 'grid':
            template = 'classifieds/grid_item_preview.html'
            post_count = 32
        else:
            template = 'classifieds/item_preview.html'
        search_list_result = lambda: render(
            request,
            template,
            {'query': True, 'object_list': search_result, 'post_count': post_count}
        )

        left_sidebar_filters = lambda: render(
            request,
            'search/left_sidebar_filters.html',
            {
                'facets': self.check_boxes_filters(facets),
                'conditions': request.GET,
                'cities': self.cities,
                'selected_city': int(request.GET.get('city', 0)),
            }
        )
        if not request.GET.get('scroll'):
            AJAX_TEMPLATE = {'inner-fragments': {
                '#search-list-result': search_list_result(),
                '#left-sidebar-filters': left_sidebar_filters(),
            }}
        else:
            AJAX_TEMPLATE = {'append-fragments': {'#search-list-result': search_list_result()}}

        return AJAX_TEMPLATE


class AddClassifiedView(TemplateView):
    template_name = 'classifieds/add_classified.html'
    classified_form = ClassifiedForm()

    def get(self, request, *args, **kwargs):
        classified_form = ClassifiedForm()
        user_form = UserForm() if not request.user.is_authenticated() else None
        return render(request, self.template_name,
                      {"form": classified_form,
                       'header': 'classified',
                       'images_tmp': get_tmp_image_urls(request.META['CSRF_COOKIE']),
                       'user_form': user_form,
                       'calling_code': json.dumps(CALLING_CODE),
                       'categories': get_options()})

    def post(self, request):
        classified_form = ClassifiedForm(data=request.POST)
        user_form = UserForm(data=request.POST) if not request.user.is_authenticated() else None
        if classified_form.is_valid() and (
                    request.user.is_authenticated() or user_form.is_valid()):
            return self.form_valid(request, classified_form, user_form)
        return render(request, self.template_name, {
            "form": classified_form,
            'header': 'classified',
            'images_tmp': get_tmp_image_urls(request.META['CSRF_COOKIE']),
            'user_form': user_form,
            'calling_code': json.dumps(CALLING_CODE),
            'categories': get_options()})

    def form_valid(self, request, classified_form, user_form):
        city_name = classified_form.cleaned_data.get('city')
        if request.user.is_anonymous():
            user = User.objects.get_or_create(email=user_form.cleaned_data.get('email'),
                                              defaults={
                                                  'phones': request.POST.getlist('phones'),
                                              })[0]
            EmailAddress.objects.get_or_create(email=user.email, defaults={'user_id': user.id})
            classified_form.email = user.email
        else:
            user = request.user
            classified_form.email = request.user.email
        if 'hide_phones' in request.POST:
            if not user.hide_phones:
                user.hide_phones = True
                user.save()
        else:
            if user.hide_phones:
                user.hide_phones = False
                user.save()
        user_verified = user.is_verified()
        city = City.objects.get_or_create(name=city_name, country=classified_form.cleaned_data.get('country'))
        classified = classified_form.save(commit=False)
        classified.city = city[0]
        classified.phones = request.POST.getlist('phones')
        classified.author = user
        if not user.is_authenticated() or not user_verified:
            classified.is_active = False
        classified.save()
        token = Token.objects.create(user=user, classified=classified)
        handnle_form_images(obj=classified,
                            img_related_key='images',
                            csrf=self.request.META['CSRF_COOKIE'],
                            images_tmp=self.request.POST.getlist('image_tmp'),
                            images_files=self.request.FILES.getlist('image'))
        if (not user.is_authenticated()) or (not user_verified):
            send_notification_after_adding_post(user, token)
            return HttpResponseRedirect(reverse('users:email_verification_sent', kwargs={'email': user.email}))
        send_notification_after_adding_post(user, token)
        if user.is_authenticated() or user_verified:
            classified.post_on_facebook()
        return HttpResponseRedirect(classified.get_absolute_url())


def get_options():
    sub_categories = SubCategory.objects.all().values_list('id', 'name', 'category_id').extra(order_by=['name'])
    sub_sub_categories = SubSubCategory.objects.all().values_list('id', 'name', 'sub_category_id').extra(order_by=['name'])
    result = {}
    result['sub_categories'] = defaultdict(list)
    result['sub_sub_categories'] = defaultdict(list)

    for sub_category_id, name, category_id in sub_categories:
        result['sub_categories'][category_id].append((sub_category_id, name))
    for sub_sub_category_id, name, sub_category_id in sub_sub_categories:
        result['sub_sub_categories'][sub_category_id].append((sub_sub_category_id, name))
    return json.dumps(result)


class EditClassifiedView(TemplateView):
    template_name = 'classifieds/add_classified.html'
    home_url = '/'

    def get(self, request, pk, *args, **kwargs):
        classified = Classified.objects.get(pk=pk)
        if request.user != classified.author:
            return HttpResponseRedirect(self.home_url)
        classified_form = ClassifiedForm(instance=classified)
        classified_form.fields['city'].initial = classified.city
        images = classified.images.all()
        return render(request, self.template_name,
                      {"form": classified_form,
                       "header": 'classified',
                       "images": images,
                       'user_form': None,
                       'calling_code': json.dumps(CALLING_CODE),
                       "images_tmp": get_tmp_image_urls(request.META['CSRF_COOKIE']),
                       'user_form': None,
                       'categories': get_options()})

    def post(self, request, pk):
        classified = Classified.objects.get(pk=pk)
        classified_form = ClassifiedForm(data=request.POST, instance=classified)
        images = classified.images.all()
        if classified_form.is_valid():
            return self.form_valid(classified_form, request)
        return render(request, self.template_name,
                      {"form": classified_form,
                       "images": images,
                       "header": 'realty',
                       "images_tmp": get_tmp_image_urls(request.META['CSRF_COOKIE']),
                       'user_form': None,
                       'calling_code': json.dumps(CALLING_CODE),
                       'categories': get_options()})

    def form_valid(self, classified_form, request):
        city_name = classified_form.cleaned_data.get('city')
        phones = request.POST.getlist('phones')
        city = City.objects.get_or_create(name=city_name, country=classified_form.cleaned_data.get('country'))
        classified = classified_form.save(commit=False)
        classified.city = city[0]
        classified.phones = phones
        if 'hide_phones' in request.POST:
            if not request.user.hide_phones:
                request.user.hide_phones = True
                request.user.save()
        else:
            if request.user.hide_phones:
                request.user.hide_phones = False
                request.user.save()
        if not request.user.phones:
            request.user.phones = phones.split(',')
            request.user.save()
        if not classified.author:
            classified.author = request.user
        classified.save()
        handnle_form_images(obj=classified,
                            img_related_key='images',
                            csrf=self.request.META['CSRF_COOKIE'],
                            images_tmp=self.request.POST.getlist('image_tmp'),
                            images_files=self.request.FILES.getlist('image'),
                            images_saved=self.request.POST.getlist('image_saved'))
        return HttpResponseRedirect(reverse("classifieds:details", kwargs={'pk': classified.id}))
