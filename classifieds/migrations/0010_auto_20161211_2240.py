# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('classifieds', '0009_classified_update_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='classifiedimage',
            name='classified',
            field=models.ForeignKey(related_name='images', to='classifieds.Classified'),
        ),
    ]
