# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0014_auto_20160708_0921'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='car',
            name='price_history',
        ),
    ]
