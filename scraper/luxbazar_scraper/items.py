# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ClassifiedItem(scrapy.Item):
    url = scrapy.Field()
    title = scrapy.Field()
    description = scrapy.Field()
    options = scrapy.Field()
    category = scrapy.Field()
    sub_category = scrapy.Field()
    sub_sub_category = scrapy.Field()
    country = scrapy.Field()
    city = scrapy.Field()
    phones = scrapy.Field()
    email = scrapy.Field()
    price = scrapy.Field()

    image_urls = scrapy.Field()
    images = scrapy.Field()

    new_price = scrapy.Field()
    id = scrapy.Field()
    model_obj = scrapy.Field()
    update = scrapy.Field()
