# -*- coding: utf-8 -*-
import json, re, scrapy

from ..items import CarItem
from vehicles.models import Car, Moto


class LuxautoSpider(scrapy.Spider):
    name = 'luxauto'
    allowed_domains = ['luxauto.lu']
    start_urls = [
        'http://www.luxauto.lu/fr/occasions/voiture/?tri=modele_asc',
        'http://www.luxauto.lu/fr/occasions/moto/?tri=modele_asc',
    ]

    def parse(self, response):
        self.logger.info('////start parsing////')
        adv_list = response.xpath('//div[@class="ligne_occasion"]')
        for adv in adv_list:
            link = adv.xpath('.//p[@class="marque"]/a/@href').extract_first()
            if 'page' in link:
                link = re.sub(r'&page=\d+', '', link)
            url = response.urljoin(link)
            ModelClass = Car if '/voiture/' in response.url else Moto
            obj = ModelClass.objects.filter(url=url).only('price').first()
            if not obj:
                yield scrapy.Request(url,
                                     callback=self.parse_detail,
                                     meta={'model_class': ModelClass().__class__.__name__})
            else:
                self.logger.info('already exists')
                price = adv.xpath('.//div[@class="prix"]/text()').extract_first()
                price = int(re.sub(r'\D', '', price))
                item = CarItem()
                item['model_obj'] = obj
                if price != obj.price:
                    item['new_price'] = price
                item['update'] = True
                yield item

        next_page = response.xpath(
            '//div[@class="navigation"][1]//div[@class="btn_nav"][2]/a/@href'
        ).extract_first()
        if next_page:
            yield scrapy.Request(url=response.urljoin(next_page), callback=self.parse)

    def parse_detail(self, response):
        item = CarItem()
        item['model_class'] = response.meta['model_class']
        if 'page' in response.url:
            item['url'] = re.sub(r'&page=\d+', '', response.url)
        else:
            item['url'] = response.url
        header = response.xpath('//h2[@class="titre_l"]')
        brand_model = header.xpath('./span[@class="marque"]/text()').extract_first().strip()
        item['brand'] = re.match(r'[^\s]+', brand_model).group()
        item['model'] = brand_model[len(item['brand']) + 1:]

        model_type = header.xpath('./text()').extract_first()
        if model_type:
            item['model_type'] = model_type.strip()

        img_block = response.xpath('//div[@class="bloc_details_annonce_t_espace"]')
        imgs = img_block.xpath('.//a[@class="fancybox-thumbs"]/@href').extract()
        item['image_urls'] = imgs

        details = response.xpath(
            '//div[@class="contenu_caracteristiques"]//dl[1]/dd/text()'
        ).extract()

        for i in xrange(len(details)):
            details[i] = details[i][1:].strip()
            if i in [1, 2]:
                details[i] = re.sub(r'\D', '', details[i])

        if re.match(r'\d{2}-\d{4}$', details[0]):
            item['model_month'] = int(details[0][:2])
            item['model_year'] = int(details[0][3:])
        if details[1]:
            item['km'] = details[1]
        if details[2]:
            item['price'] = details[2]
        if details[3]:
            item['energy'] = details[3]
        if details[4]:
            item['gear_box'] = details[4]
        if details[5]:
            cleared = re.match(r'^\d+', details[5])
            if cleared:
                item['cylinder'] = cleared.group()
        if details[6]:
            if details[6] != 'CV - 0 kW':
                item['power'] = re.search(r'\d+', details[6]).group()
        if details[7]:
            item['color'] = details[7]
        if item['model_class'] == 'Car':
            if details[8]:
                item['color_inside'] = details[8]
            if details[9]:
                item['places'] = details[9]
            if details[10]:
                item['doors'] = details[10]
        else:
            if details[8]:
                item['places'] = details[8]

        contact_block = response.xpath('//div[@class="part_t"]')
        address = contact_block.xpath('.//input[@id="gm_adresse"]/@value').extract_first()
        location = contact_block.xpath('.//input[@id="gm_cp_localite"]/@value').extract_first()
        phones = contact_block.xpath('.//ul[@class="num"]/li/text()[normalize-space()]').extract()
        seller_name = contact_block.xpath('.//p[@class="name"]/text()').extract_first()
        country = contact_block.xpath('.//input[@id="gm_pays"]/@value').extract_first()
        latitude = contact_block.xpath('.//input[@id="gm_latitude"]/@value').extract_first()
        longitude = contact_block.xpath('.//input[@id="gm_longitude"]/@value').extract_first()

        item['seller'] = {}

        if address and location:
            item['seller']['location'] = '%s, %s' % (address.strip(), location.strip())
        elif address or location:
            item['seller']['location'] = address.strip() or location.strip()
        if contact_block.xpath('.//p[@class="type_vendeur"]'):
            item['seller']['seller_type'] = 1  # private
        elif contact_block.xpath('.//p[@class="type_vendeur type_vendeur_pro"]'):
            item['seller']['seller_type'] = 2  # professional
        if seller_name:
            item['seller']['name'] = seller_name.strip()
        if phones:
            item['seller']['phones'] = ','.join([x.strip() for x in phones])
            item['phones'] = item['seller']['phones'].split(',')
        if country:
            item['country'] = country
        if latitude != '0' and longitude != '0':
            item['latitude'] = latitude
            item['longitude'] = longitude
        city = response.xpath('//input[@id="gm_cp_localite"]/@value').extract_first()
        if city and re.search(r'\d', city):
            city = re.search(r'(?<=\d)\D+', city).group()
        city = city.strip() if city else None
        if city:
            item['city'] = city

        description = response.xpath('//p[@class="commentaire"]/text()').extract()
        description = [x.strip() for x in description]
        description = '\r\n'.join([x for x in description if x])
        if description:
            item['description'] = description
        options = response.xpath('//div[@class="contenu_option"]//li/text()').extract()
        if options:
            item['options'] = json.dumps([x.strip() for x in options])

        yield item
