# -*- coding: utf-8 -*-
from sets import Set

import scrapy, re
from ..items import JobItem

from scrapy.shell import inspect_response

from vehicles.models import Country
from realty.models import City
from jobs.models import Job, Contract

contracts = Contract.objects.all()


class MonsterJobsSpider(scrapy.Spider):
    name = 'monster'
    allowed_domains = ['www.monster.lu',
                       'offreemploi.monster.lu']
    start_urls = [
        'http://www.monster.lu/fr/emploi/recherche/?q=&page=1'
    ]

    def parse(self, response):
        self.logger.info('///// parse list /////')
        link_list = response.xpath('//section[@id="resultsWrapper"]/div/article/div/h2/a/@href').extract()
        url_list = [response.urljoin(x) for x in link_list]
        for url in url_list:
            if u"?" in url:
                url = url.split(u"?")[0]
            if Job.objects.filter(url=url).exists():
                self.logger.info('/// already exists ///')
                item = JobItem()
                item['model_obj'] = Job.objects.filter(url=url).first()
                item['update'] = True
                yield item
            else:
                yield scrapy.Request(url, callback=self.parse_detail)

        max_pages = response.xpath('//input[@id="totalPages"]/@value').extract_first()
        current_page = re.search(r'(?<=page=)\d+', response.url).group()
        self.logger.info(current_page)
        self.logger.info(max_pages)
        if int(current_page) < int(max_pages):
            next_page = re.sub(r'(?<=page=)\d+', str(int(current_page) + 1), response.url)
            self.logger.info(next_page)
            self.logger.info('//////////// got to next page //////////////')
            yield scrapy.Request(response.urljoin(next_page), callback=self.parse)

    def parse_detail(self, response):
        self.logger.info('/// parse detail ///')
        item = JobItem()
        item['employer'] = {}
        item['url'] = response.url
        item['default_country_id'] = 35  # Luxembourg

        title = response.xpath('//span[@id="lblCriteria"]/@title').extract_first()
        if not title:
            title = response.css('div.opening h2').xpath('./text()').extract_first()
        item['title'] = title
        description = response.xpath('//span[@id="TrackingJobBody"]').extract()
        item['description'] = ''.join(description)
        jobsummary_content = response.xpath('//div[@id="jobsummary_content"]')
        city = jobsummary_content.xpath('.//span[@itemprop="jobLocation"]/text()').extract_first()
        if city and u'définir' not in city:
            if re.search(r'[\s\d,/-]', city):
                city_filter = {'country_id': item['country_id']} if item.get('country_id') else {}
                city = city.lower().replace('luxemburg', 'luxembourg')
                lcity = city.lower().split(',')
                check_list = [x.strip() for x in lcity]
                for c in lcity:
                    check_list.append(re.sub(r'-{2,}', '-', re.sub(r'[\s/]', '-', c)))
                cities = City.objects.filter(**city_filter).only('id', 'name')
                city_matches = []
                for c in cities:
                    stop = False
                    for raw_city in check_list:
                        if c.name.lower() in raw_city or raw_city in c.name.lower():
                            city_matches.append(c)
                            if c.name != 'Luxembourg':
                                stop = True
                            break
                    if stop:
                        break
                if city_matches:
                    item['city_id'] = city_matches[-1].id
                    if not item.get('country_id'):
                        item['country_id'] = city_matches[-1].country_id
                        if not item['employer'].get('country_id'):
                            item['employer']['country_id'] = city_matches[-1].country_id
            else:
                if city.lower() == 'luxemburg':
                    item['city'] = 'Luxembourg'
                else:
                    item['city'] = city

        script = response.xpath('//script[contains(text(), "_m.ATM.properties")]/text()').extract_first()
        if script:
            category = re.findall(r'(?<=eVar(?:28|29)":")[^\"]+', script)
            if category:
                item['category'] = []
                for cat in category:
                    cat = cat.split(';')
                    item['category'] += cat
                item['category'] = list(set(item['category']))

        work_hours = jobsummary_content.xpath('.//span[@itemprop="employmentType"]/text()').extract_first()
        if work_hours:
            item['work_hours'] = [work_hours]
        candidate_level = jobsummary_content.xpath(
            './/span[@itemprop = "experienceRequirements"]/text()').extract_first()
        if candidate_level:
            item['candidate_level'] = candidate_level
        contract_list = jobsummary_content.xpath('.//dd[@class="multipleddlast"]/span/text()').extract()
        if contract_list:
            for contract in contracts:
                matching = [contract.contract for i in contract_list if contract.contract in i]
                if matching:
                    item['contract'] = matching
        employer_name = jobsummary_content.xpath('.//span[@itemprop="hiringOrganization"]/text()').extract_first()
        if not employer_name:
            employer_name = jobsummary_content.xpath('.//span[@itemprop="name"]/text()').extract_first()
        if employer_name:
            item['employer']['name'] = employer_name
        yield item
