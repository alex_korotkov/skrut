# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0037_car_phones'),
        ('users', '0006_auto_20160907_1050'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='country',
        ),
        migrations.AddField(
            model_name='user',
            name='user_country',
            field=models.ForeignKey(verbose_name='User country', to='vehicles.Country', null=True),
        ),
    ]
