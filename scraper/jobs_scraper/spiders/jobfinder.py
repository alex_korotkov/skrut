# -*- coding: utf-8 -*-
import scrapy, re
from scrapy import FormRequest

from ..items import JobItem
import simplejson as json
from scrapy.shell import inspect_response
import time
from datetime import datetime

from vehicles.models import Country
from realty.models import City
from jobs.models import Job, Contract
from dateutil.relativedelta import relativedelta

CITIES = City.objects.all()
COUNTRIES = Country.objects.all().values_list('name', flat=True)
contracts = Contract.objects.all()


class JobfinderJobsSpider(scrapy.Spider):
    name = 'jobfinder'
    allowed_domains = ['www.jobfinder.lu']
    start_urls = [
        'http://www.jobfinder.lu/#/jobs'
    ]

    def parse(self, response):
        from_date = datetime.now() + relativedelta(months=-2)
        from_date = int(time.mktime(from_date.timetuple())) * 1000
        to_date = int(time.mktime(datetime.now().timetuple())) * 1000
        yield FormRequest('http://www.jobfinder.lu/api/searchHome',
                          callback=self.get_items,
                          formdata={'branches': [],
                                    'careerLevels': [],
                                    'categories': [],
                                    'contractTypes': [],
                                    'from': '0',
                                    'fromDate': str(from_date),
                                    'query': "",
                                    'sort': "onlineDate",
                                    'terms': [],
                                    'to': '10000',
                                    'toDate': str(to_date)})

    def get_items(self, response):
        items = json.loads(response.body_as_unicode())[u'data'][u'items']
        for item in items:
            url = 'http://www.jobfinder.lu/offers/{}'.format(item[u'id'])
            if Job.objects.filter(url=url).exists():
                self.logger.info('/// already exists ///')
                item = JobItem()
                item['model_obj'] = Job.objects.filter(url=url).first()
                item['update'] = True
                yield item
            else:
                yield scrapy.Request(response.urljoin(url), callback=self.parse_detail)

    def parse_detail(self, response):
        self.logger.info('/// parse detail ///')
        item = JobItem()
        item['employer'] = {}
        item['url'] = response.url
        item['default_country_id'] = 35  # Luxembourg

        item['title'] = response.xpath('//div[@class="offer-title"]/text()').extract_first()
        description = response.xpath('//div[@class="description"]/div').extract()
        if not description:
            description = response.xpath('//div[@class="description"]').extract()
        item['description'] = ''.join(description)
        dict_offer_details = {
            i.xpath('.//span/text()').extract_first(): i.xpath('.//text()').extract()[1].strip() if
            i.xpath('.//text()').extract()[1].strip() else i.xpath('.//ul/li/text()').extract() for i in
            response.xpath('//div[@class="well-no-border"]/div')}
        dict_items = {
            u'Type de contrat: ': 'contract',
            u"Secteurs d'activit\xe9: ": 'category',
            u'Dur\xe9e: ': 'work_hours',
            u'Niveau de poste: ': 'candidate_level'
        }
        for i, y in dict_offer_details.iteritems():
            if i in dict_items.keys():
                item[dict_items.get(i)] = [y]
        address = [i.strip() for i in response.xpath('//div[@class="employer-address"]/text()').extract()]
        phones = [i.replace(u'T\xe9l:'.strip(), '') for i in address if u'T\xe9l:' in i]
        if phones and phones != [u' -']:
            item['employer']['phones'] = phones
        seller_info = ''.join(address).lower()
        city = next((x for x in CITIES if x in seller_info), None)
        if city:
            item['city_id'] = city.id
            item['country_id'] = city.country_id
            item['employer']['country_id'] = city.country_id
        else:
            country = next((x for x in COUNTRIES if x in seller_info), None)
            if country:
                item['country'] = country
                item['employer']['country'] = country
        site = response.xpath('//div[@class="employer-contact"]/a/@href').extract_first()
        if site:
            item['employer']['site'] = site
        name = response.xpath('//div[@class="employer-title"]/b/text()').extract_first()
        if name:
            item['employer']['name'] = name

        yield item
