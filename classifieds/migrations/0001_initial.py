# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Classified',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('description', models.TextField()),
                ('options', models.TextField(blank=True)),
                ('category', models.ForeignKey(to='classifieds.Category')),
            ],
        ),
        migrations.CreateModel(
            name='SubCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('category', models.ForeignKey(to='classifieds.Category')),
            ],
        ),
        migrations.CreateModel(
            name='SubSubCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('sub_category', models.ForeignKey(to='classifieds.SubCategory')),
            ],
        ),
        migrations.AddField(
            model_name='classified',
            name='sub_category',
            field=models.ForeignKey(blank=True, to='classifieds.SubCategory', null=True),
        ),
        migrations.AddField(
            model_name='classified',
            name='sub_sub_category',
            field=models.ForeignKey(blank=True, to='classifieds.SubSubCategory', null=True),
        ),
    ]
