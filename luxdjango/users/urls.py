# -*- coding: utf-8 -*-
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^subscribe-on-search/$', view=views.Subscribe.as_view(), name='subscribe_user'),
    url(r'^social_login_redirect/', view=views.social_redirect, name='social_login_redirect'),
    url(r'^delete-subscriptions/$', view=views.DeleteSubscriptions.as_view()),
    url(r'^delete-posts/$', view=views.DeletePosts.as_view()),
    url(r'^hide-phones/$', view=views.HidePhones.as_view()),
    url(r'^update-post/$', view=views.UpdatePost.as_view()),
    url(r'^adtoken/(?P<token>\w{10,})/(?P<action>.+)/$', view=views.TokenAuth.as_view(), name='adtoken'),
    url(r"^confirm-email/(?P<email>.+)/$", views.EmailVerificationSent.as_view(), name="email_verification_sent"),
    # URL pattern for the UserListView
    url(
        regex=r'^$',
        view=views.UserListView.as_view(),
        name='list'
    ),

    # URL pattern for the UserRedirectView
    url(
        regex=r'^~redirect/$',
        view=views.UserRedirectView.as_view(),
        name='redirect'
    ),
    # URL pattern for the UserDetailView
    url(
        regex=r'^account/$',
        view=views.UserDetailView.as_view(),
        name='detail'
    ),
    url(
        regex=r'^subscriptions/$',
        view=views.UserSubscriptionsView.as_view(),
        name='subscriptions'
    ),
    url(
        regex=r'^posts/$',
        view=views.UserPostsView.as_view(),
        name='posts'
    ),

    # URL pattern for the UserUpdateView
    url(
        regex=r'^~update/$',
        view=views.UserUpdateView.as_view(),
        name='update'
    ),
    url(r'check/$', views.CheckView.as_view()),
]
